title: Protect the Open Web as a Ford-Mozilla Fellow
---
pub_date: 2018-03-21
---
author: tommy
---
tags:

mozilla
advocacy
---
categories:

advocacy
partners
---
summary: The Tor Project will host a Ford-Mozilla Fellow from September 2018 through June 2019
---
_html_body:

<p><em>March 29: This post has been edited with an updated description of what the fellow will be working on.</em></p>
<p>For the last three years, Mozilla has been shaping public-interest technology through their 10-month Open Web Fellows program. Each year, the fellows work with advocacy organizations who share their belief that the open internet can make the world a better place. The Tor Project has been <a href="https://medium.com/read-write-participate/welcoming-11-new-partners-in-the-quest-for-internet-health-843c8d1b2bf9">selected</a> as one of the host organizations for the 2018 fellowship cohort, and applications open today.</p>
<p><i>“Civil society can have an outsized impact on the health of the internet</i> <i>—</i> <i>nonprofits and institutions conduct groundbreaking research, build tools that thwart digital harassment, and protect marginalized communities online.”</i></p>
<p><i>—</i><a href="https://medium.com/read-write-participate/welcoming-11-new-partners-in-the-quest-for-internet-health-843c8d1b2bf9"><i>Welcoming 11 New Partners in the Quest for Internet Health</i></a></p>
<h3><b>Become a User Advocate</b></h3>
<p>Millions of people use Tor every day to protect their privacy, safety, and freedom of expression on the internet. We're seeking to host a User Advocate to bridge the gap between Tor users and Tor developers. If you're passionate about internet freedom and improving software to make it more user-friendly, this might be the perfect job for you. This full-time, 10-month fellowship can be performed remotely or based in our Seattle office.</p>
<p>Your job will be to identify common user problems: where people get stuck setting up or using Tor, and what we can do to address those issues. After identifying and documenting the common problems, you will coordinate with our teams to ensure the problems are addressed.</p>
<p>The ideal candidate will be someone skilled at working with developers to understand the root causes of user issues while still maintaining a broad, user-centric view of the problems and areas where Tor can improve. Previous experience working in a support role is a huge plus.</p>
<p>Fellows receive a <a href="https://advocacy.mozilla.org/en-US/open-web-fellows/overview">$60,000 stipend</a>, paid in monthly installments, as well as childcare and health insurance supplements.</p>
<p>You can find more information about this position on our <a href="https://www.torproject.org/about/jobs-useradvocate.html.en">jobs page</a>. <a href="https://foundation.mozilla.org/fellowships/apply/">Click here</a> to apply (select "Open Web" to be directed to the application for this fellowship). Applications are due by 5pm Eastern on April 20, 2018. Be sure to register by April 18.</p>
<hr />
<p>We’re deeply committed to safeguarding the open internet, and we’re grateful to have Mozilla as a partner in the fight for online privacy and freedom around the world. Whether they’re helping us <a href="https://wiki.mozilla.org/Security/Fusion">uplift Tor patches into Firefox</a>, <a href="https://blog.torproject.org/we-enhanced-security-and-integrity-tor-metrics-supported-moss">supporting our Metrics work</a>, or <a href="https://blog.torproject.org/powering-digital-resistance-help-mozilla">matching donations during our end-of-year crowdfunding campaign</a>, we know that Mozilla has our back. </p>

---
_comments:

<a id="comment-274480"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274480" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">March 21, 2018</p>
    </div>
    <a href="#comment-274480">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274480" class="permalink" rel="bookmark">[comment in progress]</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>[Edit: this post used to be for the relay advocate position, but then we swapped things around so it's for the user advocate position. I am excited about both. I'm leaving the rest of this comment and the other comments intact below, but hopefully this note will help you with context. :) -RD]</p>
<p>I'm so excited that this relay advocate position is finally available! It's something that I have wanted us to work harder on for a long time.</p>
<p>In my opinion, the thing we want most here is a 'people person' -- somebody who can get to know many of the relay operators, understand their issues, help them with the issues, connect them to each other, and raise the issues to other parts of Tor. That is, somebody to advocate on behalf of the relay operators.</p>
<p>We had a session in our Rome meeting on what we want in such a position, and you can read the notes here:<br />
<a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2018Rome/Notes/RelaysAdvocate" rel="nofollow">https://trac.torproject.org/projects/tor/wiki/org/meetings/2018Rome/Not…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274484"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274484" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>. (not verified)</span> said:</p>
      <p class="date-time">March 21, 2018</p>
    </div>
    <a href="#comment-274484">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274484" class="permalink" rel="bookmark">Since the beginning of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Since the beginning of keeping the same base relay for months at a time, when I am blocked in that country, I can never connect to sites I want. Previously I would just click "New Tor Circuit for this Site" a couple of times, and I would hit on a country relay that would allow me to reach the site that was blocked in another country.  I can't wait three months or whenever, to be assigned a new base relay that will allow me to use Tor.  Effectively, Tor has stopped working for me.  This is a major catastrophe, and must be for other people, too.  Please tell me I have this wrong, and that I just need to.......</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274488"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274488" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  tommy
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">t0mmy</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">t0mmy said:</p>
      <p class="date-time">March 21, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274484" class="permalink" rel="bookmark">Since the beginning of…</a> by <span>. (not verified)</span></p>
    <a href="#comment-274488">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274488" class="permalink" rel="bookmark">Hey there -- sorry to hear…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hey there -- sorry to hear Tor's not working right. Have you tried some of our troubleshooting steps? <a href="https://tb-manual.torproject.org/en-US/troubleshooting.html" rel="nofollow">https://tb-manual.torproject.org/en-US/troubleshooting.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274491"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274491" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">March 21, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274484" class="permalink" rel="bookmark">Since the beginning of…</a> by <span>. (not verified)</span></p>
    <a href="#comment-274491">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274491" class="permalink" rel="bookmark">It sounds like the problem…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It sounds like the problem you're having is that some destination website doesn't want to receive connections from some of the Tor exit relays. For more on that issue, you might find this blog post educational:<br />
<a href="https://blog.torproject.org/blog/call-arms-helping-internet-services-accept-anonymous-users" rel="nofollow">https://blog.torproject.org/blog/call-arms-helping-internet-services-ac…</a></p>
<p>But it is a different issue than what you call the 'base relay', and what we call the guard relay. The exit relay is the third hop in the circuit, which destination websites see. The guard relay is the first hop in the circuit, and destination websites don't see it. So I think they are unrelated.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274498"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274498" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274484" class="permalink" rel="bookmark">Since the beginning of…</a> by <span>. (not verified)</span></p>
    <a href="#comment-274498">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274498" class="permalink" rel="bookmark">&gt; Previously I would just…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Previously I would just click "New Tor Circuit for this Site" a couple of times, and I would hit on a country relay that would allow me to reach the site that was blocked in another country.</p>
<p>What happens when you click on "New identity"?</p>
<p>I don't understand why you think (yes?) that the problem is not with what exit router your circuits are using (which could certainly lead to blockage, e.g. I often encounter blockage from UK sites when the exit router is not registered in the UK) but with the entry guard.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274489"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274489" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>123 (not verified)</span> said:</p>
      <p class="date-time">March 21, 2018</p>
    </div>
    <a href="#comment-274489">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274489" class="permalink" rel="bookmark">Is this position limited to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great to hear about this opportunity! I've got a few questions:</p>
<ul>
<li>Is this position limited to certain citizenship or are you allowed to come from an arbitrary country?</li>
<li>Does this position require to travel internationally? (could you list the mandatory destination countries)</li>
<li>Is there a "not later than" start date (important information for people that need to quit their current job first)?</li>
<li></li>
</ul>
<p>Mozilla's page says:</p>
<blockquote><p>Please review the the guidelines for the grant program to which you are applying before submitting your registration. For assistance, please contact <a href="mailto:grants@mozillafoundation.org" rel="nofollow">grants@mozillafoundation.org</a></p></blockquote>
<p>Can you link to these guidelines?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274496"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274496" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274489" class="permalink" rel="bookmark">Is this position limited to…</a> by <span>123 (not verified)</span></p>
    <a href="#comment-274496">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274496" class="permalink" rel="bookmark">&gt; Is this position limited…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Is this position limited to certain citizenship </p>
<p>I certainly hope not.</p>
<p>&gt; or are you allowed to come from an arbitrary country?</p>
<p>I certainly hope so, but now that you mention it, the Relay Advocate would need to be able to travel to the US without too much hassle, and thanks to Drump, that would appear to exclude some citizenships.   D-mn, we need to shake up the US Congress bigtime!</p>
<p>Such practical considerations aside, I am very concerned that the relay advocate be a "people person", as arma put it, i.e. can get along with all kinds of people and are fluent in at least two languages (not asking too much if TP mostly look outside the US) and have an impeccable pro-human rights record, with no history of involvement with spooks anywhere in the world (unless they are a known whistleblower like Snowden or Kiriakou),</p>
<p>I'd support a certain flexibility in order to hire the right candidate.   Obviously someone like Snowden has huge name recognition and credibility in the Tor community.  So much so that if he's interested, and if TP decides he's a "people person", some job in TP should be found for him.  I do think arma has the right idea though about getting to know relay operators, and I think that is best done by meeting with them personally.</p>
<p>&gt; Does this position require to travel internationally? </p>
<p>I should certainly hope so.  </p>
<p>Hmm... we need to grip politicians around the world by their throat, shake hard, and freaking well make them give people like Snowden the ability to travel freely!</p>
<p>We the People have been far too nice to our political leaders for far too long.  If we want anything to change, if we want to take back our privacy, end the endless wars, address climate change and social justice issues, and generally stop the B$, finger pointing, and can-kicking which has characterized most legislatures, we need to start kicking the political elites where it hurts.  God knows they have been making us feel all kinds of pain for a long time now.  Time to change that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274499"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274499" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274489" class="permalink" rel="bookmark">Is this position limited to…</a> by <span>123 (not verified)</span></p>
    <a href="#comment-274499">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274499" class="permalink" rel="bookmark">&gt; Previously I would just…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Previously I would just click "New Tor Circuit for this Site" a couple of times, and I would hit on a country relay that would allow me to reach the site that was blocked in another country.</p>
<p>What happens when you click on "New identity"?</p>
<p>Can you provide detail suggesting that the problem is not with what exit router your circuits are using (which could certainly lead to blockage, e.g. I often encounter blockage from UK sites when the exit router is not registered in the UK) but with the entry guard?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274504"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274504" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274489" class="permalink" rel="bookmark">Is this position limited to…</a> by <span>123 (not verified)</span></p>
    <a href="#comment-274504">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274504" class="permalink" rel="bookmark">I found the answer for one…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I found the answer for one of these questions: Start date is September 2018.<br />
(mentioned here: <a href="https://medium.com/read-write-participate/welcoming-11-new-partners-in-the-quest-for-internet-health-843c8d1b2bf9" rel="nofollow">https://medium.com/read-write-participate/welcoming-11-new-partners-in-…</a> )</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274508"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274508" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  tommy
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">t0mmy</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">t0mmy said:</p>
      <p class="date-time">March 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274489" class="permalink" rel="bookmark">Is this position limited to…</a> by <span>123 (not verified)</span></p>
    <a href="#comment-274508">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274508" class="permalink" rel="bookmark">Hi there!…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi there!<br />
* I believe you can come from any country, but (speaking in a personal capacity) citizens of Cuba, Iran, North Korea, Sudan, Syria, or the Crimea Region in the Ukraine may be ineligible because of US sanctions.<br />
* There is an onboarding process in the UK in late August. There may be other travel opportunities but we haven't hammered them out yet. If you'd like to talk to me privately about your individual case, you can email <a href="mailto:tommyc@torproject.org" rel="nofollow">tommyc@torproject.org</a>. You can find my PGP key at <a href="https://www.torproject.org/about/corepeople" rel="nofollow">https://www.torproject.org/about/corepeople</a>.<br />
* The fellowship begins in September, plus onboarding in late August.<br />
* I believe that is general language (other fellowships may have more restrictive application criteria, for example). The guidelines in this instance include this post, the application page at <a href="https://foundation.mozilla.org/fellowships/apply/" rel="nofollow">https://foundation.mozilla.org/fellowships/apply/</a>, and, for good measure, Mozilla's participation guidelines: <a href="https://www.mozilla.org/en-US/about/governance/policies/participation/" rel="nofollow">https://www.mozilla.org/en-US/about/governance/policies/participation/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274495"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274495" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2018</p>
    </div>
    <a href="#comment-274495">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274495" class="permalink" rel="bookmark">Wow, this is huge and hugely…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wow, this is huge and hugely welcome news!   I hope Tor Project receives many applications from well qualified applicants.   </p>
<p>I endorse doing pretty much anything TP can to move away from the USG funded model to a user funded model, even if it means accepting, in the short term, small grants (if no strings are attached) from non-US governments such as Iceland, NE, even DE (but certainly not RU, CN, BR or others with highly dubious human rights records--- which excludes almost all governments, actually) or corporate "charities" like Ford Foundation.  But please, never forget why the name of Henry Ford is not fondly remembered by many Americans who follow some non-Christian religion or have non-pale skin pigmentation.  And be aware that Ford has its fingers deep into some very nasty business.</p>
<p>One thing which I don't see in the post, and it worries me, is the statement that applications *must* disclose prior work (including contract work) for intelligence agencies or governments.  CIA type gag laws are not an excuse.  TP *must* tell applicants that if it later learns they lied for any reason (such as violating a US law "protecting" CIA assets) they will be fired when the lie comes to light.</p>
<p>Please, please, TP, do what you can to keep CIA moles (and RU trolls) from becoming "inside threats" embedded in TP!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274497"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274497" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2018</p>
    </div>
    <a href="#comment-274497">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274497" class="permalink" rel="bookmark">&gt;  we’ve had our eyes on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt;  we’ve had our eyes on those parts of the world where there are fewer Tor relays.</p>
<p>Africa.  Latin America.  South Asia.  I presume those are some of the regions you have in mind.</p>
<p>Huge and urgent need for anonymity in defense of pro-democracy activism in places like Brazil, Venezuela, Hong Kong, Nigeria, Kenya, Ethiopia, etc.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274502"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274502" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2018</p>
    </div>
    <a href="#comment-274502">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274502" class="permalink" rel="bookmark">Mozilla have NO right to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Mozilla have NO right to speak Open Web. I prefer donating to Tor project rather than Mozilla!!</p>
<p><a href="https://trac.torproject.org/projects/tor/ticket/24351" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/24351</a></p>
<p>Mozilla recently took down anti-MITM addon from their marketplace.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274526"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274526" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>K H (not verified)</span> said:</p>
      <p class="date-time">March 24, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274502" class="permalink" rel="bookmark">Mozilla have NO right to…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274526">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274526" class="permalink" rel="bookmark">https://trac.torproject.org…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p><a href="https://trac.torproject.org/projects/tor/ticket/24351" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/24351</a></p></blockquote>
<p>Showing certificate CN on somewhere near on address bar may also help here (specially if it is only shown when it differs from host part of url).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274510"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274510" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>sites blocks tor (not verified)</span> said:</p>
      <p class="date-time">March 22, 2018</p>
    </div>
    <a href="#comment-274510">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274510" class="permalink" rel="bookmark">sites are blocking tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>sites are blocking tor.....no way to access the website</p>
<p>need a way for it to bypass or penetrate this block..........add vpn integration into tor to bypass this......?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274511"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274511" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  tommy
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">t0mmy</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">t0mmy said:</p>
      <p class="date-time">March 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274510" class="permalink" rel="bookmark">sites are blocking tor…</a> by <span>sites blocks tor (not verified)</span></p>
    <a href="#comment-274511">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274511" class="permalink" rel="bookmark">You might want to check out…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You might want to check out Tor Bridges: <a href="http://bridges.torproject.org" rel="nofollow">http://bridges.torproject.org</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274521"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274521" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>i like onions (not verified)</span> said:</p>
      <p class="date-time">March 23, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274510" class="permalink" rel="bookmark">sites are blocking tor…</a> by <span>sites blocks tor (not verified)</span></p>
    <a href="#comment-274521">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274521" class="permalink" rel="bookmark">Unfortunately, if your issue…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Unfortunately, if your issue is that the sites block Tor users, you'll need to ask the site owners to (if possible) not block users coming from Tor exit nodes (maybe via a polite email or message). A temporary solution would be to use the New Identity button in the Tor Browser Bundle (so that you can switch your exit IP), but it isn't always reliable if the site owner updates his/her list of Tor exit IPs all the time.</p>
<p>We need help convincing the Internet community at large that there are various legitimate uses for Tor. Refer to this Tor Project post for more info:<br />
<a href="https://blog.torproject.org/blog/call-arms-helping-internet-services-accept-anonymous-users" rel="nofollow">https://blog.torproject.org/blog/call-arms-helping-internet-services-ac…</a></p>
<p>Also, VPN integration into the TBB is not impossible, but very difficult, as the Project now has to maintain a VPN service on top of the Tor Network, which will put a strain on their already limited resources. VPN exits can also be eventually profiled and blocked as well too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274512"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274512" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2018</p>
    </div>
    <a href="#comment-274512">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274512" class="permalink" rel="bookmark">Since my previous question…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Since my previous question got partially ignored I will try again and more specifically:</p>
<p>Does this position require traveling to the US?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274629"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274629" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  tommy
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">t0mmy</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">t0mmy said:</p>
      <p class="date-time">March 28, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274512" class="permalink" rel="bookmark">Since my previous question…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274629">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274629" class="permalink" rel="bookmark">Travel to the USA is not a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Travel to the USA is not a requirement of this position.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274533"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274533" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 25, 2018</p>
    </div>
    <a href="#comment-274533">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274533" class="permalink" rel="bookmark">Why did you remove the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why did you remove the original blog post?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274628"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274628" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  tommy
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">t0mmy</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">t0mmy said:</p>
      <p class="date-time">March 28, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274533" class="permalink" rel="bookmark">Why did you remove the…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274628">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274628" class="permalink" rel="bookmark">We&#039;re finalizing the details…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We're finalizing the details of this fellowship, and so we removed references to the original position until we have everything ready, including a posting on our <a href="https://www.torproject.org/about/jobs.html.en" rel="nofollow">jobs page</a>. We expect to have the complete post in the next day or so.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274535"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274535" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>User from china (not verified)</span> said:</p>
      <p class="date-time">March 25, 2018</p>
    </div>
    <a href="#comment-274535">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274535" class="permalink" rel="bookmark">Hearing the exciting  news ,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hearing the exciting  news , i cannot  sleep for serval nights</p>
</div>
  </div>
</article>
<!-- Comment END -->
