title: New Pluggable Transports bundles 0.2.4.11-alpha (flashproxy + obfsproxy)
---
pub_date: 2013-03-19
---
author: asn
---
tags:

obfsproxy
pluggable transports
flashproxy
obfsbundle
---
categories: circumvention
---
_html_body:

<p>We've updated the Pluggable Transports Tor Browser Bundles with Firefox 17.0.4esr and Tor 0.2.4.11-alpha. These releases have numerous bug fixes and a new Torbutton as well.</p>

<ul>
<li><a href="https://www.torproject.org/dist/torbrowser/tor-pluggable-transports-browser-2.4.11-alpha-2_en-US.exe" rel="nofollow">Windows</a> (<a href="https://www.torproject.org/dist/torbrowser/tor-pluggable-transports-browser-2.4.11-alpha-2_en-US.exe.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://www.torproject.org/dist/torbrowser/osx/TorBrowser-Pluggable-Transports-2.4.11-alpha-2-osx-i386-en-US.zip" rel="nofollow">Mac OS X</a> (<a href="https://www.torproject.org/dist/torbrowser/osx/TorBrowser-Pluggable-Transports-2.4.11-alpha-2-osx-i386-en-US.zip.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-i686-2.4.11-alpha-2-dev-en-US.tar.gz" rel="nofollow">GNU/Linux 32-bit</a> (<a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-i686-2.4.11-alpha-2-dev-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
<li><a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-x86_64-2.4.11-alpha-2-dev-en-US.tar.gz" rel="nofollow">GNU/Linux 64-bit</a> (<a href="https://www.torproject.org/dist/torbrowser/linux/tor-pluggable-transports-browser-gnu-linux-x86_64-2.4.11-alpha-2-dev-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
</ul>

<p>Like the previous bundles, these contain Flashproxy and the Python version of Obfsproxy.</p>

<p><a href="http://crypto.stanford.edu/flashproxy/" rel="nofollow">Flash proxy</a> is a transport that uses proxies running in web browsers as access points into Tor. <a href="https://gitweb.torproject.org/pluggable-transports/pyobfsproxy.git" rel="nofollow">Obfsproxy</a> is a pluggable transport that makes network traffic look unlike normal Tor traffic. Both of these technologies make it harder to block access to Tor. If you previously used the obfsproxy bundle, please upgrade to this bundle, which in addition to flash proxy has new obfsproxy bridges.</p>

<p><a href="https://trac.torproject.org/projects/tor/wiki/FlashProxyHowto" rel="nofollow">Flash proxy works differently</a> from other pluggable transports, and you need to take extra steps to make it work. In particular, <strong>you will probably need to configure port forwarding</strong> in order to receive connections from browser proxies. There are instructions and hints on how to do that at this page: <a href="https://trac.torproject.org/projects/tor/wiki/FlashProxyHowto" rel="nofollow">flash proxy howto</a>.</p>

<p>These bundles contain the same hardcoded obfs2 bridge addresses as the previous bundles which may work for some jurisdictions but you are strongly advised to get new bridge addresses from BridgeDB: <a href="https://bridges.torproject.org/?transport=obfs2" rel="nofollow">https://bridges.torproject.org/?transport=obfs2</a>.</p>

<p>Furthermore, we are looking for feedback on how the bundles work. Please leave comments on the <a href="https://trac.torproject.org/projects/tor/wiki/FlashProxyUsability" rel="nofollow">flash proxy usability wiki page</a> or <a href="https://trac.torproject.org/projects/tor/ticket/7824" rel="nofollow">ticket #7824</a> with your experience, good or bad.</p>

<p>There are other ways you can help beyond testing the bundles. One is to <a href="https://gitweb.torproject.org/user/asn/pyobfsproxy.git/blob/HEAD:/doc/HOWTO.txt" rel="nofollow">run a bridge with pyobfsproxy</a>. Another is to <a href="http://crypto.stanford.edu/flashproxy/#badge-howto" rel="nofollow">put the flash proxy badge</a> on your web site or blog, or <a href="https://gitweb.torproject.org/flashproxy.git/blob/HEAD:/modules/mediawiki/custom.js" rel="nofollow">add it to your Wikipedia profile</a>. If you want your browser to continue to be a proxy after a switch to an opt-in model, click the “Yes” button on <a href="http://crypto.stanford.edu/flashproxy/options.html" rel="nofollow">the options page</a>.</p>

---
_comments:

<a id="comment-19208"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19208" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 19, 2013</p>
    </div>
    <a href="#comment-19208">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19208" class="permalink" rel="bookmark">Anyone has same problems? I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Anyone has same problems? I have two PCs working in same network ADSL+router.<br />
Flashproxy-reg-email works well in the PC running WIN7; but flashproxy-reg-email doesn't work in the PC running XP sp3, here is the details:</p>
<p>2013-03-20 13:29:21 Trying to register ":9002".<br />
2013-03-20 13:29:21 Running command: D:\xxx\download\F20\Tor Browser\App\flashproxy-reg-email :9002<br />
2013-03-20 13:29:27 Local connection from [scrubbed].<br />
2013-03-20 13:29:34 SOCKS request from [scrubbed].<br />
2013-03-20 13:29:34 Got SOCKS request for [scrubbed].<br />
2013-03-20 13:29:34 locals  (1): ['[scrubbed]']<br />
2013-03-20 13:29:34 remotes (0): []<br />
2013-03-20 13:29:34 Data from unlinked local [scrubbed] (217 bytes).<br />
2013-03-20 13:29:34 locals  (1): ['[scrubbed]']<br />
2013-03-20 13:29:34 remotes (0): []<br />
2013-03-20 13:29:41 flashproxy-reg-email: Failed to register: [Errno 1] _ssl.c:504: error:14090086:SSL routines:SSL3_GET_SERVER_CERTIFICATE:certificate verify failed<br />
2013-03-20 13:29:41 flashproxy-reg-email exited with status 1.<br />
2013-03-20 13:29:41 Running command: D:\xxx\download\F20\Tor Browser\App\flashproxy-reg-http :9002<br />
2013-03-20 13:29:43 flashproxy-reg-http: Registered ":9002" with <a href="https://xxx-xxx.bamxxx.com/" rel="nofollow">https://xxx-xxx.bamxxx.com/</a>.<br />
2013-03-20 13:29:47 Remote connection from [scrubbed].</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19243"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19243" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19208" class="permalink" rel="bookmark">Anyone has same problems? I</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19243">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19243" class="permalink" rel="bookmark">I have the same problem. Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have the same problem. Tor (flashproxy) doesn`t work in PC running win xp , however it works in PC runnig win vista.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19251"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19251" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 23, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19208" class="permalink" rel="bookmark">Anyone has same problems? I</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19251">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19251" class="permalink" rel="bookmark">Same error here, on a Win7</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Same error here, on a Win7 64bit PC:<br />
2013-03-23 13:34:46 flashproxy-reg-email: Failed to register: [Errno 1] _ssl.c:504: error:14090086:SSL routines:SSL3_GET_SERVER_CERTIFICATE:certificate verify failed<br />
2013-03-23 13:34:46 flashproxy-reg-email exited with status 1.</p>
<p>Anybody knows what's the culprit?<br />
Using Tor Browser out of the the latest pluggable transports bundle or w/ additional port forwarding result in same situation.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-19229"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19229" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2013</p>
    </div>
    <a href="#comment-19229">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19229" class="permalink" rel="bookmark">If you are on tumblr you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you are on tumblr you might find this interesting:<br />
<a href="http://flomu.tumblr.com/post/45777337880/tl-dr-if-you-reblog-this-you-will-help-internet" rel="nofollow">http://flomu.tumblr.com/post/45777337880/tl-dr-if-you-reblog-this-you-w…</a></p>
<p>P.S.: You can even EDIT the HTML of your theme and add it there.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19236"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19236" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2013</p>
    </div>
    <a href="#comment-19236">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19236" class="permalink" rel="bookmark">TOR, recent two weeks,very</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TOR, recent two weeks,very difficult to connect to tor network in China,even I use sock5 proxy, it is hard to connect. and after connecting, I remove proxy, and restart nearly the same time, but couldn't connect again.Could your people have a check the Tor status now in China?<br />
Fuck GFW.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19238"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19238" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">March 22, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19236" class="permalink" rel="bookmark">TOR, recent two weeks,very</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19238">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19238" class="permalink" rel="bookmark">The obfsproxy TBB should</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The obfsproxy TBB should work in China. Probably not out-of-the-box since the GFW tends to blacklist the hard-coded bridges contained in the bundle. So you will probably need new obfsproxy bridges: <a href="https://bridges.torproject.org/?transport=obfs2" rel="nofollow">https://bridges.torproject.org/?transport=obfs2</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-19237"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19237" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2013</p>
    </div>
    <a href="#comment-19237">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19237" class="permalink" rel="bookmark">and more now in China, I use</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>and more now in China, I use proxy to connect tor, when tor start firefox, the firefox broswer just couldn't connect too, shows, proxy refuse to connect.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19242"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19242" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19237" class="permalink" rel="bookmark">and more now in China, I use</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19242">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19242" class="permalink" rel="bookmark">You need fresh bridges:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You need fresh bridges: <a href="https://bridges.torproject.org/?transport=obfs2" rel="nofollow">https://bridges.torproject.org/?transport=obfs2</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-19246"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19246" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2013</p>
    </div>
    <a href="#comment-19246">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19246" class="permalink" rel="bookmark">Sorry thats i write here,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry thats i write here, but i have in the normal browser bundle<br />
often app.exe crash. when i close tor. how can i help and  report it? Is there any log that i can e-mail to support?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19247"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19247" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2013</p>
    </div>
    <a href="#comment-19247">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19247" class="permalink" rel="bookmark">WHY ARE YOU BLOCKING</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>WHY ARE YOU BLOCKING CYPHERPUNKS ON TRAC</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19250"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19250" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2013</p>
    </div>
    <a href="#comment-19250">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19250" class="permalink" rel="bookmark">三月 22 *.*:55.671</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>三月 22 *.*:55.671 [Notice] Tor v0.2.4.11-alpha (git-fcd9248387249d68) running on Windows XP with Libevent 2.0.21-stable and OpenSSL 1.0.0k.<br />
三月 22 *.*:55.671 [Notice] Tor can't help you if you use it wrong! Learn how to be safe at <a href="https://www.torproject.org/download/download#warning" rel="nofollow">https://www.torproject.org/download/download#warning</a><br />
三月 22 *.*:55.671 [Notice] This version is not a stable Tor release. Expect more bugs than usual.<br />
三月 22 *.*:55.671 [Notice] Read configuration file "C:\Tor Browser\Data\Tor\torrc".<br />
三月 22 *.*:55.781 [Notice] Opening Socks listener on 127.0.0.1:9150<br />
三月 22 *.*:55.781 [Notice] Opening Control listener on 127.0.0.1:9151<br />
三月 22 *.*:55.781 [Notice] Parsing GEOIP IPv4 file .\Data\Tor\geoip.<br />
三月 22 *.*:01.078 [Warning] Failed to create child process flashproxy-client: 由于应用程序配置不正确，应用程序未能启动。重新安装应用程序可能会纠正这个问题。<br />
三月 22 *.*:01.078 [Warning] Managed proxy at 'flashproxy-client' failed at launch.<br />
三月 22 *.*:01.078 [Warning] Failed to create child process pyobfsproxy: 由于应用程序配置不正确，应用程序未能启动。重新安装应用程序可能会纠正这个问题。<br />
三月 22 *.*:01.078 [Warning] Managed proxy at 'pyobfsproxy' failed at launch.<br />
三月 22 *.*:01.906 [Notice] Bootstrapped 5%: Connecting to directory server.<br />
三月 22 *.*:01.906 [Warning] We were supposed to connect to bridge '0.0.1.0:1' using pluggable transport 'websocket', but we can't find a pluggable transport proxy supporting 'websocket'. This can happen if you haven't provided a ClientTransportPlugin line, or if your pluggable transport proxy stopped running.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19271"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19271" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 25, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19250" class="permalink" rel="bookmark">三月 22 *.*:55.671</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19271">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19271" class="permalink" rel="bookmark">I think your windows</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think your windows firewall blocks flashproxy-client and pyobfsproxy.<br />
可能是你电脑防火墙拦住了它们，建议你检查设置。</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-19295"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19295" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 26, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-19271" class="permalink" rel="bookmark">I think your windows</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-19295">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19295" class="permalink" rel="bookmark">没装防火墙。但是以</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>没装防火墙。但是以前的版本Tor Flashproxy Pyobfsproxy Browser 2.4.7使用正常。</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-19255"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19255" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 23, 2013</p>
    </div>
    <a href="#comment-19255">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19255" class="permalink" rel="bookmark">this new one is slower than</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>this new one is slower than the first one!<br />
but it WORK fine<br />
irani!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19256"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19256" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 23, 2013</p>
    </div>
    <a href="#comment-19256">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19256" class="permalink" rel="bookmark">I&#039;m confused: 
I want to use</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm confused: </p>
<p>I want to use the obfsproxy 'server' (if that's correct) TBB to help those users that need to use obfsproxy, while I would connect directly with my Tor client, is this the bundle I would use?</p>
<p>IIRC, at one time there was a test obfsproxy TBB I used that was an obfsproxy server, I forwarded ports for the server, and it ran well.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19263"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19263" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2013</p>
    </div>
    <a href="#comment-19263">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19263" class="permalink" rel="bookmark">hi again
1-it got a long</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi again<br />
1-it got a long time for connecting to relays<br />
2-it seems have not compatibility with some website elements! it go in (not respond)mode and at least i must exit tor and restart it again and stop the website when it is loading is not complete<br />
irani!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-19264"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-19264" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2013</p>
    </div>
    <a href="#comment-19264">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-19264" class="permalink" rel="bookmark">China here, i&#039;ve been</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>China here, i've been successfully using flashproxy 2.4.7 alpha1 for the last 2 months or so.<br />
Last week i tried to update to the new bundle 2.4.11 alpha1 but it didn't work, giving notice that the proxy server refused the connection.<br />
(First the hardcoded bridges din't work, then i tried the ones i've been using, but with no success.)<br />
I "rolled back" to the previous version (2.4.7 alpha1) and it's again working fine with those same bridges that "failed" with 2.4.11 alpha1.<br />
Keep on the good work, thank you so much for making this progressive software available for anyone who wants to use it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-20925"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-20925" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 10, 2013</p>
    </div>
    <a href="#comment-20925">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-20925" class="permalink" rel="bookmark">[Warning] Not using bridge</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>[Warning] Not using bridge at [scrubbed]: it is in ExcludeNodes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-20970"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-20970" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 11, 2013</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-20925" class="permalink" rel="bookmark">[Warning] Not using bridge</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-20970">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-20970" class="permalink" rel="bookmark">Sounds like you should set</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sounds like you should set ExcludeNodes then?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-28305"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-28305" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 10, 2013</p>
    </div>
    <a href="#comment-28305">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-28305" class="permalink" rel="bookmark">I am in China. Just got</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am in China. Just got 2.4.12-alpha for linux. Getting message "server rejected". Should I try adding more bridges? When I add bridges should I also delete some? If yes, which ones? If I need to add more, how many?</p>
</div>
  </div>
</article>
<!-- Comment END -->
