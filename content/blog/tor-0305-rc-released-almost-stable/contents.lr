title: Tor 0.3.0.5-rc is released: almost stable!
---
pub_date: 2017-04-05
---
author: nickm
---
tags:

tor
alpha
release
release candidate
---
categories:

network
releases
---
_html_body:

<p>Tor 0.3.0.5-rc fixes a few remaining bugs, large and small, in the 0.3.0 release series.<br />
This is the second release candidate in the Tor 0.3.0 series, and has much fewer changes than the first. If we find no new bugs or regressions here, the first stable 0.3.0 release will be nearly identical to it.<br />
You can download the source code from the usual place on the website, but most users should wait for packages to become available over the upcoming weeks. There should be a new Tor Browser alpha release containing Tor 0.3.0.5-rc some time later this month.<br />
Please note: This is a release candidate, but not a stable release. Please expect more bugs than usual. If you want a stable experience, please stick to the stable releases.</p>

<h2>Changes in version 0.3.0.5-rc - 2017-04-05</h2>

<ul>
<li>Major bugfixes (crash, directory connections):
<ul>
<li>Fix a rare crash when sending a begin cell on a circuit whose linked directory connection had already been closed. Fixes bug <a href="https://bugs.torproject.org/21576" rel="nofollow">21576</a>; bugfix on 0.2.9.3-alpha. Reported by Alec Muffett.
  </li>
</ul>
</li>
<li>Major bugfixes (guard selection):
<ul>
<li>Fix a guard selection bug where Tor would refuse to bootstrap in some cases if the user swapped a bridge for another bridge in their configuration file. Fixes bug <a href="https://bugs.torproject.org/21771" rel="nofollow">21771</a>; bugfix on 0.3.0.1-alpha. Reported by "torvlnt33r".
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the March 7 2017 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor bugfix (compilation):
<ul>
<li>Fix a warning when compiling hs_service.c. Previously, it had no exported symbols when compiled for libor.a, resulting in a compilation warning from clang. Fixes bug <a href="https://bugs.torproject.org/21825" rel="nofollow">21825</a>; bugfix on 0.3.0.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (hidden services):
<ul>
<li>Make hidden services check for failed intro point connections, even when they have exceeded their intro point creation limit. Fixes bug <a href="https://bugs.torproject.org/21596" rel="nofollow">21596</a>; bugfix on 0.2.7.2-alpha. Reported by Alec Muffett.
  </li>
<li>Make hidden services with 8 to 10 introduction points check for failed circuits immediately after startup. Previously, they would wait for 5 minutes before performing their first checks. Fixes bug <a href="https://bugs.torproject.org/21594" rel="nofollow">21594</a>; bugfix on 0.2.3.9-alpha. Reported by Alec Muffett.
  </li>
</ul>
</li>
<li>Minor bugfixes (memory leaks):
<ul>
<li>Fix a memory leak when using GETCONF on a port option. Fixes bug <a href="https://bugs.torproject.org/21682" rel="nofollow">21682</a>; bugfix on 0.3.0.3-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>Avoid a double-marked-circuit warning that could happen when we receive DESTROY cells under heavy load. Fixes bug <a href="https://bugs.torproject.org/20059" rel="nofollow">20059</a>; bugfix on 0.1.0.1-rc.
  </li>
</ul>
</li>
<li>Minor bugfixes (tests):
<ul>
<li>Run the entry_guard_parse_from_state_full() test with the time set to a specific date. (The guard state that this test was parsing contained guards that had expired since the test was first written.) Fixes bug <a href="https://bugs.torproject.org/21799" rel="nofollow">21799</a>; bugfix on 0.3.0.1-alpha.
  </li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Update the description of the directory server options in the manual page, to clarify that a relay no longer needs to set DirPort in order to be a directory cache. Closes ticket <a href="https://bugs.torproject.org/21720" rel="nofollow">21720</a>.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-257213"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-257213" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 05, 2017</p>
    </div>
    <a href="#comment-257213">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-257213" class="permalink" rel="bookmark">Using the stable branch, not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Using the stable branch, not familiar to 3.0 features...</p>
<p>is there a big upgrade on hidden services on 3.0+??? no more truncated SHA1 addresss? full sha256 address to thwart fake onion sites??? really waiting for a decent onion address to Ricochet...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-257507"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-257507" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 07, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-257213" class="permalink" rel="bookmark">Using the stable branch, not</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-257507">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-257507" class="permalink" rel="bookmark">Wait... for Ricochet to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wait... for Ricochet to added to Tails "Maybe someday"</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-257290"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-257290" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 06, 2017</p>
    </div>
    <a href="#comment-257290">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-257290" class="permalink" rel="bookmark">Hi Nick, this looks like a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi Nick, this looks like a sweet release, and thanks for the excellent work. I was curious about the latest financial reports for The Tor Project, which only cover the period through the end of 2014 on the website. Do you know when the organization's 2015 and/or 2016 IRS 990 forms or audited financial statements will be made available? Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-257571"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-257571" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 07, 2017</p>
    </div>
    <a href="#comment-257571">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-257571" class="permalink" rel="bookmark">As a User of the &quot;Expert</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As a User of the "Expert Bundle" for Windows, wouldn't be possible to include some update mechanism for this package, too?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-257691"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-257691" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 07, 2017</p>
    </div>
    <a href="#comment-257691">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-257691" class="permalink" rel="bookmark">Please can we have a page</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please can we have a page explaining more about all these different versions. I find the differences between 0.3.05 and 7.02 so confusing.</p>
<p>mark</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-257853"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-257853" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 08, 2017</p>
    </div>
    <a href="#comment-257853">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-257853" class="permalink" rel="bookmark">The ClientOnly option in the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The ClientOnly option in the manual says "This config option is mostly unnecessary: we added it back when we were considering having Tor clients auto-promote themselves to being relays if they were stable and fast enough." Can anyone provide a link to this discussion?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-257947"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-257947" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 08, 2017</p>
    </div>
    <a href="#comment-257947">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-257947" class="permalink" rel="bookmark">What happens to the existing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What happens to the existing onion addresses? Will they no longer work?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-258726"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-258726" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 11, 2017</p>
    </div>
    <a href="#comment-258726">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-258726" class="permalink" rel="bookmark">We could look at developing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We could look at developing an open source community whitelisting/blacklisting initiative that could be contributed to. </p>
<p>If you wanted to block <a href="https://www.torproject.org" rel="nofollow">https://www.torproject.org</a> (just for example) we could use the cryptographic proof of identity to check <a href="http://expyuzz4wqqyqhjn.onion/" rel="nofollow">http://expyuzz4wqqyqhjn.onion/</a> and expand on torrc, or develop a plugin to perhaps add an option to block verified fake onion sites, leaving open the option for community members to ensure that fake reports aren't then overly abused.</p>
<p>As blocklists can be added to routers such as<br />
255.255.255.0 <a href="https://www.torproject.org" rel="nofollow">https://www.torproject.org</a> (as an example),<br />
It may be possible to develop an improved implementation using the cryptographic proof of identity.</p>
<p>Bypass exploiting attempts would have to kept in mind and hardened against. Eventually, with the increasing power capability of routers, it may be possible to develop packages that add support to something like DD-WRT for example.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-258796"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-258796" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 11, 2017</p>
    </div>
    <a href="#comment-258796">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-258796" class="permalink" rel="bookmark">Archives -link on top of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Archives -link on top of page gives: <a href="https://blog.torproject.org/archive" rel="nofollow">https://blog.torproject.org/archive</a></p>
<p><i>Forbidden</i></p>
<p>You don't have permission to access /archive on this server.<br />
Apache Server at blog.torproject.org Port 443</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-260476"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-260476" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 15, 2017</p>
    </div>
    <a href="#comment-260476">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-260476" class="permalink" rel="bookmark">[Moderator: the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>[Moderator: the thread<br />
<a href="https://blog.torproject.org/blog/statement-regarding-dmitry-bogatov" rel="nofollow">https://blog.torproject.org/blog/statement-regarding-dmitry-bogatov</a><br />
doesn't allow comments?  Please post here.]</p>
<p>@ Tor Project people:</p>
<p>The arrest of Dmitry Bogatov is very troubling, particularly following on the heels of similar arrests of Tor exit node operators in the USA.  Also troubling is the bizarre phrasing of the official statement which appears to suggest that TP thinks Bogatov might actually be guilty of a crime, which does not appear to be the case.</p>
<p>Here's what can be gleaned from information on line (about 2/3 from official RU government sources, which have to be read as propaganda similar to US "mainstream media", and 1/3 from Tor node operator lists):</p>
<p>o Bogatov is 25 years old, teaches math at Finance and Law University in Moscow, and also volunteers as a Debian maintainer and Tor exit node operator,</p>
<p>o Through early Apr 2017, Bogatov was running an exit node from his home, fingerprint 2402CD5A0D848D1DCA61EB708CC1FBD4364AB8AE</p>
<p>o RU has about forty exit nodes; each day about a quarter million Russians use Tor,</p>
<p>o In a previous incident, Bogatov's electronic devices were seized by the security police on 2 Feb 2017, but he was apparently not arrested or charged, and after two weeks the devices were apparently returned to him,</p>
<p>o On Sun 26 Mar 2017 about 1000 people, including the popular opposition politician, Alexei Navalny </p>
<p><a href="http://www.bbc.co.uk/news/world-europe-16057045" rel="nofollow">http://www.bbc.co.uk/news/world-europe-16057045</a></p>
<p>and several Western reporters, were arrested during a huge "unauthorized" anti-corruption protest in Moscow:</p>
<p><a href="http://www.novinite.com/articles/179468/Hundreds+Arrested+in+Anti-Corruption+Protests+in+Russia" rel="nofollow">http://www.novinite.com/articles/179468/Hundreds+Arrested+in+Anti-Corru…</a></p>
<p>o this was the largest opposition protest in RU in five years, and appears to have greatly alarmed the oppressive government of V. Putin,</p>
<p>o Navalny has been arrested many times previously, and has been convicted in RU courts under apparently trumped up charges; in Feb 2017 the European Court of Human Rights (ECHR) ordered the Russian government to pay him damages for his mistreatment in 2012</p>
<p><a href="http://www.bbc.co.uk/news/world-europe-38841716" rel="nofollow">http://www.bbc.co.uk/news/world-europe-38841716</a></p>
<p>o After (separate) cases were decided against them, both RU and US governments have stated they will no longer recognize the ECHR,</p>
<p>o Russian opposition groups immediately responded to Navalny's 26 Mar 2017 arrest by calling for another protest to be held the following Sunday, in order to demand Navalny's release,</p>
<p>o On Tue 28 Mar 2017, someone using the handle “Airat Bashirov” allegedly posted, via Tor--- with the exit node in the Tor circuit just happening to be Bogatov's exit node, a message urging people to bring incendiary devices to the protest planned for 2 Apr 2017,</p>
<p>o On Sun 2 Apr 2017, about 100 "unauthorized" protesters attempted to march to the Kremlin, but security police systematically arrested everyone who had been present at the previous protest:</p>
<p><a href="http://www.bbc.co.uk/news/world-europe-39473182" rel="nofollow">http://www.bbc.co.uk/news/world-europe-39473182</a></p>
<p>o Bogatov was apparently not arrested and may not have been present at this protest march, but his node vanished on Wed 5 Apr 2017,</p>
<p>o Bogatov was arrested at his home on Thu 6 Apr 2017, and interrogated all night.  His computers, USB sticks, smartphones, cameras and all other electronic devices were seized (again).</p>
<p>o On Mon 10 Apr 2917, Bogatov was charged in Presnensky Court (Moscow) with inciting terrorism under Article 212.  He pleaded not guilty, but is facing up to 7 years in prison.  The authorities ordered that he be held for another two months in preventative detention while the case against him proceeds,</p>
<p>o “Airat Bashirov” was still posting on 11 Apr 2017, suggesting that he cannot be Bogatov, who has been in nail since 6 Apr 2017.   Bogatov's lawyer. Vladimir Lebedev, has pointed out that the person using this handle has posted messages via Tor which were sent on into "the clearnet" via other exit nodes, including nodes outside Russia; only one was sent on from Bogatov's node.</p>
<p>o The legal issue here is exactly that studied in this EFF whitepaper:</p>
<p><a href="https://www.eff.org/wp/unreliable-informants-ip-addresses-digital-tips-and-police-raids" rel="nofollow">https://www.eff.org/wp/unreliable-informants-ip-addresses-digital-tips-…</a></p>
<p>o Possibly because they recognize that IP address is not a valid means of identification, the Russian authorities specifically stated that Bogatov was "identified" [sic] as the author of the "incitement" post using *stylometry*; however this technique is also dubious at best,</p>
<p>o Lebedev provided surveillance videos (irony!) from supermarkets showing his client shopping with his wife at the time the "incitement" post was allegedly sent from his exit node, suggesting that Bogatov did not send the offensive post.</p>
</div>
  </div>
</article>
<!-- Comment END -->
