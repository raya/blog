title: New Release: Tor Browser 11.0a7
---
pub_date: 2021-09-25
---
author: sysrqb
---
tags:

tor browser
tbb-11.0
tbb
---
categories: applications
---
summary: Tor Browser 11.0a7 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 11.0a7 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/11.0a4/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the latest stable <a href="https://blog.torproject.org/new-release-tor-browser-1054">Windows/macOS/Linux</a> or <a href="https://blog.torproject.org/new-release-tor-browser-1053">Android</a> release instead.</p>
<p>This version updates <b>Firefox</b> to version 78.14.0esr on Windows, macOS, and Linux, and this version updates Firefox's <b>GeckoView component</b> to 92.0 on Android. This version includes important security updates to Firefox on <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-39/">Windows, macOS, and Linux</a>, and <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2021-38/">Android</a>.</p>
<p style="background-color:cornsilk; padding-left:1em; padding-right:1em;"><b>Warning</b>:<br />
Tor Browser Alpha does <em><b>not</b> support <u>version 2 onion services</u></em>. Tor Browser (Stable) will <em><b>stop</b> supporting <u>version 2 onion services</u></em> <em><b>very soon</b></em>. Please see the <a href="https://support.torproject.org/onionservices/v2-deprecation/">deprecation F.A.Q.</a> entry regarding Tor version 0.4.6. Migrate your services and update your bookmarks to version 3 onion services as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master"> Tor Browser 11.0a6:</a></p>
<ul>
<li>All Platforms
<ul>
<li>Update Openssl to 1.1.1l</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 78.14.0esr</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40597">Bug 40597</a>: Implement TorSettings module</li>
</ul>
</li>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40611">Bug 40611</a>: Rebase geckoview patches onto 92.0</li>
</ul>
</li>
<li>OS X
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40358">Bug 40358</a>: Make OpenSSL 1.1.1l buildable for macOS</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-292887"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-292887" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>anon (not verified)</span> said:</p>
      <p class="date-time">October 01, 2021</p>
    </div>
    <a href="#comment-292887">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-292887" class="permalink" rel="bookmark">Hello, I use a system Tor on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello, I use a system Tor on my Windows system. If I ever restart the Tor service, any open Tor Browser instances stop displaying the Tor Circuit information until I close and start that browser again. I found a similar report here: <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40573" rel="nofollow">https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/405…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
