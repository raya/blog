title: Tor Weekly News — April 23rd, 2014
---
pub_date: 2014-04-23
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the sixteenth issue of Tor Weekly News in 2014, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the Tor community.</p>

<h1>Cutting out relays running version 0.2.2.x</h1>

<p>Tor relays running the now ancient Tor 0.2.2.x are scheduled to be <a href="https://bugs.torproject.org/11149" rel="nofollow">removed from the consensus</a>. The change has already been merged in the master branch and will be out with the next Tor 0.2.5 alpha.</p>

<p>Even if most relay operators have been warned, the change has not yet been widely announced. But as three directory authorities are already not voting for the deprecated versions, the downtime of two others while cleaning up after the OpenSSL “Heartbleed” issue was sufficient to get these relays removed from the consensus <a href="https://metrics.torproject.org/network.html?graph=versions&amp;start=2014-04-01&amp;end=2014-04-23#versions" rel="nofollow">for a couple of days</a>, as Roger Dingledine <a href="https://lists.torproject.org/pipermail/tor-relays/2014-April/004422.html" rel="nofollow">explained</a>.</p>

<p>Eventually relays running versions prior to 0.2.3.16-alpha might also be removed from the consensus. “I think 0.2.3.16-alpha’s fix of #6033 makes that one a plausible ’not below this one’ cutoff”, Roger writes in the <a href="https://bugs.torproject.org/11149#comment:7" rel="nofollow">relevant Trac entry</a>.</p>

<p>Relay operators should always make sure to run a <a href="https://consensus-health.torproject.org/#recommendedversions" rel="nofollow">recommended Tor version</a>. The <a href="https://weather.torproject.org/subscribe/" rel="nofollow">Tor Weather service</a> can be used by relay operators to get email notifications if an outdated version is detected.</p>

<h1>Miscellaneous news</h1>

<p>Nathan Freitas <a href="https://lists.mayfirst.org/pipermail/guardian-dev/2014-April/003436.html" rel="nofollow">announced</a> the third (and probably final) release candidate for Orbot 13.0.6: “The big improvements in this build are a fix for the disconnected UI/activity (Tor is on, but UI shows off), and improvements to the transparent proxying iptables scripts”.</p>

<p>The Tails developers put out two calls for testing: the <a href="https://tails.boum.org/news/test_1.0-rc1/index.en.html" rel="nofollow">first</a> is for the first release candidate of Tails 1.0; while the <a href="https://tails.boum.org/news/test_UEFI/index.en.html" rel="nofollow">second</a> is for UEFI support, which “allows you to start Tails using a USB stick on recent hardware, and especially on Mac”. “Test wildly”, and report any bugs you find!</p>

<p>Andrea Shepard <a href="https://lists.torproject.org/pipermail/tor-relays/2014-April/004340.html" rel="nofollow">sent</a> a list of 1777 fingerprints for relays “which have ever turned up as potentially exposed by Heartbleed”. It appears that enough directory authority operators now <a href="https://lists.torproject.org/pipermail/tor-relays/2014-April/004362.html" rel="nofollow">reject relays known to be problematic</a>: sssheep <a href="https://lists.torproject.org/pipermail/tor-talk/2014-April/032762.html" rel="nofollow">reported</a> that the still-vulnerable section of the network was down to 0.01% of the consensus weight.</p>

<p>Mick <a href="https://lists.torproject.org/pipermail/tor-relays/2014-April/004414.html" rel="nofollow">drew attention</a> to the fact that in its current state, <a href="https://www.atagar.com/arm/" rel="nofollow">arm</a> — the command-line relay status monitor — wrongly advises relay operators to run it with the same user as Tor, in order to access information about the relay’s connections. This is in fact a very bad idea, and a <a href="https://bugs.torproject.org/10702" rel="nofollow">ticket</a> is already open to address this issue. Lunar <a href="https://lists.torproject.org/pipermail/tor-relays/2014-April/004412.html" rel="nofollow">detailed</a> the correct method of doing this, which is also explained in the ticket.</p>

<p>On the tor-relays mailing list, David Stainton <a href="https://lists.torproject.org/pipermail/tor-relays/2014-April/004373.html" rel="nofollow">mentioned</a> his <a href="https://github.com/david415/ansible-tor" rel="nofollow">Tor role</a> for the <a href="http://www.ansible.com/" rel="nofollow">Ansible automation tool</a>. David hoped that “relay operators will find this useful for deploying and maintaining large numbers of Tor relays and bridges”. The documentation specifies that it currently works with Debian and Ubuntu systems, and contains several configuration examples.</p>

<p>David Fifield continued his progress on <a href="https://trac.torproject.org/projects/tor/wiki/doc/meek" rel="nofollow">meek</a>, a pluggable transport “that routes your traffic through a third-party web service in a way that should be difficult to block”. David sent a <a href="https://lists.torproject.org/pipermail/tor-dev/2014-April/006718.html" rel="nofollow">call for wider testing</a> of experimental Tor Browser builds and a call for <a href="https://lists.torproject.org/pipermail/tor-dev/2014-April/006719.html" rel="nofollow">reviews of the code</a>. “There are a lot of components that make up the meek transport […] This is your chance to get in on the ground floor of a new transport!”</p>

<p>Ximin Luo <a href="https://lists.torproject.org/pipermail/tor-dev/2014-April/006689.html" rel="nofollow">raised</a> several points regarding how “indirect” pluggable transports like <a href="http://crypto.stanford.edu/flashproxy/" rel="nofollow">flashproxy</a> or meek are currently handled by Tor. Whereas obfs3 or ScrambleSuit connect directly to the specified peer, transforming the data flow along the way, Ximin describes meek and flashproxy as providing “the metaphor of connecting to a global homogeneous service”. The latter being “incompatible with the metaphor of connecting to a specific endpoint”. Solutions on how to make the design, code, and configuration better are up for discussion.</p>

<p>Nicolas Vigier submitted his <a href="https://lists.torproject.org/pipermail/tor-reports/2014-April/000510.html" rel="nofollow">status report for March</a>.</p>

<p>Philipp Winter <a href="https://blog.torproject.org/blog/call-papers-foci14-workshop" rel="nofollow">relayed</a> the call for papers for the <a href="https://www.usenix.org/conference/foci14/call-for-papers" rel="nofollow">4th USENIX Workshop on Free and Open Communications on the Internet</a>. The workshop will be held on August 18th, and should bring together the wider community of researchers and practitioners interested in Tor and other ways to study, detect, or circumvent censorship. Papers have to be submitted before May 13th.</p>

<p>Fabio Pietrosanti <a href="https://lists.torproject.org/pipermail/tor-dev/2014-April/006723.html" rel="nofollow">wondered</a> whether anyone had “ever tried to start Tor from a Python application using Ctypes”, making it possible to “sandbox the Python application using AppArmor without enabling any kind of execve() call”.</p>

<h1>Tor help desk roundup</h1>

<p>Many people email the Tor Help Desk from behind restrictive university firewalls that require them to connect using a proxy. Often these firewalls, Cyberoam and Fortiguard are two examples, use Deep Packet Inspection and block Tor traffic. Unfortunately Tor Browser users can’t use a proxy to connect to the internet and also use a pluggable transport. The Tor Browser team plans to include this capability in a <a href="https://bugs.torproject.org/8402" rel="nofollow">future release</a>.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, harmony, Matt Pagan, and an anonymous contributor.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

