title: Introducing Bastet, Our New Directory Authority
---
pub_date: 2017-11-02
---
author: tommy
---
tags: directory authority
---
categories: network
---
summary:

We added a new directory authority last month, increasing the diversity and stability of the directory authority system. The latest authority, named “Bastet” after the ancient Egyptian goddess, is run by Tor contributor Stefani. 
---
_html_body:

<p><em>By Gunkarta (Own work) [<a href="https://creativecommons.org/licenses/by-sa/3.0)">CC BY-SA 3.0</a>], via Wikimedia Commons</em></p>
<p>Every day, millions of people use Tor to protect their privacy and circumvent censorship. Their connections are passed through the Tor network, which separates users’ identities from their online activity.</p>
<p>Thanks to <a href="https://metrics.torproject.org/networksize.html">Tor Metrics data</a>, we know that the network comprises over 6,000 relays. But how does the network choose the route that Tor traffic takes through the network? How does every Tor user get the same information on relays? How does Tor authenticate the connection to any given relay?</p>
<h3><b>Relaying relay information</b></h3>
<p>The answer is through directory authorities — dedicated servers which tell Tor clients which relays make up the Tor network. Information about these directory authorities, located around the world and maintained by super trusted we-know-you-and-have-had-many-beers-with-you Tor volunteers, are hard-coded into Tor. Every hour, these volunteer-run directory authorities vote on and reach a consensus on the relays that make up the Tor network.</p>
<p>You can see more information on our directory authorities with <a href="https://atlas.torproject.org/#search/flag:authority">Atlas</a>, and read more about what they do on <a href="https://www.torproject.org/docs/faq.html.en#KeyManagement">Tor’s FAQ</a>. </p>
<p>We added a new directory authority last month, increasing the diversity and stability of the directory authority system. The latest authority, named “Bastet” after the ancient Egyptian goddess, is run by Tor contributor Stefani. </p>
<h3>Get involved</h3>
<p>Tor relies on volunteers to help us make the Tor network faster, more robust, and decentralized. You can lend a hand by <a href="https://www.torproject.org/docs/tor-doc-relay.html.en">running a relay</a> or <a href="https://donate.torproject.org/pdr">donating</a> to the Tor Project.</p>

---
_comments:

<a id="comment-272287"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272287" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>LotusAlive (not verified)</span> said:</p>
      <p class="date-time">November 02, 2017</p>
    </div>
    <a href="#comment-272287">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272287" class="permalink" rel="bookmark">Thank youssss, so much...♡  …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank youssss, so much...♡  (It's too late for me, but i HAD TO...(*♡*)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272294"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272294" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 02, 2017</p>
    </div>
    <a href="#comment-272294">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272294" class="permalink" rel="bookmark">&quot;super trusted we-know-you…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"super trusted we-know-you-and-have-had-many-beers-with-you"</p>
<p>??!<br />
From which brand?<br />
;-)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272295"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272295" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Mike (not verified)</span> said:</p>
      <p class="date-time">November 02, 2017</p>
    </div>
    <a href="#comment-272295">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272295" class="permalink" rel="bookmark">Thanks Tor project!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks Tor project!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-272296"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272296" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 02, 2017</p>
    </div>
    <a href="#comment-272296">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272296" class="permalink" rel="bookmark">I hope &quot;we-know-you-and-have…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I hope "we-know-you-and-have-had-many-beers-with-you" is enough (-:</p>
<p><a href="https://www.burojansen.nl/bvd-aivd/dutch-secret-service-tries-to-recruit-tor-admin/" rel="nofollow">https://www.burojansen.nl/bvd-aivd/dutch-secret-service-tries-to-recrui…</a></p>
<p><a href="https://bugzilla.mozilla.org/show_bug.cgi?id=1408647" rel="nofollow">https://bugzilla.mozilla.org/show_bug.cgi?id=1408647</a><br />
<a href="http://wetten.overheid.nl/BWBR0039896/2017-09-01" rel="nofollow">http://wetten.overheid.nl/BWBR0039896/2017-09-01</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272302"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272302" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>st (not verified)</span> said:</p>
      <p class="date-time">November 03, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272296" class="permalink" rel="bookmark">I hope &quot;we-know-you-and-have…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-272302">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272302" class="permalink" rel="bookmark">it was commented yet few…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>it was commented yet few months ago on this blog if i remember well : rotten cops (free-lance maybe &amp; certainly untrust _ dirty).<br />
- denouncing them is the best attitude (i am not sure they were allowed _ illegal means that this couple should go &amp; be behind the bars of a jail).<br />
- disallowing backdoor 'compromised' cert &amp; disapproving  'deviant' behavior is a reflex depending on the feeling of honesty ... Tor is built on the human being , hopefully in this case ...<br />
how-to purge all these shit*cert ?</p>
<p>- the links are redirected to the original article (http ?) written in dutch : unreachable by tor , ok using startpage.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272304"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272304" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 03, 2017</p>
    </div>
    <a href="#comment-272304">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272304" class="permalink" rel="bookmark">What about the previously …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about the previously "announced" new Directory Authority controlled by the Calyx Institute guru Nicholas Merrill? Is there any ticket for it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272320"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272320" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 03, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272304" class="permalink" rel="bookmark">What about the previously …</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-272320">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272320" class="permalink" rel="bookmark">https://trac.torproject.org…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://trac.torproject.org/projects/tor/ticket/21915" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/21915</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272313"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272313" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Indra (not verified)</span> said:</p>
      <p class="date-time">November 03, 2017</p>
    </div>
    <a href="#comment-272313">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272313" class="permalink" rel="bookmark">These multifunctional…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>These multifunctional devices in the hands of the Cat-head God were used for transfer of knowledges and thoughts directly to acceptors mind. Also used as weapon and control different kind of energy and nature elements. Not all "gods" had at their disposal such tools. For example one famous demonic god, founder of three religions did not have such tools and organized an operation to steal them. the consequence of this theft and deceit and how these tools were used you can observe in modern society, its development trend, morals and other spheres.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272615"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272615" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 18, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272313" class="permalink" rel="bookmark">These multifunctional…</a> by <span>Indra (not verified)</span></p>
    <a href="#comment-272615">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272615" class="permalink" rel="bookmark">@ Indra: …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ Indra: </p>
<p>Didn't know that, very cool!</p>
<p>I too was struck by the appropriateness of the name.</p>
<p>In ancient Egypt, festivals honoring Bastet--- a goddess who in later times was regarded as the defender of Maat (truth and righteousness)--- were according to ancient Greek tourists notable for the enthusiastic consumption of considerable quantities of good Egyptian beer.  Imagine thousands of ordinary people boating down the Nile, singing uproariously, and pulling up to attend joyful festivities full of music, dancing, food and drink.  And in corners here and there, someone incautiously scribbling rude anonymous graffiti injurious to the dignity of palace and priest.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-272465"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272465" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Arnab (not verified)</span> said:</p>
      <p class="date-time">November 11, 2017</p>
    </div>
    <a href="#comment-272465">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272465" class="permalink" rel="bookmark">I would like to join all of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I would like to join all of you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272487"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272487" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">November 13, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-272465" class="permalink" rel="bookmark">I would like to join all of…</a> by <span>Arnab (not verified)</span></p>
    <a href="#comment-272487">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272487" class="permalink" rel="bookmark">Great and you are welcome…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great and you are welcome. What do you want to work with?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-272695"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272695" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-272695">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272695" class="permalink" rel="bookmark">Not the time and place, I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not the time and place, I know, but speaking of collaboration, here's one Tor Project should probably follow closely, or even join:</p>
<p>eff.org<br />
Why We're Helping The Stranger Unseal Electronic Surveillance Records<br />
Amul Kalia<br />
20 Nov 2017</p>
<p>&gt; Consider this: Deputy Attorney General Rod Rosenstein has been going around talking about “responsible encryption” for some time now— proselytizing for encryption that’s somehow only accessible by the government—something we all know to be unworkable. If the Department of Justice (DOJ) is taking this aggressive public position about what kind of access it should have to user data, it begs the question—what kind of technical assistance from companies and orders for user data is the DOJ demanding in sealed court documents? EFF’s client The Stranger, a Seattle-based newspaper, has filed a petition with one court to find out.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-272661"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-272661" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Bill  (not verified)</span> said:</p>
      <p class="date-time">November 20, 2017</p>
    </div>
    <a href="#comment-272661">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-272661" class="permalink" rel="bookmark">Firefox updated to v.57 and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Firefox updated to v.57 and it's wide open , naked it seems.  Noscript was disabled as an add on,, saying it will be available in the future.  I've been a tech since the 80's, I'm concerned that everything is reducing us all to completely predictive data file, and that the information gleaned will be used by anyone with $35 who wants to deny us employment, and anything else in life, because of who we are.  Tor needs to keep it's rights to Firefox use but without giving away the farm to the Google spymaster.  Anyone want to bet that their Google, Facebook, Twitter personas aren't being continuously sold to foreigners, corporations, and creeps?</p>
</div>
  </div>
</article>
<!-- Comment END -->
