title: Tor 0.3.0.4-rc is released
---
pub_date: 2017-03-01
---
author: nickm
---
tags:

tor
alpha
release
release candidate
---
categories:

network
releases
---
_html_body:

<p>Tor 0.3.0.4-rc fixes some remaining bugs, large and small, in the 0.3.0 release series, and introduces a few reliability features to keep them from coming back.<br />
This is the first release candidate in the Tor 0.3.0 series. If we find no new bugs or regressions here, the first stable 0.3.0 release will be nearly identical to it.</p>

<p>You can download the source code from the usual place on the website, but most users should wait for packages to become available over the upcoming weeks.</p>

<p>Please note: This is a release candidate, but not a stable release. Please expect more bugs than usual. If you want a stable experience, please stick to the stable releases.</p>

<h2>Changes in version 0.3.0.4-rc - 2017-03-01</h2>

<ul>
<li>Major bugfixes (bridges):
<ul>
<li>When the same bridge is configured multiple times with the same identity, but at different address:port combinations, treat those bridge instances as separate guards. This fix restores the ability of clients to configure the same bridge with multiple pluggable transports. Fixes bug <a href="https://bugs.torproject.org/21027" rel="nofollow">21027</a>; bugfix on 0.3.0.1-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (hidden service directory v3):
<ul>
<li>Stop crashing on a failed v3 hidden service descriptor lookup failure. Fixes bug <a href="https://bugs.torproject.org/21471" rel="nofollow">21471</a>; bugfixes on tor-0.3.0.1-alpha.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Major bugfixes (parsing):
<ul>
<li>When parsing a malformed content-length field from an HTTP message, do not read off the end of the buffer. This bug was a potential remote denial-of-service attack against Tor clients and relays. A workaround was released in October 2016, to prevent this bug from crashing Tor. This is a fix for the underlying issue, which should no longer matter (if you applied the earlier patch). Fixes bug <a href="https://bugs.torproject.org/20894" rel="nofollow">20894</a>; bugfix on 0.2.0.16-alpha. Bug found by fuzzing using AFL (<a href="http://lcamtuf.coredump.cx/afl/" rel="nofollow">http://lcamtuf.coredump.cx/afl/</a>).
  </li>
<li>Fix an integer underflow bug when comparing malformed Tor versions. This bug could crash Tor when built with --enable-expensive-hardening, or on Tor 0.2.9.1-alpha through Tor 0.2.9.8, which were built with -ftrapv by default. In other cases it was harmless. Part of TROVE-2017-001. Fixes bug <a href="https://bugs.torproject.org/21278" rel="nofollow">21278</a>; bugfix on 0.0.8pre1. Found by OSS-Fuzz.
  </li>
</ul>
</li>
<li>Minor feature (protocol versioning):
<ul>
<li>Add new protocol version for proposal 224. HSIntro now advertises version "3-4" and HSDir version "1-2". Fixes ticket <a href="https://bugs.torproject.org/20656" rel="nofollow">20656</a>.
  </li>
</ul>
</li>
<li>Minor features (directory authorities):
<ul>
<li>Directory authorities now reject descriptors that claim to be malformed versions of Tor. Helps prevent exploitation of bug <a href="https://bugs.torproject.org/21278" rel="nofollow">21278</a>.
  </li>
<li>Reject version numbers with components that exceed INT32_MAX. Otherwise 32-bit and 64-bit platforms would behave inconsistently. Fixes bug <a href="https://bugs.torproject.org/21450" rel="nofollow">21450</a>; bugfix on 0.0.8pre1.
  </li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the February 8 2017 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor features (reliability, crash):
<ul>
<li>Try better to detect problems in buffers where they might grow (or think they have grown) over 2 GB in size. Diagnostic for bug <a href="https://bugs.torproject.org/21369" rel="nofollow">21369</a>.
  </li>
</ul>
</li>
<li>Minor features (testing):
<ul>
<li>During 'make test-network-all', if tor logs any warnings, ask chutney to output them. Requires a recent version of chutney with the 21572 patch. Implements 21570.
  </li>
</ul>
</li>
<li>Minor bugfixes (certificate expiration time):
<ul>
<li>Avoid using link certificates that don't become valid till some time in the future. Fixes bug <a href="https://bugs.torproject.org/21420" rel="nofollow">21420</a>; bugfix on 0.2.4.11-alpha
  </li>
</ul>
</li>
<li>Minor bugfixes (code correctness):
<ul>
<li>Repair a couple of (unreachable or harmless) cases of the risky comparison-by-subtraction pattern that caused bug <a href="https://bugs.torproject.org/21278" rel="nofollow">21278</a>.
  </li>
<li>Remove a redundant check for the UseEntryGuards option from the options_transition_affects_guards() function. Fixes bug <a href="https://bugs.torproject.org/21492" rel="nofollow">21492</a>; bugfix on 0.3.0.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (directory mirrors):
<ul>
<li>Allow relays to use directory mirrors without a DirPort: these relays need to be contacted over their ORPorts using a begindir connection. Fixes one case of bug <a href="https://bugs.torproject.org/20711" rel="nofollow">20711</a>; bugfix on 0.2.8.2-alpha.
  </li>
<li>Clarify the message logged when a remote relay is unexpectedly missing an ORPort or DirPort: users were confusing this with a local port. Fixes another case of bug <a href="https://bugs.torproject.org/20711" rel="nofollow">20711</a>; bugfix on 0.2.8.2-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (guards):
<ul>
<li>Don't warn about a missing guard state on timeout-measurement circuits: they aren't supposed to be using guards. Fixes an instance of bug <a href="https://bugs.torproject.org/21007" rel="nofollow">21007</a>; bugfix on 0.3.0.1-alpha.
  </li>
<li>Silence a BUG() warning when attempting to use a guard whose descriptor we don't know, and make this scenario less likely to happen. Fixes bug <a href="https://bugs.torproject.org/21415" rel="nofollow">21415</a>; bugfix on 0.3.0.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (hidden service):
<ul>
<li>Pass correct buffer length when encoding legacy ESTABLISH_INTRO cells. Previously, we were using sizeof() on a pointer, instead of the real destination buffer. Fortunately, that value was only used to double-check that there was enough room--which was already enforced elsewhere. Fixes bug <a href="https://bugs.torproject.org/21553" rel="nofollow">21553</a>; bugfix on 0.3.0.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Fix Raspbian build issues related to missing socket errno in test_util.c. Fixes bug <a href="https://bugs.torproject.org/21116" rel="nofollow">21116</a>; bugfix on tor-0.2.8.2. Patch by "hein".
  </li>
<li>Rename "make fuzz" to "make test-fuzz-corpora", since it doesn't actually fuzz anything. Fixes bug <a href="https://bugs.torproject.org/21447" rel="nofollow">21447</a>; bugfix on 0.3.0.3-alpha.
  </li>
<li>Use bash in src/test/test-network.sh. This ensures we reliably call chutney's newer tools/test-network.sh when available. Fixes bug <a href="https://bugs.torproject.org/21562" rel="nofollow">21562</a>; bugfix on 0.2.9.1-alpha.
  </li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Small fixes to the fuzzing documentation. Closes ticket <a href="https://bugs.torproject.org/21472" rel="nofollow">21472</a>.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-238419"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-238419" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 02, 2017</p>
    </div>
    <a href="#comment-238419">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-238419" class="permalink" rel="bookmark">THANKS A LOT!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>THANKS A LOT!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-238918"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-238918" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 03, 2017</p>
    </div>
    <a href="#comment-238918">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-238918" class="permalink" rel="bookmark">How much memory should</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How much memory should fragile-hardened tor take up when running as a client?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-240264"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-240264" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 05, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-238918" class="permalink" rel="bookmark">How much memory should</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-240264">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-240264" class="permalink" rel="bookmark">In my case it took up a few</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In my case it took up a few terrabytes before it crashed the system</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-239018"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-239018" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 03, 2017</p>
    </div>
    <a href="#comment-239018">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-239018" class="permalink" rel="bookmark">Regarding this quote:
When</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Regarding this quote:<br />
<cite>When the same bridge is configured multiple times with the same identity, but at different address:port combinations, treat those bridge instances as separate guards. This fix restores the ability of clients to configure the same bridge with multiple pluggable transports. Fixes bug 21027</cite></p>
<p>Yes, but are you able to make tor mark those "semi-separate" bridge instances as belonging to the same family, since they are the same guy? Otherwise, a malicious bridge may somehow end up in multiple circuits of the same user, more often than acceptable, and observe more traffic than it should?</p>
</div>
  </div>
</article>
<!-- Comment END -->
