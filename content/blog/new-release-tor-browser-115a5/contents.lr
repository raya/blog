title: New Alpha Release: Tor Browser 11.5a5 (Android)
---
pub_date: 2022-03-03
---
author: aguestuser
---
categories:

applications
releases
---
summary: Tor Browser 11.5a5 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a5 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a5/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-01/) to Firefox.

Tor Browser 11.5a5 updates Firefox to 96.3.0 and includes bugfixes and stability improvements. Notably: we fixed a bug that was preventing users of Android Q and later from downloading files.

We also use the opportunity to update various components of Tor Browser, bumping Tor to 0.4.7.4-alpha and NoScript to 11.3.4, while switching to the latest version of Go (1.17.7) for building our Go-related projects.

As usual, please report any issues you find in this alpha version, so we can fix them for the next upcoming major stable release (11.5).

The full changelog since [Tor Browser 11.5a3](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- Android
  - Update Firefox to 96.3.0
  - Update Tor to 0.4.7.4-alpha
  - Update NoScript to 11.3.4
  - [Bug android-components#40075](https://gitlab.torproject.org/tpo/applications/android-components/-/issues/40075): Fix problem preventing file downloads on Android Q and above
  - [Bug tor-browser-build#40432](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40432): Support bug reports by displaying Android Components commit hash in "About Tor Browser" display
- Build System
  - Android
    - Update Go to 1.17.7
    - [Bug tor-browser-build#40056](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40056): Download .aar and .jar files for all .pom files
    - [Bug tor-browser-build#40385](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40385): Update components for mozilla95
    - [Bug tor-browser-build#40418](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40418): Update components for mozilla96
    - [Bug tor-browser-build#40435](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40435): Remove default obfs4 bridge "deusexmachina"
    - [Bug tor-browser-build#40436](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40436): Use intermediate files for default bridge lines
    - [Bug tor-browser-build#40438](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40438): Use the same list of bridges for desktop and android
