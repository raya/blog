title: Tails 0.17 is out!
---
pub_date: 2013-02-25
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 0.17, is out.</p>

<p>All users must upgrade as soon as possible.</p>

<p><a href="https://tails.boum.org/download/" rel="nofollow">Download it now.</a></p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>New features
<ul>
<li>Install the KeePassX password manager, with a configuration and <a href="https://tails.boum.org/doc/encryption_and_privacy/manage_passwords/" rel="nofollow">documentation</a> that makes it easy to persist the password database.</li>
</ul>
</li>
<li>Iceweasel
<ul>
<li>Upgrade to Iceweasel 17.0.3esr-1+tails1~bpo60+1.</li>
<li>Do not allow listing all available fonts.</li>
<li>Improve default spellchecker dictionary selection.</li>
<li>Disable the add-ons automatic update feature.</li>
<li>Remove NoScript click-to-play confirmation.</li>
<li>Sync some prefs set by Torbutton, to be ready when it stops setting these.</li>
<li>Disable navigation timing.</li>
<li>Disable SPDY.</li>
<li>More aggressive iceweasel HTTP pipelining settings.</li>
<li>Enable WebGL (as click-to-play only).</li>
<li>Disable network.http.connection-retry-timeout.</li>
<li>Disable full path information for plugins.</li>
<li>Remove NoScript blocks of WebFonts.</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Upgrade to live-boot 3.0~b11-1 and live-config 3.0.12-1.</li>
<li>Don't add "quiet" to the kernel command-line ourselves.</li>
<li>Upgrade I2P to 0.9.4.</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Many bugfixes brought by the Debian Squeeze 6.0.7 point-release.</li>
<li>Use the regular GnuPG agent + pinentry-gtk2 instead of Seahorse as a GnuPG agent. This fixes usage of OpenPGP in Claws Mail, and brings support for OpenPGP smartcards.</li>
<li>Enable I2P hidden mode. Else, killing I2P ungracefully is bad for the I2P network.</li>
<li>Add shutdown and reboot launchers to the menu. This workarounds the lack of a shutdown helper applet in camouflage mode.</li>
<li>Remove Pidgin's MXit and Sametime support to workaround security flaws.</li>
</ul>
</li>
<li>Hardware support
<ul>
<li>Install recent Intel and AMD microcode.</li>
<li>Install firmware loader for Qualcomm Gobi USB chipsets.</li>
<li>Upgrade barry to 0.18.3-5~bpo60+1.</li>
</ul>
</li>
<li>Localization
<ul>
<li>Tails USB Installer: update translations for Arabic, Czech, German, Hebrew, Polish and Spanish.</li>
<li>tails-greeter: update Spanish and French translations, new Polish translation.</li>
<li>tails-persistence-setup: update translations for Arabic, Bulgarian, Spanish, French, Dutch, Polish and Chinese.</li>
<li>WhisperBack: update Spanish and Korean translations, import new Polish translation.</li>
</ul>
</li>
</ul>

<p>Plus the usual bunch of bug reports and minor improvements.</p>

<p>See the <a href="http://git.immerda.ch/?p=amnesia.git;a=blob_plain;f=debian/changelog;hb=refs/tags/0.17" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>I want to try it / to upgrade!</strong></p>

<p>See the <a href="https://tails.boum.org/getting_started/" rel="nofollow">Getting started</a> page.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is scheduled for April 9. It will probably be a minor, bugfix only one.</p>

<p>Have a look to our <a href="https://tails.boum.org/contribute/roadmap/" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Would you want to help? As explained in our <a href="https://tails.boum.org/contribute/" rel="nofollow">"how to contribute" documentation</a>, there are many ways <strong>you</strong> can contribute to Tails. If you want to help, come talk to us!</p>

