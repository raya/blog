title: New Alpha Release: Tor Browser 12.0a2 (Android, Windows, macOS, Linux)
---
pub_date: 2022-09-10
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 12.0a2 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 12.0a2 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/12.0a2/).

This marks our first alpha on the Firefox ESR 102 series, and our first Android released based on the Firefox ESR (Extended Support Release) series.

In the past, our Tor Browser for Android releases have been based on the Firefox Rapid Release schedule. Going forward, Tor Browser for Android will be based on the latest Firefox ESR (same as our Windows, macOS and Linux releases) and we will be back-porting Android-specific security updates from the Rapid Release branches.

This updated release schedule should allow us to improve the general stability of Tor Browser for Android and be more confident in our releases going forward.

Tor Browser 12.0a2 updates Firefox on Android, Windows, macOS, and Linux to 102.2.0esr.

We use this opportunity to update various other components of Tor Browser as well :
- Tor 0.4.7.10
- Tor Launcher 0.2.39 (Desktop only)
- NoScript 11.4.10
- Go 1.18.5 (Android only)

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-34/) to Firefox. We also backport the following Android-specific security updates:
- [CVE-2022-36317](https://www.mozilla.org/en-US/security/advisories/mfsa2022-28/#CVE-2022-36317)
- [CVE-2022-38474](https://www.mozilla.org/en-US/security/advisories/mfsa2022-33/#CVE-2022-38474)

Notably, we have also enabled HTTPS-Only mode for Tor Browser for Android (this feature is already enabled for Desktop). This feature, when enabled, overrides HTTPS-Everywhere functionality. For now this can be reverted in the 'Settings > Privacy and security' pane( see: https://gitlab.torproject.org/tpo/applications/fenix/-/merge_requests/151 ).

The full changelog since [Tor Browser 12.0a1](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=main) is:

- All Platforms
  - Update Firefox to 102.2.0esr
  - Update Tor to 0.4.7.10
  - Update NoScript to 11.4.10
  - Update Translations
  - [Bug tor-browser#40242](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40242): Tor Browser has two default bridges that share a fingerprint, and Tor ignores one
- Windows + macOS + Linux
  - Update Tor-Launcher to 0.2.39
  - Update Manual
  - [Bug tor-browser-build#40580](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40580): Add support for uk (ukranian) locale
  - [Bug tor-browser-buid#40595](https://gitlab.torproject.org/tpo/applications/tor-browser-buid/-/issues/40595): Migrate to 102 on desktop
  - [Bug tor-browser#41075](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41075): The Tor Browser is showing caution sign but your document said it won't
  - [Bug tor-browser#41089](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41089): Add tor-browser build scripts + Makefile to tor-browser
- macOS
  - [Bug tor-browser#41108](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41108): Remove privileged macOS installation from 102
- Android
  - [Bug fenix#40225](https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40225): Bundled extensions don't get updated with Android Tor Browser updates (they stay stuck at the first installed version)
  - [Bug tor-browser#41094](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41094): Enable HTTPS-Only Mode by default in Tor Browser Android
  - [Bug tor-browser#41156](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41156): User-installed addons are broken on Android
  - [Bug tor-browser#41166](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41166): Backport fix for CVE-2022-36317: Long URL would hang Firefox for Android (Bug 1759951)
  - [Bug tor-browser#41167](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41167): Backport fix for CVE-2022-38474: Recording notification not shown when microphone was recording on Android (Bug 1719511)
- Build System
  - All Platforms
    - [Bug tor-browser-build#40407](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40407): Bump binutils version to pick up security improvements for Windows users
    - [Bug tor-browser-build#40591](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40591): Rust 1.60 not working to build 102 on Debian Jessie
    - [Bug tor-browser-build#40592](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40592): Consider re-using our LLVM/Clang to build Rust
    - [Bug tor-browser-build#40593](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40593): Update signing scripts to take into account new project names and layout
    - [Bug tor-browser-build#40607](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40607): Add alpha-specific release prep template
    - [Bug tor-browser-build#40610](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40610): src-*.tar.xz tarballs are missing in https://dist.torproject.org/torbrowser/12.0a1/
    - [Bug tor-browser-build#40612](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40612): Migrate Release Prep template to Release Prep - Stable
  - Windows + macOS + Linux
    - [Bug tor-browser#41099](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41099): Update+comment the update channels in update_responses.config.yaml
  - Windows
    - [Bug tor-browser-buid#29318](https://gitlab.torproject.org/tpo/applications/tor-browser-buid/-/issues/29318): Use Clang for everything on Windows
    - [Bug tor-browser-build#29321](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/29321): Use mingw-w64/clang toolchain to build tor
    - [Bug tor-browser-build#29322](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/29322): Use mingw-w64/clang toolchain to build OpenSSL
    - [Bug tor-browser-build#40409](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40409): Upgrade NSIS to 3.08
  - macOS
    - [Bug tor-browser-build#40605](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40605): Reworked the macOS toolchain creation
    - [Bug tor-browser-build#40620](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40620): Update macosx-sdk to 11.0
  - Linux
    - [Bug tor-browser-build#31321](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/31321): Add cc -&gt; gcc link to projects/gcc
  - Android
    - Update Go to 1.18.5
    - [Bug tor-browser-build#40574](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40574): Improve tools/signing/android-signing
    - [Bug tor-browser-build#40582](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40582): Prepared TBA to use Mozilla 102 components
    - [Bug tor-browser-build#40604](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40604): Bug 40604: Fix binutils build on android

