title: New Release: Tor Browser 11.5.3 (Android)
---
pub_date: 2022-08-30
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5.3 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5.3 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5.3/).

The full changelog since [Tor Browser 11.5.2](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=tbb-11.5.3-build1) is:

- Android
  - [Bug fenix#40225](https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40225): Bundled extensions don't get updated with Android Tor Browser updates (they stay stuck at the first installed version)
  - [Bug tor-browser#41156](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41156): User-installed addons are broken on Android
