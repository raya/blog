title: New Release: Tor Browser 10.5a5 (Android Only)
---
pub_date: 2020-12-07
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.5
---
categories: applications
---
_html_body:

<p>Tor Browser 10.5a5 is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a5/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the latest stable release for <a href="https://blog.torproject.org/new-release-tor-browser-1005">Android</a> instead.</p>
<p>This release updates Fenix to 84.0.0-beta.2. Additionally, we update Tor to 0.4.5.2-alpha and HTTPS Everywhere to 2020.11.17.</p>
<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">full changelog</a> since Tor Browser 10.5a4 is:</p>
<ul>
<li>Android
<ul>
<li>Update Fenix to 84.0.0-beta.2</li>
<li>Update HTTPS Everywhere to 2020.11.17</li>
<li>Update Tor to 0.4.5.2-alpha</li>
<li>Translations update</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40159">Bug 40159</a>: Update snowflake to ece43cbf</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40236">Bug 40236</a>: Rebase tor-browser patches to 84.0b1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40258">Bug 40258</a>: Rebase tor-browser patches to 84.0b7</li>
<li><a href="https://bugs.torproject.org/tpo/applications/fenix/40119">Bug 40119</a>: Rebase Fenix patches to Fenix 84 beta 2</li>
<li><a href="https://bugs.torproject.org/tpo/applications/android-components/40027">Bug 40027</a>: Rebase android-components patches for Fenix 84 beta 2 builds</li>
</ul>
</li>
<li>Build System
<ul>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40081">Bug 40081</a>: Build Mozilla code with --enable-rust-simd</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40128">Bug 40128</a>: Allow updating Fenix allowed_addons.json</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40140">Bug 40140</a>: Create own Gradle project</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40147">Bug 40147</a>: Remove RANLIB workaround once we pick up 0.4.5.2-alpha</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40155">Bug 40155</a>: Update toolchain for Fenix 84</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40156">Bug 40156</a>: Update Fenix and dependencies to 84.0.0-beta2</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40161">Bug 40161</a>: Pick up Go 1.15.5</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40163">Bug 40163</a>: Bug 40163: Avoid checking hash of .pom files</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40166">Bug 40166</a>: Update apt cache before calling pre_pkginst in container-image config</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40171">Bug 40171</a>: Include all uniffi-rs artifacts into application-services</li>
</ul>
</li>
</ul>
</li>
</ul>

