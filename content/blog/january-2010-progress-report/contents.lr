title: January 2010 Progress Report
---
pub_date: 2010-02-13
---
author: phobos
---
tags:

progress report
translations
bug fixes
releases
advocacy
enhancements
feature removals
---
categories:

advocacy
localization
releases
reports
---
_html_body:

<p><strong>New releases, new hires, new funding</strong><br />
On January 19, 2010 we released the latest in the -stable series, Tor 0.2.1.22-stable.<br />
Tor 0.2.1.22 fixes a critical privacy problem in bridge directory authorities -- it would tell you its whole history of bridge descriptors if you make the right directory request. This stable update also rotates two of the seven v3 directory authority keys and locations.<br />
<strong>Directory authority changes</strong>:</p>

<ul>
<li>Rotate keys (both v3 identity and relay identity) for moria1 and gabelmoo.</li>
</ul>

<p><strong>Major bugfixes:</strong></p>

<ul>
<li>Stop bridge directory authorities from answering dbg-stability.txt directory queries, which would let people fetch a list of all bridge identities they track. Bugfix on 0.2.1.6-alpha.</li>
</ul>

<p>On January 19, 2010, we released the latest in the -alpha series, Tor 0.2.2.7-alpha.<br />
Tor 0.2.2.7-alpha fixes a huge client-side performance bug, as well as laying the groundwork for further relay-side performance fixes. It also starts cleaning up client behavior with respect to the EntryNodes, ExitNodes, and StrictNodes config options. This release also rotates two directory authority keys, due to a security breach of some of the Torproject servers.<br />
<strong>Directory authority changes:</strong></p>

<ul>
<li>Rotate keys (both v3 identity and relay identity) for moria1 and gabelmoo.</li>
</ul>

<p><strong>Major features (performance):</strong>
</p>

<ul>
<li>We were selecting our guards uniformly at random, and then weighting which of our guards we’d use uniformly at random. This imbalance meant that Tor clients were severely limited on throughput (and probably latency too) by the first hop in their circuit. Now we select guards weighted by currently advertised bandwidth. We also automatically discard guards picked using the old algorithm. Fixes bug 1217; bugfix on 0.2.1.3-alpha. Found by Mike Perry.</li>
<li>When choosing which cells to relay first, relays can now favor circuits that have been quiet recently, to provide lower latency for low-volume circuits. By default, relays enable or disable this feature based on a setting in the consensus. You can override this default by using the new "CircuitPriorityHalflife" config option. Design and code by Ian Goldberg, Can Tang, and Chris Alexander.</li>
<li>Add separate per-conn write limiting to go with the per-conn read limiting. We added a global write limit in Tor 0.1.2.5-alpha, but never per-conn write limits.</li>
<li>New consensus params "bwconnrate" and "bwconnburst" to let us rate-limit client connections as they enter the network. It’s controlled in the consensus so we can turn it on and off for experiments. It’s starting out off. Based on proposal 163.</li>
</ul>

<p><strong>Major features (relay selection options):</strong></p>

<p><li>Switch to a StrictNodes config option, rather than the previous "StrictEntryNodes" / "StrictExitNodes" separation that was missing a "StrictExcludeNodes" option.</li>
<li>If EntryNodes, ExitNodes, ExcludeNodes, or ExcludeExitNodes change during a config reload, mark and discard all our origin circuits. This fix should address edge cases where we change the config options and but then choose a circuit that we created before the change.</li>
<li>If EntryNodes or ExitNodes are set, be more willing to use an unsuitable (e.g. slow or unstable) circuit. The user asked for it, they get it.</li>
<li>Make EntryNodes config option much more aggressive even when StrictNodes is not set. Before it would prepend your requested entrynodes to your list of guard nodes, but feel free to use others after that. Now it chooses only from your EntryNodes if any of those are available, and only falls back to others if a) they’re all down and b) StrictNodes is not set.</li>
<li>Now we refresh your entry guards from EntryNodes at each consensus fetch -- rather than just at startup and then they slowly rot as the network changes.</li></p>

<p><strong>Major bugfixes:</strong></p>

<ul>
<li>Stop bridge directory authorities from answering dbg-stability.txt directory queries, which would let people fetch a list of all bridge identities they track. Bugfix on 0.2.1.6-alpha.</li>
</ul>

<p><strong>Minor features:</strong>
</p>

<ul>
<li>Log a notice when we get a new control connection. Now it’s easier for security-conscious users to recognize when a local application is knocking on their controller door. Suggested by bug 1196.</li>
<li>New config option "CircuitStreamTimeout" to override our internal timeout schedule for how many seconds until we detach a stream from a circuit and try a new circuit. If your network is particularly slow, you might want to set this to a number like 60.</li>
<li>New controller command "getinfo config-text". It returns the contents that Tor would write if you send it a SAVECONF command, so the controller can write the file to disk itself.</li>
<li> New options for SafeLogging to allow scrubbing only log messages generated while acting as a relay.</li>
<li>Ship the bridges spec file in the tarball too.</li>
<li>Avoid a mad rush at the beginning of each month when each client rotates half of its guards. Instead we spread the rotation out throughout the month, but we still avoid leaving a precise timestamp in the state file about when we first picked the guard. Improves over the behavior introduced in 0.1.2.17.</li>
</ul>

<p><strong>Minor bugfixes (compiling):</strong>
</p>

<ul>
<li>Fix compilation on OS X 10.3, which has a stub mlockall() but hides it. Bugfix on 0.2.2.6-alpha.</li>
<li>Fix compilation on Solaris by removing support for the DisableAllSwap config option. Solaris doesn’t have an rlimit for mlockall, so we cannot use it safely. Fixes bug 1198; bugfix on 0.2.2.6-alpha.</li>
</ul>

<p><strong>Minor bugfixes (crashes):</strong></p>

<ul>
<li>Do not segfault when writing buffer stats when we haven’t observed a single circuit to report about. Found by Fabian Lanze. Bugfix on 0.2.2.1-alpha.</li>
<li>If we’re in the pathological case where there’s no exit bandwidth but there is non-exit bandwidth, or no guard bandwidth but there is non-guard bandwidth, don’t crash during path selection. Bugfix on 0.2.0.3-alpha.</li>
<li>Fix an impossible-to-actually-trigger buffer overflow in relay descriptor generation. Bugfix on 0.1.0.15.</li>
</ul>

<p><strong>Minor bugfixes (privacy):</strong>
</p>

<ul>
<li>Fix an instance where a Tor directory mirror might accidentally log the IP address of a misbehaving Tor client. Bugfix on 0.1.0.1-rc.</li>
<li>Don’t list Windows capabilities in relay descriptors. We never made use of them, and maybe it’s a bad idea to publish them. Bugfix on 0.1.1.8-alpha.</li>
</ul>

<p><strong>Minor bugfixes (other):</strong></p>

<ul>
<li>Resolve an edge case in path weighting that could make us misweight our relay selection. Fixes bug 1203; bugfix on 0.0.8rc1.</li>
<li>Fix statistics on client numbers by country as seen by bridges that were broken in 0.2.2.1-alpha. Also switch to reporting full 24-hour intervals instead of variable 12-to-48-hour intervals.</li>
<li>After we free an internal connection structure, overwrite it with a different memory value than we use for overwriting a freed internal circuit structure. Should help with debugging. Suggested by bug 1055.</li>
<li>Update our OpenSSL 0.9.8l fix so that it works with OpenSSL 0.9.8m too.</li>
</ul>

<p><strong>Removed features:</strong></p>

<ul>
<li>Remove the HSAuthorityRecordStats option that version 0 hidden service authorities could have used to track statistics of overall hidden service usage.</li>
</ul>

<p>On January 19, 2010, we released an updated Tor Browser Bundle, version 1.3.1.</p>

<ul>
<li> update Firefox to 3.5.7</li>
<li> update Pidgin to 2.6.5</li>
<li> update Tor to 0.2.1.22</li>
</ul>

<p>On January 25, 2010, we released Vidalia 0.2.7.</p>

<ul>o
<li>Remove the explicit palette set for the configuration dialog that prevented the dialog from inheriting colors from the user’s current system theme. (Ticket #485. Patch from mkirk.)</li>
<li>Correct the path to the badge pixmap used in time skew warning messages. (Ticket #537. Patch from mkirk.)</li>
<li>Fix compilation on Debian GNU/kFreeBSD. Patch from dererk.</li>
<li>Clean up a couple status event messages related to dangerous port warnings.</li>
<li>Change the vidalia_ru.nsh output encoding from KOI8-R to Windows-1251. (Ticket #527)</li>
<li>Add an option for building an OS X 10.4 compatible binary.</li>
</ul>

<p>On January 26, 2010, we released an updated -alpha, Tor 0.2.2.8-alpha.<br />
<strong>Major bugfixes:</strong></p>

<ul>
<li>Fix a memory corruption bug on bridges that occured during the inclusion of stats data in extra-info descriptors. Also fix the interface for geoip_get_bridge_stats* to prevent similar bugs in the future. Diagnosis by Tas, patch by Karsten and Sebastian. Fixes bug 1208; bugfix on 0.2.2.7-alpha.</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>Ignore OutboundBindAddress when connecting to localhost. Connections to localhost need to come _from_ localhost, or else local servers (like DNS and outgoing HTTP/SOCKS proxies) will often refuse to listen.</li>
</ul>

<p><strong>Design, develop, and implement enhancements that make Tor a better tool for users in censored countries.</strong><br />
Submitted Proposal 169. A backward-compatible change to the Tor connection establishment protocol to avoid the use of TLS renegotiation. In response to others using TLS renegotiation incorrectly, vendors are pulling support for TLS renegotiation. As TLS renegotiation disappears from the Internet, Tor’s use of it will stand out. In order to blend in with the crowd, we need to remove TLS renegotiation from the Tor protocol. The full spec can be found at <a href="http://gitweb.torproject.org//tor.git?a=blob;f=doc/spec/proposals/169-eliminating-renegotiation.txt;hb=HEAD" rel="nofollow">http://gitweb.torproject.org//tor.git?a=blob;f=doc/spec/proposals/169-e…</a>.</p>

<p><strong>Architecture and technical design docs for Tor enhancements related to blocking-resistance.</strong><br />
Submitted Proposal 169. A backward-compatible change to the Tor connection establishment protocol to avoid the use of TLS renegotiation. In response to others using TLS renegotiation incorrectly, vendors are pulling support for TLS renegotiation. As TLS renegotiation disappears from the Internet, Tor’s use of it will stand out. In order to blend in with the crowd, we need to remove TLS renegotiation from the Tor protocol. The full spec can be found at <a href="http://gitweb.torproject.org//tor.git?a=blob;f=doc/spec/proposals/169-eliminating-renegotiation.txt;hb=HEAD" rel="nofollow">http://gitweb.torproject.org//tor.git?a=blob;f=doc/spec/proposals/169-e…</a>.</p>

<p><strong>Hide Tor’s network signature.</strong><br />
Submitted Proposal 169. A backward-compatible change to the Tor connection establishment protocol to avoid the use of TLS renegotiation. In response to others using TLS renegotiation incorrectly, vendors are pulling support for TLS renegotiation. As TLS renegotiation disappears from the Internet, Tor’s use of it will stand out. In order to blend in with the crowd, we need to remove TLS renegotiation from the Tor protocol. The full spec can be found at <a href="http://gitweb.torproject.org//tor.git?a=blob;f=doc/spec/proposals/169-eliminating-renegotiation.txt;hb=HEAD" rel="nofollow">http://gitweb.torproject.org//tor.git?a=blob;f=doc/spec/proposals/169-e…</a>.</p>

<p><strong>Grow the Tor network and user base. Outreach.</strong></p>

<ul>
<li>Paul, Karsten, and Roger attended Financial Cryptography and Data Security 2010 Conference. Roger Dingledine presented a paper he had written with Tsuen-Wan Ngan and Dan Wallach on “Building Incentives into Tor”. This paper won Best Paper Award at the conference. Learn more at <a href="http://fc10.ifca.ai/" rel="nofollow">http://fc10.ifca.ai/</a>.</li>
<li>Karsten and Roger attended the Workshop on Ethics in Computer Security Research, <a href="http://www.cs.stevens.edu/~spock/wecsr2010/" rel="nofollow">http://www.cs.stevens.edu/~spock/wecsr2010/</a>. They presented “A Case Study on Measuring Statistical Data in the Tor Anonymity Network.”</li>
<li>Andrew attended the Internet Freedom speech by Secretary of State Clinton, <a href="http://www.state.gov/secretary/rm/2010/01/135519.htm" rel="nofollow">http://www.state.gov/secretary/rm/2010/01/135519.htm</a>.</li>
<li>Roger and Jacob discussed Tor with the Pirate Party of Sweden.</li>
<li>Jacob met with NorduNet to discuss their bandwidth authority and how to help Tor grow in the NorduNet, <a href="http://www.nordu.net" rel="nofollow">http://www.nordu.net</a>.</li>
<li>Jacob and Wikileaks people met with policymakers in Iceland to discuss freedom of speech, freedom of press, and that online privacy should be a fundamental right.</li>
<li>Roger, Karen, and Andrew met with CDT, Internews, and BBG to discuss various topics.</li>
<li>Andrew was interviewed for 90 minutes by vbs.tv about Tor, online anonymity and privacy, and the increasing usage of Tor as a censorship circumvention tool. vbs.tv will release the interview in 2010.</li>
</ul>

<p><strong>Preconfigured privacy (circumvention) bundles for USB or LiveCD.</strong></p>

<p>On January 19, 2010, we released an updated Tor Browser Bundle, version 1.3.1.</p>

<ul>
<li>update Firefox to 3.5.7</li>
<li>update Pidgin to 2.6.5</li>
<li>update Tor to 0.2.1.22</li>
</ul>

<p><strong>Bridge relay and bridge authority work.</strong><br />
From the Tor 0.2.2.8-alpha release notes;<br />
Fix a memory corruption bug on bridges that occurred during the inclusion of stats data in extra-<br />
info descriptors. Also fix the interface for geoip get bridge stats to prevent similar bugs in the<br />
future. Diagnosis by Tas, patch by Karsten and Sebastian. Fixes bug 1208; bugfix on 0.2.2.7-<br />
alpha.<br />
Roger and Christian defined a roadmap for bridgedb updates, scalability, and bugfixes. The plan can be found at <a href="http://gitweb.torproject.org//bridgedb.git?a=blob_plain;f=TODO;hb=HEAD" rel="nofollow">http://gitweb.torproject.org//bridgedb.git?a=blob_plain;f=TODO;hb=HEAD</a></p>

<p><strong>Scalability, load balancing, directory overhead, efficiency.</strong><br />
From the 0.2.2.7-alpha release notes:<br />
We were selecting our guards uniformly at random, and then weighting which of our guards we’duse uniformly at random. This imbalance meant that Tor clients were severely limited on throughput (and probably latency too) by the first hop in their circuit. Now we select guards weighted by currently advertised bandwidth. We also automatically discard guards picked using the old algorithm. Fixes bug 1217; bugfix on 0.2.1.3-alpha. Found by Mike Perry.<br />
When choosing which cells to relay first, relays can now favor circuits that have been quiet recently, to provide lower latency for low-volume circuits. By default, relays enable or disable this feature based on a setting in the consensus. You can override this default by using the new “CircuitPriorityHalflife” configuration option. Design and code by Ian Goldberg, Can Tang, and Chris Alexander.<br />
Mike Perry implemented consensus parameters for the Circuit Build Times constants and found good defaults based on experimentation on a few simulated links. The simulations seem to indicate that tor does really poorly on links with greater than 1 second of latency. Mike wrote up his findings at <a href="http://archives.seul.org/or/dev/Jan-2010/msg00012.html" rel="nofollow">http://archives.seul.org/or/dev/Jan-2010/msg00012.html</a>. Mike’s work on circuit build times should improve tor client performance as the clients pick new guard nodes and learn better circuit build times.</p>

<p><strong>More reliable (e.g. split) download mechanism.</strong><br />
Enhanced get-tor to handle Apple OS X split files.</p>

<p><strong>Translation work, ultimately a browser-based approach.</strong><br />
Updated translations via the translation portal for Chinese, Norwegian, Russian, Dutch, French,<br />
Polish, Swedish, Italian, German, Spanish, Burmese, and Turkish languages.</p>

---
_comments:

<a id="comment-4340"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4340" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 13, 2010</p>
    </div>
    <a href="#comment-4340">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4340" class="permalink" rel="bookmark">I have vista 64 .. my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have vista 64 .. my download doesnt seem to be working ,,Is this the problem</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4352"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4352" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 15, 2010</p>
    </div>
    <a href="#comment-4352">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4352" class="permalink" rel="bookmark">IDEA: Make all relays</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>IDEA: Make all relays Bridges, set all clients also to be bridges unless user selects otherwise: </p>
<p>Why not make the majority of Tor relays and clients bridges by default. In fact, make bridges the only type of relay. Only a small number of addresses of high-bandwidth nodes would be shipped with Tor, and they would have the main function of bootstrapping new nodes into the network, if the user does not want to obtain bridge addresses using e-mail or other methods. Next, every new unique, persistent node downloads a small number of bridge relay addresses from some kind of distributed table, so no one user or authority knows all relays. </p>
<p>This cant improve scalability and performance if done intelligently, and would certainly improve security for everyone. </p>
<p>What do the developers think?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4363"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4363" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 17, 2010</p>
    </div>
    <a href="#comment-4363">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4363" class="permalink" rel="bookmark">I have never been able to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have never been able to get Tor or Vidalia to work on my os x mac with Firefox.  And believe me, I have tried, like a dozen times.  And I have read every forum and help area I could track down.  Fresh installs of Tor and Firefox.  And I'm not a dummy.  I am intelligent and computer literate.  But Tor is ridiculous.  I have to say that if this program is ever going to move into mainstream use it will have to work correctly when installed, without a computer science degree required.  Otherwise folks will just give up after banging their fists on the table enough times.  It's a shame too since it seems like such a good idea.  I just don't understand why you can't make it work like other software:  click on it, it works.  It needs to be de-geekified.  Maybe you could bring someone on to the team who understands user-friendliness. I really wanted this to work.</p>
<p>If somebody who works on this project answers these comments I would be glad to post my os, hardware and browser settings and info.</p>
<p>newstuff<br />
at<br />
earthlink<br />
net or com</p>
<p>Chris</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-4365"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4365" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 18, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-4363" class="permalink" rel="bookmark">I have never been able to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4365">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4365" class="permalink" rel="bookmark">You should send an email to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You should send an email to tor-assistants with the issues.  Tor is simply drag and drop and work 99% of the time.  The issue is when the vendor breaks their OS, we had to work around it, which isn't pretty or easy to do.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-4381"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4381" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 19, 2010</p>
    </div>
    <a href="#comment-4381">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4381" class="permalink" rel="bookmark">The &quot;selecting our guards</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The "selecting our guards uniformly at random" at this and older<br />
tor-versions is.... a little bit strange(EntryGuards in state-files are not<br />
Entryguards in 'cached-consensus'-file) and slow.<br />
But the new one -0.2.1.23- is to stiff and calculable(?).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4387"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4387" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 21, 2010</p>
    </div>
    <a href="#comment-4387">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4387" class="permalink" rel="bookmark">Hi,
maybe off-topic:
I have</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,<br />
maybe off-topic:<br />
I have 0.2.1.23.<br />
Up to now i use 'StrictNodes' in torcc.<br />
Now 'StrictNodes' seems to be "StrictEntryNodes/StrictExitNodes".<br />
Right?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4418"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4418" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 25, 2010</p>
    </div>
    <a href="#comment-4418">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4418" class="permalink" rel="bookmark">I am missing the bandwidth</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am missing the bandwidth display in Vidalia to be able to display the data from my server installation of tor, not only the local one.</p>
</div>
  </div>
</article>
<!-- Comment END -->
