title: Tor 0.2.9.9 is released
---
pub_date: 2017-01-23
---
author: arma
---
tags:

tor
stable
release
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.9.9 fixes a denial-of-service bug where an attacker could cause relays and clients to crash, even if they were not built with the --enable-expensive-hardening option. This bug affects all 0.2.9.x versions, and also affects 0.3.0.1-alpha: all relays running an affected version should upgrade.<br />
This release also resolves a client-side onion service reachability bug, and resolves a pair of small portability issues.<br />
You can download the source code from <a href="https://dist.torproject.org/" rel="nofollow">https://dist.torproject.org/</a> but most users should wait for the upcoming Tor Browser release, or for their upcoming system package updates.</p>

<h2>Changes in version 0.2.9.9 - 2017-01-23</h2>

<ul>
<li>Major bugfixes (security):
<ul>
<li>Downgrade the "-ftrapv" option from "always on" to "only on when --enable-expensive-hardening is provided." This hardening option, like others, can turn survivable bugs into crashes -- and having it on by default made a (relatively harmless) integer overflow bug into a denial-of-service bug. Fixes bug <a href="https://bugs.torproject.org/21278" rel="nofollow">21278</a> (TROVE-2017-001); bugfix on 0.2.9.1-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (client, onion service):
<ul>
<li>Fix a client-side onion service reachability bug, where multiple socks requests to an onion service (or a single slow request) could cause us to mistakenly mark some of the service's introduction points as failed, and we cache that failure so eventually we run out and can't reach the service. Also resolves a mysterious "Remote server sent bogus reason code 65021" log warning. The bug was introduced in ticket <a href="https://bugs.torproject.org/17218" rel="nofollow">17218</a>, where we tried to remember the circuit end reason as a uint16_t, which mangled negative values. Partially fixes bug <a href="https://bugs.torproject.org/21056" rel="nofollow">21056</a> and fixes bug <a href="https://bugs.torproject.org/20307" rel="nofollow">20307</a>; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the January 4 2017 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor bugfixes (portability):
<ul>
<li>Avoid crashing when Tor is built using headers that contain CLOCK_MONOTONIC_COARSE, but then tries to run on an older kernel without CLOCK_MONOTONIC_COARSE. Fixes bug <a href="https://bugs.torproject.org/21035" rel="nofollow">21035</a>; bugfix on 0.2.9.1-alpha.
  </li>
<li>Fix Libevent detection on platforms without Libevent 1 headers installed. Fixes bug <a href="https://bugs.torproject.org/21051" rel="nofollow">21051</a>; bugfix on 0.2.9.1-alpha.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-232128"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232128" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 23, 2017</p>
    </div>
    <a href="#comment-232128">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232128" class="permalink" rel="bookmark">Please update Torbirdy for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please update Torbirdy for jessie-backports!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-232147"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232147" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 23, 2017</p>
    </div>
    <a href="#comment-232147">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232147" class="permalink" rel="bookmark">I&#039;ll take over this release</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'll take over this release to ask you guys about an explanation for this <a href="https://metrics.torproject.org/userstats-relay-country.html?graph=userstats-relay-country&amp;country=ae" rel="nofollow">https://metrics.torproject.org/userstats-relay-country.html?graph=users…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-232302"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232302" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 23, 2017</p>
    </div>
    <a href="#comment-232302">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232302" class="permalink" rel="bookmark">Hmm, why do you think that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hmm, why do you think that undefined behavior after integer overflow bug is relatively harmless? -ftrapv and -fwrapv were developed specially to convert UB to defined: crash or wrap. So, if you want Tor to continue execution with "survivable bug", -fwrapv is your choice.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-232502"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232502" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 24, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-232302" class="permalink" rel="bookmark">Hmm, why do you think that</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-232502">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232502" class="permalink" rel="bookmark">Yeah, I think the next step</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yeah, I think the next step is to open up a discussion about how and when to put -ftrapv, or maybe -fwrapv, back into place. We figured step one was to fix the immediate remote DoS vulnerability, then step two will be to clean up the other pieces.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-232303"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232303" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 23, 2017</p>
    </div>
    <a href="#comment-232303">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232303" class="permalink" rel="bookmark">&quot;This bug affects all</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"This bug affects all 0.2.9.x versions"<br />
Tor Browser is not affected. It is built with -fwrapv.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-232345"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232345" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">January 24, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-232303" class="permalink" rel="bookmark">&quot;This bug affects all</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-232345">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232345" class="permalink" rel="bookmark">The tor we ship in Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The tor we ship in Tor Browser is affected as well which is why we rebuild parts of the upcoming Tor Browser bundles to pick the new tor versions up.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-232616"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232616" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 25, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-232616">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232616" class="permalink" rel="bookmark">Oh, the hardened one is not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Oh, the hardened one is not affected, sure. But the others are going with undefined behavior now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-232350"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232350" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 24, 2017</p>
    </div>
    <a href="#comment-232350">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232350" class="permalink" rel="bookmark">Oh, the hardened one is not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Oh, the hardened one is not affected, sure. But others are going with undefined behavior now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-232454"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232454" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 24, 2017</p>
    </div>
    <a href="#comment-232454">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232454" class="permalink" rel="bookmark">I noticed strange network</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I noticed strange network anomaly. There are IPs which host many nodes to utilize the whole bandwidth. At the moment Tor network has 7216 nodes. Among these nodes 210 IPs are used to host few nodes. You can get full list of them using this simple command:</p>
<p><div class="geshifilter"><pre class="php geshifilter-php" style="font-family:monospace;"><ol><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal"><span style="color: #666666; font-style: italic;"># cat /var/lib/tor/cached-microdesc-consensus \&lt;br /&gt;</span></div></li><li style="font-family: monospace; font-weight: normal;"><div style="font-family: monospace; font-weight: normal; font-style: normal">    <span style="color: #339933;">|</span> grep <span style="color: #0000ff;">'^r '</span> <span style="color: #339933;">|</span> awk <span style="color: #0000ff;">'{print $6}'</span> <span style="color: #339933;">|</span> <a href="http://www.php.net/sort"><span style="color: #990000;">sort</span></a> <span style="color: #339933;">|</span> uniq <span style="color: #339933;">-</span>d <span style="color: #339933;">-</span>c</div></li></ol></pre></div></p>
<p>Why there is no any IP which hosts more than 2 nodes? None of them has 3, 4 or more. All of them (<b>210</b>) host exactly 2 nodes! How it can be explained? I first noticed it long time ago, so I can confirm this is valid at least for about 2 last years. Can it be vague indication that many of them are managed by the same party?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-232501"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232501" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 24, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-232454" class="permalink" rel="bookmark">I noticed strange network</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-232501">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232501" class="permalink" rel="bookmark">Well, the &quot;why no more than</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Well, the "why no more than 2" part is easy to answer: the directory authorities vote Running about no more than 2 relays per IP address:</p>
<p><a href="https://gitweb.torproject.org/torspec.git/tree/proposals/109-no-sharing-ips.txt" rel="nofollow">https://gitweb.torproject.org/torspec.git/tree/proposals/109-no-sharing…</a></p>
<p>(Originally the plan was "no more than 3", but somewhere along the line it got changed to "no more than 2".)</p>
<p>You could check out the individual votes by directory authorities:<br />
<a href="https://collector.torproject.org/archive/relay-descriptors/votes/" rel="nofollow">https://collector.torproject.org/archive/relay-descriptors/votes/</a><br />
to see that some IP addresses have more than 2 relays on them, but they don't get Running votes for the extra ones.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-232792"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232792" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 26, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-232792">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232792" class="permalink" rel="bookmark">Thanks, Roger! I didn&#039;t know</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks, Roger! I didn't know about these particularities of Tor protocol.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-232585"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232585" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 25, 2017</p>
    </div>
    <a href="#comment-232585">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232585" class="permalink" rel="bookmark">Hello
The tor package</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello</p>
<p>The tor package version 0.2.9.9 from deb.torproject.org for armhf and armel does not seems to be aviliable but tor-geoipdb version 0.2.9.9 is availiable on jessie and strech.</p>
<p>Did you planned to release it later?</p>
<p>Regards</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-233282"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-233282" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 29, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-232585" class="permalink" rel="bookmark">Hello
The tor package</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-233282">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-233282" class="permalink" rel="bookmark">I&#039;m also still waiting for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm also still waiting for 0.2.9.9 for arm... Or is the arm-architecture for some strange reason not affected by the 0.2.9.8 bugs?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-232597"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232597" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 25, 2017</p>
    </div>
    <a href="#comment-232597">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232597" class="permalink" rel="bookmark">Sorry. You are not using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry. You are not using Tor.<br />
Your IP address appears to be: 162.243.117.41<br />
TorLauncher NOTE: WARN DANGEROUS_SOCKS PROTOCOL=SOCKS5 ADDRESS=162.243.117.41:80<br />
What's going on?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-232608"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232608" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 25, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-232597" class="permalink" rel="bookmark">Sorry. You are not using</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-232608">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232608" class="permalink" rel="bookmark">Sounds like you are using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sounds like you are using some program that you didn't specify, which is not Tor Browser? Maybe you have installed the "torbrowser-launcher" deb from Debian or Ubuntu, for example?</p>
<p>Actually, it looks like maybe you're using some non-standard program that isn't Tor Browser, *and* you've configured it in some surprising and broken way, to use a proxy or something.</p>
<p>My best advice is to discard whatever you were doing, and use Tor Browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-232924"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-232924" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 26, 2017</p>
    </div>
    <a href="#comment-232924">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-232924" class="permalink" rel="bookmark">Do we have any mechanism to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do we have any mechanism to make use of ControlPort option safe? This option is used by many applications such as Tor Brower, which, it turn, are running in completely non-trusted environment. So, if environment (e.g. VM with Tor browser) is compromised, by using access to ControlPort the anonymity will be compromised too, as access to this port allows to do almost anything concerning configuration of Tor. </p>
<p>It would be good to have "restricted ControlPort" which allows the application to only restart tor chains [kill -1 $(pidof tor)] and nothing more; and, better, restart only those chains that are used by this application (so, tor chains of other applications will not be affected). Is it already somehow implemented or not? Do we have any proposal or ticket on this thing?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-238849"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-238849" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 03, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-232924" class="permalink" rel="bookmark">Do we have any mechanism to</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-238849">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-238849" class="permalink" rel="bookmark">I&#039;m no expert but here&#039;s my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm no expert but here's my two cents.</p>
<p>I'm not exactly sure what you mean by "tor chains". If you mean circuits, then these can be isolated without ControlPort access, using IsolateSOCKSAuth and using a SOCKS username and password in the client. I believe TorBrowser does this for the "New Tor circuit for this site" button. Torsocks does it for the IsolatePID (-i) option. </p>
<p>I think Yawning is working on a sort of application-level "firewall" that sits between the client and the ControlPort, so that applications can only access/modify data relevant to themselves, and blocks unsafe commands. Think of it like a permissions system for the ControlPort. At least that's my understanding -- read some of his blog posts for more info about this and his sandbox.</p>
<p>Regarding `kill -1 $(pidof tor)` SIGHUP is already reserved for online reloading torrc. I'm uncertain whether this has the same affect as NEWNYM, and I probably wouldn't rely on it for such (unless documented). They could dedicate another signal like SIGUSR1 to this, but I don't think it would be useful in a lot of cases. It would only work when the tor process and the client process are running as the same user on the same machine, and the signal might even get blocked by SELinux/AppArmor/Tomyo in some cases.</p>
<p>I'm not even sure NEWNYM is what you want for this kind of use case, either. NEWNYM prevents all built circuits from being reused, not just those being used by the application that wants a new identity. Theoretically, this could be used to associate different streams (applications) with each other in a kind of high-tech correlation attack under certain conditions. What you want is only for the application (that's asking for a new circuit) to have its existing circuits retired, but leave other applications' circuits on the standard 10 minute cycle. I don't think it would be portable enough for Tor to operate this way by trying to identify the sending process of a signal (not to mention associate the sender's PID with sockets connected to the SOCKSPort).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-242009"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-242009" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-238849" class="permalink" rel="bookmark">I&#039;m no expert but here&#039;s my</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-242009">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-242009" class="permalink" rel="bookmark">Thanks for so good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for so good reply!</p>
<p><i>I'm not exactly sure what you mean by "tor chains". If you mean circuits</i></p>
<p>Yes, exactly.</p>
<p><i>I believe TorBrowser does this for the "New Tor circuit for this site" button.</i></p>
<p>In typical install, yes, but my case is different, because my TBB is running inside VM, and Tor is running on another OS (host system). Furthermore, VM has no access to ControlPort (I forbid it intentionally). This isolation gives me good security (similar to Whonix), but it has this issue with NEWNYM...</p>
<p><i>I think Yawning is working on a sort of application-level "firewall" that sits between the client and the ControlPort, so that applications can only access/modify data relevant to themselves, and blocks unsafe commands. Think of it like a permissions system for the ControlPort.</i></p>
<p>Thanks for information. I think you mean his works on sandboxing. It's good to know, however, I don't think this is a proper replacement. System kernel, iptables and tor itself will be always more trusted than 3rd order application firewall. It would be more reliable to make this isolation on the level of Tor itself.</p>
<p><i>SIGHUP is already reserved for online reloading torrc. I'm uncertain whether this has the same affect as NEWNYM, and I probably wouldn't rely on it for such (unless documented).</i></p>
<p>Good point. However, I remember that -1 signal was recommended and used for circuits restart for many years. Hopefully, this behavior is preserved at least for backward compatibility.</p>
<p><i>It would only work when the tor process and the client process are running as the same user on the same machine, and the signal might even get blocked by SELinux/AppArmor/Tomyo in some cases.</i></p>
<p>Well, I don't have experience with SELinux etc, but I have experience with VMs. Since ControlPort is blocked in my setup, now it works as follows.</p>
<p>Let's assume for simplicity, that each application is just a particular VM, which connects to particular Tor's SocksPort on host machine. So, when I need to refresh any of my VMs, I do the following:</p>
<ol>
<li>I restart my VM, so it's rolled back to its original state (easy to make with dmsetup snapshots).</li>
<li>I restart all Tor circuits with kill -1 signal.</li>
</ol>
<p>Both of these steps are run by scripts from my host machine, so this is the place where VMs are managed and where Tor is running. However, I don't want to restart all circuits, because, as you correctly pointed it out, it gives extra correlation. I want to restart only those circuits that are used by my VM. In other words, all circuits, which were associated to particular SocksPort must be marked as retired, and new circuits must be created instead of them. This is exactly what I want.</p>
<p><i>I'm not even sure NEWNYM is what you want for this kind of use case, either. NEWNYM prevents all built circuits from being reused, not just those being used by the application that wants a new identity. Theoretically, this could be used to associate different streams (applications) with each other in a kind of high-tech correlation attack under certain conditions.</i></p>
<p>Exactly. But I use kill -1 because I don't have better option. However, I could connect to ControlPort from my host machine (not VM, where application is running) and make exactly this command, NEWNYM. AFAIK, stem and tor-arm have this functionality, I only need to understand the protocol and write my script for that. Maybe, in this protocol I can do NEWNYM associated with particular SocksPort, but I'm not sure. </p>
<p><i>What you want is only for the application (that's asking for a new circuit) to have its existing circuits retired, but leave other applications' circuits on the standard 10 minute cycle. I don't think it would be portable enough for Tor to operate this way by trying to identify the sending process of a signal (not to mention associate the sender's PID with sockets connected to the SOCKSPort).</i></p>
<p>You are completely right. This is what I want. Surely, proper mechanism shouldn't be some signal, but something like "restricted port". So that I point each my VM to specific SocksPort and RestricedControlPort, and then be sure that communication of my VM with Tor's RestrictedControlPort refresh only the circuits used by that SocksPort. </p>
<p>Basically, instead of making extra application-level firewall filtering request to single and very dangerous ControlPort (like root access to Tor!), I suggest to make "users" of each SocksPort distinguished by different ports, which are completely safe. System's security is much easier to reach by standard kernel's firewall than by any 3rd level mechanisms.</p>
<p>Finally, it would be like this:</p>
<p>SocksPort 9000<br />
RestrictedControlPort 9001</p>
<p>SocksPort 9002<br />
RestrictedControlPort 9003</p>
<p>ControlPort 9004</p>
<p>Or, maybe in other syntax:</p>
<p>SocksPort 9000,9001<br />
SocksPort 9000,9003<br />
ControlPort 9004</p>
<p>So that, ports 9001 and 9003 will behave exactly like 9004 on those operations that are permitted for VM's on ports 9000 and 9002, respectively.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
