title: New Release: Tor Browser 11.0.7 (Windows, macOS, Linux)
---
pub_date: 2022-03-08
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.0.7 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.7 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.7/).

This version includes important security updates to Firefox:
- https://www.mozilla.org/en-US/security/advisories/mfsa2022-09/
- https://www.mozilla.org/en-US/security/advisories/mfsa2022-11/

Tor Browser 11.0.7 updates Firefox on Windows, macOS, and Linux to 91.7.0esr.

We use the opportunity as well to update various other components of Tor Browser:
- Tor 0.4.6.10
- NoScript 11.3.7

We also switch to the latest Go version (1.16.14) for building our Go-related projects.

The full changelog since [Tor Browser 11.0.6](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- Windows + OS X + Linux
  - Update Tor to 0.4.6.10
  - Update NoScript to 11.3.7
  - [Bug tor-browser-build#40435](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40435): Remove default bridge "deusexmachina"
  - [Bug tor-browser-build#40440](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40440): Bump version of snowflake to include PT LOG events
  - [Bug tor-browser#40684](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40684): Misc UI bugs in 11.0a10
  - [Bug tor-browser#40687](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40687): Update message bar styling in about:preferences#tor
  - [Bug tor-browser#40691](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40691): Make quickstart checkbox grey when "off" on about:torconnect
  - [Bug tor-browser#40714](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40714): Next button closes "How do circuits work?" onboarding tour
  - [Bug tor-browser#40824](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40824): Drop 16439 Patch (android screencasting disable)
  - [Bug tor-browser#40826](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40826): Cherry-pick fixes for Mozilla bug 1758062
- Build System
  - Windows + OS X + Linux
    - Update Go to 1.16.14
    - [Bug tor-browser-build#40441](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40441): Add Austin as valid git-tag signer
