title: Volunteer as an alpha tester
---
author: duncan
---
pub_date: 2022-06-14
---
categories:
applications
community
---
summary:

We rely on a community of volunteer alpha testers to try out early versions of Tor Browser before their general release. Volunteer to become an alpha tester today.

---
body:

Tor Browser receives hundreds of changes a year: from updates to Firefox – the underlying browser on which Tor Browser is based – to entirely new features designed to help protect at-risk and censored users. However, each change made to Tor Browser has the potential to introduce new and sometimes elusive bugs.

In order to find and fix these bugs before they reach the majority of our users, we apply updates to an early version of Tor Browser known as Tor Browser Alpha before releasing them more widely. Then, as a small nonprofit, we rely on a community of volunteer testers to try out our alphas before their general release in order to keep Tor Browser available on so many platforms.

Volunteering to become an alpha tester is one of the most accessible and effective ways you can help at-risk users stay connected to Tor. By spending a few minutes testing each new alpha release and reporting bugs back to our developers, you'll help provide a more stable Tor Browser for millions of users around the world.

It's also important for us to reach as diverse a group of alpha testers as possible, including those on different platforms, in different countries, who speak different languages and use Tor Browser in different ways. For example, users who are based in countries in which the Tor Network is blocked (such as Belarus, China, Russia and Turkmenistan) are invaluable in helping us test [Connection Assist](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40781) – a new feature designed to detect and circumvent censorship of the Tor Network automatically. Even disparities in bandwidth can be enough for one user to experience a bug while another won't.

In addition, alpha testers have the opportunity to preview new features early, like the experimental [redesign of Connection Settings](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40782) (formally Tor Network Settings) that we're currently testing. You can help shape the direction of major new features like this by providing feedback on their user experience as we iterate on them between each alpha release.

## How to volunteer as an alpha tester

### Step 1

* Read our guide to [becoming an alpha tester](https://community.torproject.org/user-research/become-tester/).

### Step 2

* [Download the latest version of Tor Browser Alpha](https://www.torproject.org/download/alpha/) available for your platform and start testing.

### Step 3

Should you find a bug in Tor Browser Alpha:

1. Check to see whether it occurs in the general release of Tor Browser. If it does, then it's an issue with Tor Browser itself, and you should follow [these steps to get in touch](https://support.torproject.org/misc/bug-or-feedback/).
2. If this bug does not occur in the general release of Tor Browser, report it to the [Tor Browser Alpha Feedback category](https://forum.torproject.net/c/feedback/tor-browser-alpha-feedback/6) on the Tor Forum.

As a small thank you for helping us keep Tor Browser stable and bug-free, alpha testers can collect special badges by reporting bugs on the Tor Forum too.

![Alpha tester forum badges](alpha-tester-badges.png)

Lastly, it's important to understand that Tor Browser Alpha is an unstable version of Tor Browser and should not be used for activities that could put you at risk. Instead, please limit your use of alpha to preview new features, test their performance and provide feedback before their release.

Happy testing!
