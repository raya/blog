title: New Release: OnionShare 2.3
---
pub_date: 2021-02-26
---
author: micah
---
tags:

OnionShare
onion services
---
categories:

applications
onion services
---
summary: OnionShare 2.3 adds tabs, anonymous chat, better command line support, and quite a bit more.
---
_html_body:

<p><em>This post was originally published on <a href="https://micahflee.com/2021/02/onionshare-tabs-anonymous-chat-cli/">Micah Lee's blog</a>.</em></p>
<p>After a ridiculously long sixteen months (or roughly ten years in pandemic time) I'm excited to announce that OnionShare 2.3 is out! Download it from <a href="https://onionshare.org/">onionshare.org</a>.</p>
<p>This version includes loads of new and exciting features which you can read about in much more detail on the brand new OnionShare documentation website, <a href="https://docs.onionshare.org/">docs.onionshare.org</a>. For now though I'm just going to go over the major ones: tabs, anonymous chat, and better command line support.</p>
<h2 id="doing-all-the-things-at-once">Doing all the things at once</h2>
<p>In the olden days, OnionShare only did one thing: let you securely and anonymously share files over the Tor network. With time we added new features. You could use it as an <a href="https://micahflee.com/2019/02/onionshare-2/">anonymous dropbox</a>, and then later to <a href="https://micahflee.com/2019/10/new-version-of-onionshare-makes-it-easy-for-anyone-to-publish-anonymous-uncensorable-websites/">host an onion site</a>.</p>
<p>But what if you wanted to, for example, run your own anonymous dropbox <em>as well as</em> share files with someone? If your OnionShare was busy running a service, you couldn't run a second service without stopping the first service. This is all fixed now thanks to tabs.</p>
<p><img alt="onionshare's new layout" src="/static/images/blog/inline-images/onionshareblog-1.png" /></p>
<p>Now when you open OnionShare you are presented with a blank tab that lets you choose between sharing files, receiving files, hosting a website, or chatting anonymous. You can have as many tabs open as you want at a time, and you can easily save tabs (that's what the purple thumbtack in the tab bar means) so that if you quit OnionShare and open it again later, these services can start back up with the same OnionShare addresses.</p>
<p>So with OnionShare 2.3 you can host a few websites, have your own personal anonymous dropbox, and securely send files to people whenever you want, all at the same time. Under the hood, the addition of tabs also makes OnionShare connect to the Tor network faster, especially if you're using a bridge.</p>
<h2 id="secure-anonymous-ephemeral-chat-rooms-that-don-t-log-anything">Secure, anonymous, ephemeral chat rooms that don't log anything</h2>
<p>Another major new feature is chat. You start a chat service, it gives you an OnionShare address, and then you send this address to everyone who is invited to the chat room (using an encrypted messaging app like Signal, for example). Then everyone loads this address in a <a href="https://www.torproject.org/">Tor Browser</a>, makes up a name to go by, and can have a completely private conversation.</p>
<p><img alt="onionshare chat" src="/static/images/blog/inline-images/onionshareblog-2.png" /></p>
<p>If you're already using an encrypted messaging app, what’s the point of an OnionShare chat room? It leaves fewer traces.</p>
<p>If, for example, you send a message to a Signal group, a copy of your message ends up on each device (the devices, and computers if they set up Signal Desktop of each member of the group). Even if disappearing messages is turned on it’s hard to confirm all copies of the messages are actually deleted from all devices, and from any other places (like notifications databases) they may have been saved to. OnionShare chat rooms don’t store any messages anywhere, so the problem is reduced to a minimum.</p>
<p>OnionShare chat rooms can also be useful for people wanting to chat anonymously and securely with someone without needing to create any accounts. For example, a whistleblower can send an OnionShare address to a journalist using a disposable e-mail address, and then wait for the journalist to join the chat room, all without compromising their anonymity.</p>
<p>Because OnionShare relies on Tor onion services, connections between the Tor Browser and OnionShare are all end-to-end encrypted (E2EE). When someone posts a message to an OnionShare chat room, they send it to the server through their E2EE onion connection. The OnionShare server then forwards the message to all other members of the chat room through the other members' E2EE onion connections, using WebSockets. OnionShare doesn’t implement any chat encryption on its own. It relies on the Tor onion service’s encryption instead.</p>
<p>Huge thanks to <a href="https://twitter.com/Saptak013">Saptak Sengupta</a> for developing the anonymous chat feature (doing the bulk of the work in like a single day (!), in the midst of a hacker con in Goa, India last March).</p>
<h2 id="onionshare-from-the-command-line">OnionShare from the command line</h2>
<p><img alt="onionshare command line" src="/static/images/blog/inline-images/onionshareblog-5.png" /></p>
<p>OnionShare 2.3 finally de-couples the command line and the graphical versions. You can install <span class="geshifilter"><code class="php geshifilter-php">onionshare<span style="color: #339933;">-</span>cli</code></span> on any platform, including headless Linux servers, <a href="https://pypi.org/project/onionshare-cli/">using pip</a>:</p>
<p><span class="geshifilter"><code class="php geshifilter-php">pip3 install <span style="color: #339933;">--</span>user onionshare<span style="color: #339933;">-</span>cli </code></span></p>
<p>You also need to have <span class="geshifilter"><code class="php geshifilter-php">tor</code></span> installed to use it from your package manager, or Homebrew if you're using macOS.</p>
<p>It's simple to use. For example, here's how you start a chat server:</p>
<p><img alt="onionshare command line" src="/static/images/blog/inline-images/onionshareblog-4.png" /></p>
<p>I hope you enjoy the new version of OnionShare!</p>
<p><em>Note February 21, 2021: OnionShare 2.3 for Linux will be available in <a href="https://flathub.org/">Flathub</a> after <a href="https://github.com/flathub/flathub/pull/2129">this pull request</a> is reviewed and merged, so hang tight. In the meantime, it's already <a href="https://snapcraft.io/onionshare">available in Snapcraft</a> (though it logs analytics), or you can install the <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #339933;">.</span>flatpak</code></span> file directly from <a href="https://onionshare.org/dist/2.3/">onionshare.org/dist/2.3</a>.</em></p>
<p><em>Update February 22, 2022: Version 2.3 had a bug where chat was broken :( but we just released version 2.3.1 which fixes it! :).</em></p>
<p><em>Update February 23, 2020: The Flatpak package is live! Linux users <a href="https://flathub.org/apps/details/org.onionshare.OnionShare">get it from Flathub</a>.</em></p>
<hr />
<p> </p>
<p><em>If you'd like to leave a comment, you can do so on <a href="https://micahflee.com/2021/02/onionshare-tabs-anonymous-chat-cli/">Micah Lee's original blog post</a>.</em></p>
<p> </p>
<hr />
<p> </p>

