title: Tails 1.0 is out
---
pub_date: 2014-04-29
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 1.0, is out.</p>

<p>All users must <a href="https://tails.boum.org/doc/first_steps/upgrade/" rel="nofollow">upgrade</a> as soon as possible: this release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_0.23/" rel="nofollow">numerous security issues</a>.</p>

<p>For more information about what the 1.0 release means for Tails, and about its future, see the <a href="https://tails.boum.org/news/version_1.0/" rel="nofollow">full announcement</a>.</p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Security fixes
<ul>
<li>Upgrade the web browser to 24.5.0esr-0+tails1~bpo60+1 (Firefox 24.5.0esr + Iceweasel patches + Torbrowser patches).</li>
<li>Upgrade Tor to 0.2.4.21-1+tails1~d60.squeeze+1:
<ul>
<li>Based on 0.2.4.21-1~d60.squeeze+1.</li>
<li>Backport the fix for <a href="https://trac.torproject.org/projects/tor/ticket/11464" rel="nofollow">bug #11464 on Tor Project's Trac</a>. It adds client-side blacklists for all Tor directory authority keys that was vulnerable to Heartbleed.  This protects clients in case attackers were able to compromise a majority of the authority signing and identity keys.</li>
</ul>
</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Disable inbound I2P connections. Tails already restricts incoming connections, but this change tells I2P about it.</li>
<li>Fix link to the system requirements documentation page in the Tails Upgrader error shown when too little RAM is available.</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Upgrade I2P to 0.9.12-2~deb6u+1.</li>
<li>Import TorBrowser profile. This was forgotten in Tails 0.23 and even though we didn't explicitly set those preferences in that release they defaulted to the same values. This future-proofs us in case the defaults would ever change.</li>
<li>Import new custom version of Tor Launcher:
<ul>
<li>Based on upstream Tor Launcher 0.2.5.3.</li>
<li>Improve how Tor Launcher handles incomplete translation. (<a href="https://trac.torproject.org/projects/tor/ticket/11483" rel="nofollow">bug #11483 on Tor Project's Trac</a>; more future-proof fix for <a href="https://labs.riseup.net/code/issues/6885" rel="nofollow">ticket #6885</a>)</li>
<li>Remove the bridge settings prompt. (<a href="https://trac.torproject.org/projects/tor/ticket/11482" rel="nofollow">bug #11482 on Tor Project's Trac</a>; closes <a href="https://labs.riseup.net/code/issues/6934" rel="nofollow">ticket #6934</a>)</li>
<li>Always show bridge help button. (<a href="https://trac.torproject.org/projects/tor/ticket/11484" rel="nofollow">bug #11484 on Tor Project's Trac</a>)</li>
</ul>
</li>
<li>Integrate the new Tails logo into various places:
<ul>
<li>The website</li>
<li>The boot splash</li>
<li>The "About Tails" dialog</li>
</ul>
</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<ul>
<li><a href="https://tails.boum.org/support/known_issues/" rel="nofollow">Longstanding</a> known issues.</li>
</ul>

<p><strong>I want to try it or to upgrade!</strong></p>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> page.</p>

<p>As no software is ever perfect, we maintain a list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">problems that affects the last release of Tails</a>.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for June 10.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap/" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Would you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, come talk to us!</p>

<p><strong>Support and feedback</strong></p>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

