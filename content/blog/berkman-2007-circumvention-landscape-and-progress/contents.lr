title: Berkman 2007 Circumvention Landscape and Progress
---
pub_date: 2009-03-05
---
author: phobos
---
tags:

Berkman Center for Internet and Society
research reports
tool comparison
---
categories:

reports
research
---
_html_body:

<p>The Berkman Center has released their report on the <a href="http://cyber.law.harvard.edu/publications/2009/2007_Circumvention_Landscape_Report" rel="nofollow">landscape of circumvention technologies</a> as it was in 2007.  Tor was included in this test and comes out as a secure tool with some improvements needed.  Technology Review also picked up this report and <a href="http://www.technologyreview.com/web/22252/page1/" rel="nofollow">wrote an article</a> on the results.</p>

<p>Since the original publication of the report, we've responded to and improved a number of identified weaknesses. The main focus has been on usability of the software. Tor is easier to understand, configure, and install. We've worked on finding translators for the various parts of the suite of tools that comprise Tor. We developed and enhanced a Firefox plugin called Torbutton. The current Torbutton mitigates all known browser-based anonymity attacks. Torbutton is included in our bundles and is automatically installed into the user's Firefox browser configuration. Torbutton can be found at <a href="https://torproject.org/torbutton/" rel="nofollow">https://torproject.org/torbutton/</a> or <a href="https://addons.mozilla.org/en-US/firefox/addon/2275" rel="nofollow">https://addons.mozilla.org/en-US/firefox/addon/2275</a>.</p>

<p>In response to the demand for portability and ease of use, we created the Tor Browser Bundle. The Tor Browser Bundle lets you use Tor on Windows without needing to install any software. It can run off a USB flash drive, or any portable media, and comes with a pre-configured web browser and is self contained. The Tor IM Browser Bundle additionally allows instant messaging and chat. The bundle contains the Tor controller Vidalia, a portable version of Firefox, a caching http proxy called Polipo, the instant messaging client Pidgin with Off The Record client to client encryption for secure chats, and of course, the Tor proxy software. The Tor Browser Bundle can be found at <a href="https://torproject.org/torbrowser/" rel="nofollow">https://torproject.org/torbrowser/</a>.</p>

<p>There is an arms race between censors and circumvention tools. We've developed a number of features to prepare for the next few steps in this race. Some of these features involve the ability to use non-public relays and normalization of the Tor traffic to allow those in content denied environments the ability to continue to reach the information and communities they need. Much more research and experimentation needs to occur in the next few years to further enhance these features.</p>

<p>As part of its dedication to transparency and openness, The Tor Project has published its three year development roadmap, focused on providing anti-censorship tools and services for the advancement of Internet freedom in closed societies. While Tor's original goal was to provide online anonymity, many people around the world use Tor to get around Internet censorship, as well. Human Rights Watch and Global Voices Online have both recommended Tor as a tool to circumvent censorship regimes in oppressive nations. The roadmap is focused on providing anti-censorship tools and services for the advancement of Internet freedom in closed societies. This roadmap can be found at <a href="https://www.torproject.org/press/2008-12-19-roadmap-press-release" rel="nofollow">https://www.torproject.org/press/2008-12-19-roadmap-press-release</a>.</p>

---
_comments:

<a id="comment-771"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-771" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://www.pgpru.com">unknown (not verified)</a> said:</p>
      <p class="date-time">March 06, 2009</p>
    </div>
    <a href="#comment-771">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-771" class="permalink" rel="bookmark">I see a double posting of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I see a double posting of the phrase from the last indent of the message:</p>
<p>[quote]<br />
The Tor Project has published its three year development roadmap, **focused on providing anti-censorship tools and services for the advancement of Internet freedom in closed societies**. ...<br />
[/quote]</p>
<p>[quote]<br />
...in oppressive nations. The roadmap is **focused on providing anti-censorship tools and services for the advancement of Internet freedom in closed societies.**<br />
[/quote]</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-780"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-780" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">March 06, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-771" class="permalink" rel="bookmark">I see a double posting of</a> by <a rel="nofollow" href="http://www.pgpru.com">unknown (not verified)</a></p>
    <a href="#comment-780">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-780" class="permalink" rel="bookmark">nice catch</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's in the original response as well.  We're just trying to re-inforce the point. ;)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1499"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1499" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Glycine (not verified)</span> said:</p>
      <p class="date-time">June 08, 2009</p>
    </div>
    <a href="#comment-1499">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1499" class="permalink" rel="bookmark">Torbutton</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"The current Torbutton mitigates all known browser-based anonymity attacks. Torbutton is included in our bundles and is automatically installed into the user's Firefox browser configuration."</p>
<p>That's nice, but what if i didn't want Torbutton installed?   Is there a simple way to uninstall it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1517"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1517" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 10, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1499" class="permalink" rel="bookmark">Torbutton</a> by <span>Glycine (not verified)</span></p>
    <a href="#comment-1517">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1517" class="permalink" rel="bookmark">re: Torbutton</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, simply go to Firefox, Addons, and uninstall Torbutton.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1773"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1773" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Germand Dude (not verified)</span> said:</p>
      <p class="date-time">July 09, 2009</p>
    </div>
    <a href="#comment-1773">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1773" class="permalink" rel="bookmark">The best news for me is that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The best news for me is that you've included Firefox in your ideas. I can't move around without this browser so any application I install must be compliant.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2190"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2190" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Todd Smith (not verified)</span> said:</p>
      <p class="date-time">August 22, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1773" class="permalink" rel="bookmark">The best news for me is that</a> by <span>Germand Dude (not verified)</span></p>
    <a href="#comment-2190">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2190" class="permalink" rel="bookmark">Also what&#039;s awesome</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is that it can be run off portable media. You never have to install it and a lot of people carry around a USB stick anyways. Being able to run off a USB key is really convenient!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1943"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1943" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Mark Jensti (not verified)</span> said:</p>
      <p class="date-time">July 27, 2009</p>
    </div>
    <a href="#comment-1943">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1943" class="permalink" rel="bookmark">That&#039;s nice, but what if i</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's nice, but what if i didn't want Torbutton installed? Is there a simple way to uninstall it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1954"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1954" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">July 29, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1943" class="permalink" rel="bookmark">That&#039;s nice, but what if i</a> by <span>Mark Jensti (not verified)</span></p>
    <a href="#comment-1954">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1954" class="permalink" rel="bookmark">It&#039;s easy.  1) Don&#039;t check</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's easy.  1) Don't check it on install.  2) In Firefox, to go Tools, Add-ons, and uninstall Torbutton.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1944"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1944" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>juli (not verified)</span> said:</p>
      <p class="date-time">July 28, 2009</p>
    </div>
    <a href="#comment-1944">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1944" class="permalink" rel="bookmark">Tor toggle</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I sent a comment a week ago, but I couldn't find it or an answer going through the comments on the right. How does one search in comments? I typed in Tor toggle with no results. </p>
<p>Anyway, Tor seems to be working well, but I am unable to toggle it off when I do not need anonymous browsing. If I exit it, it simply gives a 404 page, and blocks browsing altogether. </p>
<p>Solution?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1955"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1955" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">July 29, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1944" class="permalink" rel="bookmark">Tor toggle</a> by <span>juli (not verified)</span></p>
    <a href="#comment-1955">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1955" class="permalink" rel="bookmark">re: Tor toggle</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>well, these are comments.  we're working on a support forum.</p>
<p>Is this with the Tor Browser Bundle?  or Firefox 3.5.1?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2028"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2028" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 11, 2009</p>
    </div>
    <a href="#comment-2028">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2028" class="permalink" rel="bookmark">It&#039;s really convenient</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I like Tor Browser Bundle. It's really what I need! I installed it very easily and I'm very satisfied with it. Besides, it's very nice that it deals with Firefox. It is my favourite browser. I installed additional plugins (found them at <a href="http://www.picktorrent.com" rel="nofollow">http://www.picktorrent.com</a>  ) and now Firefox is indispensable for me.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2191"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2191" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Cherry Chan (not verified)</span> said:</p>
      <p class="date-time">August 22, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2028" class="permalink" rel="bookmark">It&#039;s really convenient</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2191">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2191" class="permalink" rel="bookmark">I&#039;m using it right now!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've installed the to browser bundle and i have to agree. I really like what I'm seeing!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4832"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4832" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 27, 2010</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2028" class="permalink" rel="bookmark">It&#039;s really convenient</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-4832">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4832" class="permalink" rel="bookmark">I&#039;ve installed the to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've installed the to browser bundle and i have to agree. I really like what I'm seeing!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2229"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2229" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>SwissTorExit (not verified)</span> said:</p>
      <p class="date-time">August 27, 2009</p>
    </div>
    <a href="#comment-2229">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2229" class="permalink" rel="bookmark">serch engine</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i use ixquick while don't log, use ssl, respect privacy</p>
<p>scoogle while are google search engine but in ssl and many languages .</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-3561"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-3561" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2009</p>
    </div>
    <a href="#comment-3561">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-3561" class="permalink" rel="bookmark">I like Tor Browser Bundle.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I like Tor Browser Bundle. Thanks</p>
<p>The kind of effect the erectile difficulty had on the man and his partner<br />
his emotional and behavioral reactions to his erectile difficulties<br />
<a href="http://www.globalcanadameds.com/" rel="nofollow">canada pharmacy online</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-4038"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-4038" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 21, 2010</p>
    </div>
    <a href="#comment-4038">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-4038" class="permalink" rel="bookmark">Do you know that members of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you know that members of the Berkman center research, teach, and write books, articles, weblogs with RSS 2.0 feeds (for which the Center holds the specification), and podcasts (of which the first series took place at the Berkman Center). Its newsletter, "The Filter", is on the Web and available by e-mail, and it hosts a blog community of Harvard faculty, students, and Berkman Center affiliates.</p>
</div>
  </div>
</article>
<!-- Comment END -->
