title: Strength in Numbers: Community Is Key
---
pub_date: 2018-11-21
---
author: isabela
---
tags:

Strength in Numbers
executive director
---
categories: jobs
---
summary: This is my first blog post as the Tor Project’s Executive Director. I can’t express how excited I am for this next journey. I have been a Tor user and advocate since its early days, in the Vidalia times. Tor has come a long way, always evolving to provide a holistic solution for anonymity, security and privacy online.
---
_html_body:

<p><em>This post is one in a series of blogs to complement our 2018 crowdfunding campaign, Strength in Numbers. Anonymity loves company, and the internet freedom movement is stronger when we fight together. <a href="https://torproject.org/donate/donate-sin-ib-bp">Please donate today</a>, and your gift will be matched by Mozilla. </em></p>
<p>Hello World!</p>
<p>This is my first blog post as the Tor Project’s Executive Director. I can’t express how excited I am for this next journey. I have been a Tor user and advocate since its early days, in the <a href="https://blog.torproject.org/plain-vidalia-bundles-be-discontinued-dont-panic">Vidalia times</a>. Tor has come a long way, always evolving to provide a holistic solution for anonymity, security and privacy online.</p>
<p>Our year-end campaign is called Strength in Numbers, because many of the things that make Tor strong depend on size and diversity. You see, the beauty of Tor is that to achieve its goals of security, anonymity, and privacy, it must have a strong and vibrant community. We must have a decentralized network of volunteer-run servers to have a Tor network. And the higher the number of countries where these relays exist, the safer all communications running through them are. Therefore, Tor’s community must truly be a global one.</p>
<p>We also want Tor to be widely used by a variety of people in a variety of countries on a variety of platforms. I would like to share the <a href="https://lists.torproject.org/pipermail/tor-project/2018-November/002061.html">short term vision</a> I have been developing with the help of others in the Tor community for us to be able to achieve that. These are things that we want to drive our work for the next ~3 years:</p>
<ul>
<li>A Mature Tor Project (organization/community)
<ul>
<li>Stable income flows from a diverse funding base</li>
<li>Diverse and robust organization that meets our needs</li>
<li>Strong organizational culture, focused on employee and volunteer happiness and stability</li>
<li>Global brand recognition - Tor means strong privacy</li>
</ul>
</li>
<li>Full Access (product)
<ul>
<li><a href="https://blog.torproject.org/strength-numbers-internet-freedom-line">Any person on the planet can access the Tor network</a></li>
<li>Any person on the planet can use the Tor network to access any website or online services</li>
<li>Ensure the Tor network is diverse, healthy, stable and scalable</li>
</ul>
</li>
</ul>
<p>As you can see, one of our first goals is to diversify our funding base. It’s very important for any nonprofit organization to have diverse sources of income. But unrestricted donations, like the ones we get during end of year fundraising campaigns, not only help us diversify our funds, but also allow us to be agile in our work. We can easily allocate resources to whatever important events that requires our response, and reorder our priorities whenever needed. This is extremely important for any software development organization, especially one that provides essential safety to people in volatile locations like Tor.</p>
<p>Not everyone who uses Tor is in a situation where they can provide financial support. It’s so important that those of us who are able to give do, so those in harm’s way can communicate safely.</p>
<p>Won’t you help us with our important work? Please donate today.</p>
<p><a href="https://torproject.org/donate/donate-sin-ib-bp"><img alt="donate button " src="/static/images/blog/inline-images/tor-donate-button_4.png" class="align-center" /></a><figure role="group" class="align-center">
<img alt="tor-pic" src="/static/images/blog/inline-images/tor-project-shari-farewell_0.png" />
<figcaption><em>Jon, Shari, Isa, Erin, and Ruby </em></figcaption>
</figure>
</p>
<p>And use Tor! As this year’s campaign t-shirt says, Anonymity Loves Company. The more people use Tor, the safer those who need it the most will be.</p>
<p>I look forward for the amazing work we will be doing in 2019 to achieve our goals. Thank you for being part of this community and for joining us on this journey. You help us keep Tor strong.</p>
<p> </p>

---
_comments:

<a id="comment-278543"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278543" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>#Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2018</p>
    </div>
    <a href="#comment-278543">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278543" class="permalink" rel="bookmark">We love you guise. ;-)
That…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We love you guise. ;-)</p>
<p>That is all</p>
<p>Carry On</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278545"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278545" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>citizenD (not verified)</span> said:</p>
      <p class="date-time">November 21, 2018</p>
    </div>
    <a href="#comment-278545">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278545" class="permalink" rel="bookmark">Thank yall for your work!…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank yall for your work! Really appreciate it :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278546"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278546" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2018</p>
    </div>
    <a href="#comment-278546">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278546" class="permalink" rel="bookmark">I see Isa in the top picture…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I see Isa in the top picture, but in the picture captioned "Jon, Shari, Isa, Erin, and Ruby" I don't.  I hesitate to explain why I consider this somewhat disconcerting.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278555"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278555" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  steph
  </article>
    <div class="comment-header">
      <p class="comment__submitted">steph said:</p>
      <p class="date-time">November 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278546" class="permalink" rel="bookmark">I see Isa in the top picture…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278555">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278555" class="permalink" rel="bookmark">Hrm. She is third from the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hrm. She is third from the left.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278559"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278559" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to steph</p>
    <a href="#comment-278559">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278559" class="permalink" rel="bookmark">Some days it seems as though…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Some days it seems as though there is always something weird happening in this blog.</p>
<p>Here's the problem.  In the picture I see four people, some of whom I thought I recognized.  But the caption "Jon, Shari, Isa, Erin, and Ruby" mentions five people, does it not?</p>
<p>Is the joke that Isa is depicted abstractly, represented as the seal between the two pairs of visible people?</p>
<p>Oh wait, other penny just dropped: there are indeed four humans and one canine.  OK, heh, yah got me.  Ruby, first from right, who is I take it is biologically a canid, was hard to see and my vision is poor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-278547"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278547" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2018</p>
    </div>
    <a href="#comment-278547">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278547" class="permalink" rel="bookmark">These are all wonderful…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>These are all wonderful goals, and I love how well you kept the list short!  The Tor user community faces so many dangers (technical, political, disinformational..) that it is easy to become overwhelmed.  But if we always bear in mind the fact that our many many enemies have problems of their own, and if we acknowledge how far Tor Project has come in just the past few years towards achieving the goals you enumerate, we should always be able to find the hope, courage, and undying determination we need to struggle day by day to make Tor bigger, better, and globally accessible!  On good days we might even have fun making the world a better place.</p>
<p>Everyone needs Tor.  Now we just have to convince everyone of that fact, and figure out how to enable everyone to actually use Tor everyday for everything, even in such difficult environments as China or Russia (or, maybe soon, the USA).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278549"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278549" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2018</p>
    </div>
    <a href="#comment-278549">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278549" class="permalink" rel="bookmark">Won&#039;t you provide a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Won't you provide a standalone Tor for Linux as you do with TBB for Linux? Installing from source kills the user experience. Apt-get (and other package managers) would have to install Tor non-anonymously.</p>
<p>For instance, Tor for Windows has a GUI to install it and I can just unpack a Tor Browser and use it on Linux. A standalone Tor for Linux would have been a great UX improvement. I believe, that should improve the strength in numbers!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278556"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278556" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278549" class="permalink" rel="bookmark">Won&#039;t you provide a…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278556">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278556" class="permalink" rel="bookmark">&gt; Apt-get (and other package…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Apt-get (and other package managers) would have to install Tor non-anonymously.</p>
<p>Actually, I believe that is not true, and I want every Tor user to know this. (TP could help get the word out by regularly posting a blog popularizing the facts I am about to cite.  I know you can download debs this way but haven't tried downloading src.)  </p>
<p>Please search for onion mirrors for debian.org, or look back in posts to this blog until you find the posts (at least two) which give the onion addresses and explain what you need (download apt-transport-tor from the Debian mirrors the usual way) to upgrade a Debian system via Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278557"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278557" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278549" class="permalink" rel="bookmark">Won&#039;t you provide a…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278557">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278557" class="permalink" rel="bookmark">&gt; For instance, Tor for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; For instance, Tor for Windows has a GUI to install it and I can just unpack a Tor Browser and use it on Linux. A standalone Tor for Linux would have been a great UX improvement.</p>
<p>Huh?  At the download page for torproject.org you find tarballs and detached signatures for Windows, MacOS, and Linux.  If you run Tor Browser on a Linux system you should unpack the tarball for Linux somewhere (after verifying the signature of course).</p>
<p>If by "standalone Tor for Linux" you mean a tor client/server utility, you can find that in the Debian repositories and probably for other Linux distributions also.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278606"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278606" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 27, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278549" class="permalink" rel="bookmark">Won&#039;t you provide a…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278606">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278606" class="permalink" rel="bookmark">&gt; Apt-get (and other package…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Apt-get (and other package managers) would have to install Tor non-anonymously.</p>
<p>Using apt does not provide less anonymity when installing tor than directly connecting to thetorproject.org since you can use apt-transport-https. And once you have tor installed you can use apt-transport-tor.</p>
<p>There are instructions for using apt over tor here (and in general) <a href="http://expyuzz4wqqyqhjn.onion/docs/debian.html.en#apt-over-tor" rel="nofollow">http://expyuzz4wqqyqhjn.onion/docs/debian.html.en#apt-over-tor</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278558"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278558" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 22, 2018</p>
    </div>
    <a href="#comment-278558">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278558" class="permalink" rel="bookmark">Congrats on your new job.
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Congrats on your new job.</p>
<p>One very important point which you post does not address: for many years, TP (in the person of Roger D) has repeatedly insisted "no backsdoors, ever!".  This assurance would mean much more if accompanied by a more detailed explanation of the definition of "backdoor".  Tor users are worried in particular that USG will try to coerce TP into adding some kind of "backdoor", leveraging the fact that TP is currently organized legally as a US-based tax-exempt non-profit.</p>
<p>Among the kind of highly plausible backdoors USG might try to coerce TP into secretly adding:</p>
<p>o changing the parameters of some Tor-critical cryptographic protocol in a subtle way which quietly makes it much easier for NSA supercomputers to break at will,</p>
<p>o changing the parameters of a critical psuedorandom number generator used by Tor Browser for the same purpose (the Snowden leaks show that NSA has already attempted this at least once, which involved years of blatant full-bore lying to NIST employees),</p>
<p>o more direct legal assaults such as outlawing gpg and openssl entirely and forcing TP to use some kind of "key escrow" in some new NSA designed "crypto standard"; note that Mr. Joe Biden, a possible 2020 Presidential candidate, was directly involved in promoting the notorious Clipper Chip, and Biden makes no secret that if elected he intends to try again.</p>
<p>And as for the current US President, his open admiration for V. Putin and the authoritarian policies of that regime speak for themselves.  So both "mainstream" US parties have openly declared their hostile intentions toward the Tor community.</p>
<p>Are you in a position to categorically state that TP will never cave into pressure from USG to change Tor software or Tor environment (volunteer nodes, Directory Authorities) in any way which will make it easier for USG agencies or contractors (or RU, CN, IR, BR, PK, IL, FR, UK, AU etc agencies or their contractors) or corporations (Amazon, Comcast, surveillance-as-a-service companies such as Hacking Team and NSO Group) to break into Tor network protected bitstreams or to obtain other sensitive information about Tor users and Tor infrastructure?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278637"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278637" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 29, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278558" class="permalink" rel="bookmark">Congrats on your new job.
…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278637">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278637" class="permalink" rel="bookmark">&quot;Among the kind of highly…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Among the kind of highly plausible backdoors...". How about some affirmation (audit?) regarding this one:</p>
<p> - Keeping the notoriously curious, 3-rd party (and not committed to anonimity) supplied, NoScript as a part of every TB package?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278737"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278737" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 05, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278637" class="permalink" rel="bookmark">&quot;Among the kind of highly…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278737">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278737" class="permalink" rel="bookmark">Plus one.  Cybersecurity…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Plus one.  Cybersecurity news is full of stories, not all of them "scare stories", about schemes by governments or criminals to punt a cyberespionage ware as a "security product".</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278639"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278639" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 29, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278558" class="permalink" rel="bookmark">Congrats on your new job.
…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278639">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278639" class="permalink" rel="bookmark">Why no answer?  Does TP…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why no answer?  Does TP still say "no backdoors ever" or not?  (Where backdoor means anything that weakens Tor, such as weakened PRNG, not just an escrowed "master key" or secret side door squirrelled away inside TOr code.)</p>
<p>See:</p>
<p>wired.com<br />
Deputy AG Rod Rosenstein Is Still Calling for an Encryption Backdoor<br />
Lily Hay Newman<br />
29 Nov 2018</p>
<p>&gt; Tension has existed for decades between law enforcement and privacy advocates over data encryption. The United States government has consistently lobbied for the creation of so-called backdoors in encryption schemes that would give law enforcement a way in to otherwise unreadable data. Meanwhile, cryptographers have universally decried the notion as unworkable. But at a cybercrime symposium at the Georgetown University Law School on Thursday, deputy attorney general Rod Rosenstein renewed the call.... But privacy advocates and cryptographers say that the creation of a cryptographic "master key" would represent a dangerous point of failure in crucial user protections. The paper "Keys Under Doormats" laid out the technological infeasibility of backdoor schemes in 2015. If a backdoor mechanism were independently discovered by bad actors, or stolen from the government, entire data protection schemes would be instantly undermined. That risk is all too real; it was just last year that WikiLeaks exposed a trove of CIA hacking secrets in its so-called Vault7 dump. And a leaked NSA spy tool called EternalBlue has caused devastating damage since it became public in 2017.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278653"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278653" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278558" class="permalink" rel="bookmark">Congrats on your new job.
…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278653">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278653" class="permalink" rel="bookmark">@ Tor Project executive:
So…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ Tor Project executive:</p>
<p>So you will respond to questions about the dog, but not backdoors?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278710"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278710" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  steph
  </article>
    <div class="comment-header">
      <p class="comment__submitted">steph said:</p>
      <p class="date-time">December 04, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278653" class="permalink" rel="bookmark">@ Tor Project executive:
So…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278710">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278710" class="permalink" rel="bookmark">We will never have backdoors.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We will never have backdoors.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-278565"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278565" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Micio (not verified)</span> said:</p>
      <p class="date-time">November 22, 2018</p>
    </div>
    <a href="#comment-278565">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278565" class="permalink" rel="bookmark">That&#039;s funny a dog on pic …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's funny a dog on pic ,which is his role on torproject.org? I'm kidding guys, love tor. To be honest I have seen him randomly  while was watching the pic.Was well camouflaged, that's tor after all. ☺☺☺</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278577"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278577" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="Tweet daily?  #onionlove">Tweet daily?  … (not verified)</span> said:</p>
      <p class="date-time">November 24, 2018</p>
    </div>
    <a href="#comment-278577">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278577" class="permalink" rel="bookmark">Crescer uma cebolla,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Crescer uma cebolla, alimentar o mundo.</p>
<p>Tudo passa tanto.</p>
<p>Desafortunadamente, quando todo mundo sabe, pessoas.</p>
<p>Alexa vendeu minha vida à Amazona.</p>
<p>Vosso vau rescinde em Xinjiang.</p>
<p>Minhas letras estão regressadas, mas não meus beijos.</p>
<p>Com frequência a morte chega pela manhã, como a canção de pássaros.</p>
<p>Esconderijo vossos substantivos.</p>
<p>Nunca escutar a conselho.</p>
</div>
  </div>
</article>
<!-- Comment END -->
