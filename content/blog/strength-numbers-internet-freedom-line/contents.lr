title: Strength in Numbers: Internet Freedom Is on the Line
---
pub_date: 2018-11-20
---
author: steph
---
tags:

Strength in Numbers
internet freedom
censorship
---
categories: circumvention
---
summary: The Tor Project believes that everyone should have private access to an uncensored web, but digital authoritarianism is on the rise. For the 8th year in a row, internet freedom has declined around the world, including in the United States.
---
_html_body:

<p><em>This post is one in a series of blogs to complement our 2018 crowdfunding campaign, Strength in Numbers. Anonymity loves company, and the internet freedom movement is stronger when we fight together. <a href="https://torproject.org/donate/donate-sin-if-bp">Please contribute today</a>, and your gift will be matched by Mozilla.</em></p>
<hr />
<p>The Tor Project believes that everyone should have private access to an uncensored web, but digital authoritarianism is on the rise. For the 8th year in a row, internet freedom has declined around the world, including in the United States.</p>
<p>“Of the 65 countries assessed, 26 have been on an overall decline since June 2017,” reveals <a href="https://freedomhouse.org/report/freedom-net/freedom-net-2018">a new report</a> by Freedom House.</p>
<p>A huge factor in this decline is government censorship, a growing problem in many countries. Freedom to publish, share, and access information online is critical for a healthy society, yet governments and entities around the world are denying people this universal human right, and their tactics for doing so are becoming more advanced.</p>
<p>In many countries around the world, people are only permitted to access state-sponsored news, where the stories always spin a nation's government and leadership in favorable lights.</p>
<p>Internet controls in China have reached new extremes, and China is <a href="https://qz.com/africa/1447015/china-is-helping-african-countries-control-the-internet/">exporting its methods</a> to other governments. China, Egypt, Iran, Venezuela, Ethiopia, Turkey, and a few other countries now block the Tor network.</p>
<p><a href="https://blog.torproject.org/domain-fronting-critical-open-web">Amazon and Google shut down domain fronting</a>, a once reliable tactic used by many pluggable transports to access the Tor network when it is blocked.</p>
<p>These developments make our work more important than ever. That is why in 2018 we worked hard to keep the Tor network secure and strong. We made Tor Browser <a href="https://blog.torproject.org/new-release-tor-browser-80">more user-friendly</a> and localized it into 9 additional languages (for a total of 24). We brought Tor Browser to mobile with the alpha version of Tor Browser for Android, which has already been installed <a href="https://play.google.com/store/apps/details?id=org.torproject.torbrowser_alpha">over half a million times</a>. We traveled to different countries to meet at-risk communities and provided them with digital security training.</p>
<p>We are determined to reach even more people in need.<br />
 <br />
<a href="https://www.torproject.org/download">Tor Browser</a> protects against tracking, surveillance, and censorship, and we think everyone, no matter where they are in the world, should be able to use it and enjoy their universal human rights to privacy and freedom.</p>
<p>We’re in a race with the censors. They are getting more sophisticated, and so must we.</p>
<p>We improved our bridge-fetching system with the launch of Tor Browser 8, so it’s easier to access Tor where it is blocked, but we aren’t stopping there. We are forming an anti-censorship team to take this work to the next level. Our goal is for people in countries where Tor is blocked to be able to browse freely, privately, and safely.</p>
<p><img alt="Tor Story - Iran " src="/static/images/blog/inline-images/tor-iran-story-quote.png" class="align-center" /></p>
<p><strong>Our mission is bold, but we can’t do it alone. There is strength in numbers. Please join us in this fight for internet freedom and help us equip more people with the ability to circumvent censorship.</strong></p>
<p><a href="https://torproject.org/donate/donate-sin-if-bp"><img alt="donate button" src="/static/images/blog/inline-images/tor-donate-button_3.png" class="align-center" /></a></p>
<p>Tor is the strongest tool for privacy and freedom online, and your donation will help us make our circumvention methods stronger and reach the people who need it most. <a href="https://torproject.org/donate/donate-sin-if-bp">Donate today</a>, and Mozilla will match your donation.</p>
<p> </p>

---
_comments:

<a id="comment-278528"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278528" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2018</p>
    </div>
    <a href="#comment-278528">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278528" class="permalink" rel="bookmark">I completely agree with…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I completely agree with everything you wrote!</p>
<p>Expanding slightly on one topic you mentioned: both well known US based companies such as Google, and little known ones such as Remark, are falling over themselves in their efforts to join the enormously profitable and infinitely unethical oppression-as-a-service industry, in particular to become Chinese government contractors helping to build the social credit surveillance and population control system.  US lawmakers (and US three letter agencies) have been willfully blind to the rather obvious trend: companies such as Amazon increasingly value their Chinese government contracts more than their USG contracts.   Corporate sovereignty indeed.</p>
<p>One hopeful sign is the Google employee boycott of work on "censorbrowser".  I hope their example will inspire dissidents inside Amazon and other tech-cos to pressure their employers to withdraw from the oppression-as-a-service industry.</p>
<p>Tor Project can perhaps help, by attempting to raise the profile of attempts to organize boycotts in US universities against NSA and DARPA funding, etc., and likewise for other the university systems of other nations with ambitions to pursue "power projection" by internet-enabled technological means.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278530"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278530" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>jay (not verified)</span> said:</p>
      <p class="date-time">November 20, 2018</p>
    </div>
    <a href="#comment-278530">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278530" class="permalink" rel="bookmark">I agree&#039; push forward on and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I agree' push forward on and moving with the Tor Project, implement a catch weaver that block all attempts to lick info to any undermining authorities"</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278531"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278531" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2018</p>
    </div>
    <a href="#comment-278531">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278531" class="permalink" rel="bookmark">Sorry dude I don&#039;t want to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry dude I don't want to share my card information with Mozilla.<br />
Mozilla is not supporting online freedom.</p>
<p><a href="https://addons.mozilla.org/en-US/firefox/addon/bcma/" rel="nofollow">https://addons.mozilla.org/en-US/firefox/addon/bcma/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278532"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278532" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  steph
  </article>
    <div class="comment-header">
      <p class="comment__submitted">steph said:</p>
      <p class="date-time">November 20, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278531" class="permalink" rel="bookmark">Sorry dude I don&#039;t want to…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278532">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278532" class="permalink" rel="bookmark">We do not share our…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We do not share our individual donors' information with anyone.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278620"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278620" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 27, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to steph</p>
    <a href="#comment-278620">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278620" class="permalink" rel="bookmark">Except large donors?  
IIRC…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Except large donors?  </p>
<p>IIRC, TP is currently registered NGO as 501(c)(3) but perhaps you should seek 501(c)(4) status.  That allows you to lobby freely, endorse candidates, and keep donor identities secret from US IRS.  Th catch (there's always a catch) is that donations are not tax-deductible.  But maybe its worth thinking about, as many other US based NGOs are doing:</p>
<p>theatlantic.com<br />
The Tax-Code Shift That’s Changing Liberal Activism<br />
Nonp<a href="https://www.theatlantic.com/ideas/archive/2018/11/501c3-501c4-activists-and-tax-code/576364/Non-profit" rel="nofollow">https://www.theatlantic.com/ideas/archive/2018/11/501c3-501c4-activists…</a> groups that used to focus their energies on litigation and education are structuring themselves to be political players.<br />
David Pozen, Columbia Law School</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-278533"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278533" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 20, 2018</p>
    </div>
    <a href="#comment-278533">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278533" class="permalink" rel="bookmark">Could you be more precise…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Could you be more precise about how Russia is now blocking the Tor network?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278541"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278541" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  steph
  </article>
    <div class="comment-header">
      <p class="comment__submitted">steph said:</p>
      <p class="date-time">November 21, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-278533" class="permalink" rel="bookmark">Could you be more precise…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-278541">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278541" class="permalink" rel="bookmark">Here&#039;s what OONI has…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Here's what OONI has gathered about anomalies with internet connections located in Russian Federation: <a href="https://explorer.ooni.io/country/RU" rel="nofollow">https://explorer.ooni.io/country/RU</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-278554"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278554" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to steph</p>
    <a href="#comment-278554">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278554" class="permalink" rel="bookmark">But it only shows blocking…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>But it only shows blocking illegal sites and "9/17/17" under each bar in chart. What about the Tor network itself?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-278544"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278544" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>user (not verified)</span> said:</p>
      <p class="date-time">November 21, 2018</p>
    </div>
    <a href="#comment-278544">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278544" class="permalink" rel="bookmark">new update firefox 
https:/…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>new update firefox </p>
<p><a href="https://techdows.com/2018/10/firefox-65-content-blocking-mode-to-come-with-standard-strict-and-custom-options.html" rel="nofollow">https://techdows.com/2018/10/firefox-65-content-blocking-mode-to-come-w…</a></p>
<p><a href="https://www.bleepingcomputer.com/news/software/mozilla-overhauls-content-blocking-settings-in-firefox-65/" rel="nofollow">https://www.bleepingcomputer.com/news/software/mozilla-overhauls-conten…</a></p>
<p>thanks for make tor faster and safer</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278567"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278567" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>zerostorm42 (not verified)</span> said:</p>
      <p class="date-time">November 23, 2018</p>
    </div>
    <a href="#comment-278567">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278567" class="permalink" rel="bookmark">For me is Tor the only…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For me is Tor the only possibility to get privacy and freedom back.<br />
Everywhere else goverment and its executive spionage you<br />
Everywhere enterprises are spionage you.<br />
Tor is for me also a room for free speech without anxity and that is a lot of worth.<br />
Greetings to all in the world who use Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278584"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278584" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Eric (not verified)</span> said:</p>
      <p class="date-time">November 24, 2018</p>
    </div>
    <a href="#comment-278584">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278584" class="permalink" rel="bookmark">Looking forward to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Looking forward to understanding tor new to it all</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278617"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278617" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 27, 2018</p>
    </div>
    <a href="#comment-278617">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278617" class="permalink" rel="bookmark">https://www.torproject.org…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.torproject.org/about/contact.html.en#mail" rel="nofollow">https://www.torproject.org/about/contact.html.en#mail</a></p>
<p>&gt; The Tor Project<br />
&gt; 217 1st Ave South #4903<br />
&gt; Seattle, WA 98194 USA</p>
<p>Didn't you (?) say a few weeks ago that the new address is a USPS box?   I can't find it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-278621"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278621" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 27, 2018</p>
    </div>
    <a href="#comment-278621">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278621" class="permalink" rel="bookmark">@ Iranian blogger:
Both IR…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@ Iranian blogger:</p>
<p>Both IR and US teachers regularly complain (with justice) that they are underpaid, and even stage walkouts and other street protests.  If they made commmon cause, perhaps they could shame both governments regarding their priorities, which too often don't seem to rate education very highly.</p>
</div>
  </div>
</article>
<!-- Comment END -->
