title: Tor Browser 6.0a4-hardened is released
---
pub_date: 2016-03-18
---
author: boklm
---
tags:

tor browser
tbb
tbb-6.0
---
categories:

applications
releases
---
_html_body:

<p>A new hardened Tor Browser release is available. It can be found in the <a href="https://dist.torproject.org/torbrowser/6.0a4-hardened/" rel="nofollow">6.0a4-hardened distribution directory</a> and on the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-hardened" rel="nofollow">download page for hardened builds</a>.</p>

<p>This release updates firefox to <a href="https://www.mozilla.org/en-US/firefox/38.7.1/releasenotes/" rel="nofollow">38.7.1</a>. Mozilla decided to disable the Graphite library in this release and we are taking the same action: irrespective of the security slider settings the Graphite library won't be used for rendering fonts in Tor Browser 6.0a4-hardened. The Graphite font rendering library was already disabled for users on the security level "High" or "Medium-High".</p>

<p><strong>Note</strong>: There is no incremental update from 6.0a3-hardened available due to <a href="https://trac.torproject.org/projects/tor/ticket/17858" rel="nofollow">bug 17858</a>. The internal updater should work, though, doing a complete update.</p>

<p>Here is the complete changelog since 6.0a3-hardened:</p>

<p>Tor Browser 6.0a4-hardened -- March 18 2016</p>

<ul>
<li>
    All Platforms
<ul>
<li>
        Update Firefox to 38.7.1esr
      </li>
<li>
        Update Torbutton to 1.9.5.2
<ul>
<li>
            <a href="https://trac.torproject.org/projects/tor/ticket/18557" rel="nofollow">Bug 18557</a>: Exempt Graphite from the Security Slider
          </li>
</ul>
</li>
<li>
        <a href="https://trac.torproject.org/projects/tor/ticket/18536" rel="nofollow">Bug 18536</a>: Make Mosaddegh and MaBishomarim available on port 80 and 443
      </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-164398"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164398" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 19, 2016</p>
    </div>
    <a href="#comment-164398">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164398" class="permalink" rel="bookmark">I&#039;ve had this problem for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've had this problem for several months now, where I can't watch embedded twitter videos on websites anymore, even with NoScript turned off and security level at the lowest setting.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-164767"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164767" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-164398" class="permalink" rel="bookmark">I&#039;ve had this problem for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-164767">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164767" class="permalink" rel="bookmark">Are you sure it&#039;s a problem</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are you sure it's a problem with the browser?  Twitter might have blocked their servers from serving video to Tor exit nodes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-164403"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164403" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 19, 2016</p>
    </div>
    <a href="#comment-164403">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164403" class="permalink" rel="bookmark">جميل</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>جميل</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-164589"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164589" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2016</p>
    </div>
    <a href="#comment-164589">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164589" class="permalink" rel="bookmark">Tor isn&#039;t for bogging down</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor isn't for bogging down the bandwidth watching Twitter videos. Really!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-164634"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164634" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2016</p>
    </div>
    <a href="#comment-164634">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164634" class="permalink" rel="bookmark">Thanks for helping us</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for helping us protect ourselves a bit better from those who are convinced that it's their God-given right to violate our civil liberties.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-164754"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164754" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2016</p>
    </div>
    <a href="#comment-164754">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164754" class="permalink" rel="bookmark">......</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>......</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-164761"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164761" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2016</p>
    </div>
    <a href="#comment-164761">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164761" class="permalink" rel="bookmark">希望你们的TOR路由器</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>希望你们的TOR路由器开发MAC地址克隆的功能，达到更好的匿名性。</p>
<p>Google Translate:<br />
I hope you develop the TOR router MAC address cloning feature to achieve better anonymity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-164763"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164763" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2016</p>
    </div>
    <a href="#comment-164763">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164763" class="permalink" rel="bookmark">MAC地址欺骗功能对于</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>MAC地址欺骗功能对于匿名用户可是非常有用。</p>
<p>Google Translate:<br />
MAC address spoofing function, but very useful for anonymous users.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-164768"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164768" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 20, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-164763" class="permalink" rel="bookmark">MAC地址欺骗功能对于</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-164768">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164768" class="permalink" rel="bookmark">Tails has some MAC address</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tails has some MAC address spoofing features:<br />
<a href="https://tails.boum.org/doc/first_steps/startup_options/mac_spoofing/index.en.html" rel="nofollow">https://tails.boum.org/doc/first_steps/startup_options/mac_spoofing/ind…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-164777"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164777" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to boklm</p>
    <a href="#comment-164777">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164777" class="permalink" rel="bookmark">集成到TOR里面更方便</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>集成到TOR里面更方便，我想任何TOR用户都不希望暴露自己的真实身份。</p>
<p>Google Translate:<br />
Integrated into the TOR which is more convenient, I think any TOR users do not want to expose his true identity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-164781"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164781" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 21, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-164777" class="permalink" rel="bookmark">集成到TOR里面更方便</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-164781">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164781" class="permalink" rel="bookmark">This is difficult to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is difficult to integrate this into Tor directly. This is something that should be done at the operating system level.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-164819"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164819" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to boklm</p>
    <a href="#comment-164819">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164819" class="permalink" rel="bookmark">I understand</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I understand</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-165051"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-165051" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to boklm</p>
    <a href="#comment-165051">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-165051" class="permalink" rel="bookmark">My friend uses obs4 and has</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My friend uses obs4 and has one server then an exit server.<br />
All of the country ids are in a UKUSA agreement country or a third party country except occasionally in russia but never without being in a UKUSA country also.<br />
UKUSA country and third party countries UK, USA, Australia, New Zealand, Norway, Netherlands, Sweden, Germany, Italy, Spain, Belgium, Canada, France and Demark.<br />
My friend is concerned that the tor relay and traffic us being compromised?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-165084"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-165084" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to boklm</p>
    <a href="#comment-165084">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-165084" class="permalink" rel="bookmark">why hasn&#039;t my post come up</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>why hasn't my post come up on the blog page re UKUSA?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-165527"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-165527" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to boklm</p>
    <a href="#comment-165527">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-165527" class="permalink" rel="bookmark">Technitium Mac Address</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Technitium Mac Address Changer is a free, extremely easy to use software at an operating system level.<br />
the above suggested software   allows any wired or wireless  port to have it's mac address changed to a random one chosen out of hundreds of thousands of bona fide mac addresses in the database.  there are no doubt many such softwares all offering the same potential </p>
<p>however RE your original concern, it is my understanding that a mac address is only known on the local network (eg , at a public wi fi spot your computer can be identified out of all the users) but the mac address is never known to the server hosting your internet or the web pages your visit.   I believe thus (please correct me if I'm wrong) that mac address is only a reasonably superficial level of vulnerability</p>
<p>however if the 'powers that be' know your computer or device belongs to  'you ', then they know your mac address = ' you '</p>
<p>So this may have ramifications in a court if they need to prove a link</p>
<p>many more experienced viewers may wish to correct me</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-165097"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-165097" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to boklm</p>
    <a href="#comment-165097">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-165097" class="permalink" rel="bookmark">Last Tails version that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Last Tails version that spoofed MAC addresses for me was Tails 1.8.2. Starting with Tails 2.0, message appears that my network card is disconnected and I have to unselect 'Enable MAC addrssing spoofing' to connect to the internet.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-164765"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164765" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2016</p>
    </div>
    <a href="#comment-164765">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164765" class="permalink" rel="bookmark">Your little Tor Project</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Your little Tor Project amounts to extortion. I am going to track you cunts down and rip your fucking heads off.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-164770"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164770" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 20, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-164765" class="permalink" rel="bookmark">Your little Tor Project</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-164770">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164770" class="permalink" rel="bookmark">I&#039;m not sure what you are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm not sure what you are talking about. We don't do any extortion.</p>
<p>Is your computer infected by some ransomware software such as CryptoLocker maybe?<br />
<a href="https://blog.torproject.org/blog/tor-misused-criminals" rel="nofollow">https://blog.torproject.org/blog/tor-misused-criminals</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-164778"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164778" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2016</p>
    </div>
    <a href="#comment-164778">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164778" class="permalink" rel="bookmark">Where can we get PHP Tor for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where can we get PHP Tor for server for anonimizer?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-164784"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-164784" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  boklm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">boklm said:</p>
      <p class="date-time">March 21, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-164778" class="permalink" rel="bookmark">Where can we get PHP Tor for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-164784">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-164784" class="permalink" rel="bookmark">I don&#039;t understand your</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't understand your question. Are you asking how to setup a PHP website as an onion service?</p>
<p>If you are setting up an onion service, this page can help:<br />
<a href="https://help.riseup.net/en/security/network-security/tor/onionservices-best-practices" rel="nofollow">https://help.riseup.net/en/security/network-security/tor/onionservices-…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-166125"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-166125" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to boklm</p>
    <a href="#comment-166125">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-166125" class="permalink" rel="bookmark">I need exit node personal</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I need exit node personal for me from this server with the Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-165085"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-165085" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 21, 2016</p>
    </div>
    <a href="#comment-165085">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-165085" class="permalink" rel="bookmark">what is the VPN to use on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>what is the VPN to use on TOR.? and what is the best way to set up TOR so it is at the best levels of anonymity for use on the "Dark_Web" </p>
<p>Peace</p>
<p>XxXxX</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-165292"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-165292" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2016</p>
    </div>
    <a href="#comment-165292">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-165292" class="permalink" rel="bookmark">http://browwser.ru/blog/tor/t</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://browwser.ru/blog/tor/tor-browser-60a4-hardened-released/" rel="nofollow">http://browwser.ru/blog/tor/tor-browser-60a4-hardened-released/</a><br />
For Russians</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-165470"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-165470" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2016</p>
    </div>
    <a href="#comment-165470">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-165470" class="permalink" rel="bookmark">i like your hard work
but i</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i like your hard work<br />
but i am also very upset by your meaningless Captchas</p>
<p>even when i open a saved/bookmarked link in TOR it still appears , and not only single CAPTCHA fucking series of them</p>
<p>CAN YOU PLEASE DO SOMETHING FOR THIS MATTER</p>
<p>please don't say that this is for safety ... it,s insane having too many</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-165629"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-165629" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">March 23, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-165470" class="permalink" rel="bookmark">i like your hard work
but i</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-165629">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-165629" class="permalink" rel="bookmark">These are not our CAPTCHAs</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>These are not our CAPTCHAs but Cloudflare's. We are working with them to make it easier for Tor users to access websites.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-168059"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-168059" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 01, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-168059">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-168059" class="permalink" rel="bookmark">Would it be possible to move</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Would it be possible to move the "Get new Tor circuit for this site" menu option to its own button? It'd make blocking bypass so much easier.  (Even better would be if, once clicked, it detected the cloudflare or other common blocking service header/title and automatically kept trying to get new circuits for some amount of tries or the actual page loads.) I can only click-scroll-click so many times until I just give up.</p>
<p>The proof of work solution sounds ridiculous, imo. What's to stop a botmaster from just ripping it out of the tor browser code and popping it in it's bot? (Run out of tokens? Then make a new "browser".) Also, yes, let's place more processor/energy burden on people who are only trying to protect their anonymity. When everything already goes more slowly through tor...<br />
<a href="https://github.com/gtank/captcha-draft/blob/master/captcha-plugin-draft.txt" rel="nofollow">https://github.com/gtank/captcha-draft/blob/master/captcha-plugin-draft…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-169073"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-169073" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 06, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to gk</p>
    <a href="#comment-169073">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-169073" class="permalink" rel="bookmark">Cloudflare is malware</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Cloudflare is malware parading as securing software.  The most worthless garbage ever developed.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-166193"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-166193" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 24, 2016</p>
    </div>
    <a href="#comment-166193">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-166193" class="permalink" rel="bookmark">&quot;Make Mosaddegh and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Make Mosaddegh and MaBishomarim available on port 80 and 443 " what is this?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-167754"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-167754" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 30, 2016</p>
    </div>
    <a href="#comment-167754">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-167754" class="permalink" rel="bookmark">Is it just me or is there a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is it just me or is there a vulnerability to code distribution if this website goes dark or DDS attack.<br />
Would a crypto file in Tor be consulted for updates or ip for site recovery if primary routes become "unreachable"?</p>
<p>would an alert popup "primary ip for update not resolving"<br />
relaunhcing tor to firewalled backup node.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-169227"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-169227" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 07, 2016</p>
    </div>
    <a href="#comment-169227">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-169227" class="permalink" rel="bookmark">I can&#039;t run this on Gentoo</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can't run this on Gentoo Hardened. Are there any external dependencies I need to have already installed?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-169435"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-169435" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 08, 2016</p>
    </div>
    <a href="#comment-169435">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-169435" class="permalink" rel="bookmark">Pls can I use tor to vote on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Pls can I use tor to vote on online contest more than once?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-169535"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-169535" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 08, 2016</p>
    </div>
    <a href="#comment-169535">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-169535" class="permalink" rel="bookmark">What&#039;s the deal with these</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What's the deal with these experimental browsers? I check for update, still on the 5.4 (or something) but then i see this 6.0 for windows? Could some1 please explain</p>
</div>
  </div>
</article>
<!-- Comment END -->
