title: Tor 0.2.1.7-alpha released
---
pub_date: 2008-11-21
---
author: phobos
---
tags:

bugs
alpha release
bug fixes
---
categories: releases
---
_html_body:

<p>Tor 0.2.1.7-alpha fixes a major security problem in Debian and Ubuntu<br />
packages (and maybe other packages) noticed by Theo de Raadt, fixes<br />
a smaller security flaw that might allow an attacker to access local<br />
services, adds better defense against DNS poisoning attacks on exit<br />
relays, further improves hidden service performance, and fixes a variety<br />
of other issues.</p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p>Changes in version 0.2.1.7-alpha - 2008-11-08</p>

<p><strong>Security fixes:</strong></p>

<ul>
<li>The "ClientDNSRejectInternalAddresses" config option wasn't being<br />
      consistently obeyed: if an exit relay refuses a stream because its<br />
      exit policy doesn't allow it, we would remember what IP address<br />
      the relay said the destination address resolves to, even if it's<br />
      an internal IP address. Bugfix on 0.2.0.7-alpha; patch by rovv.</li>
<li>The "User" and "Group" config options did not clear the<br />
      supplementary group entries for the Tor process. The "User" option<br />
      is now more robust, and we now set the groups to the specified<br />
      user's primary group. The "Group" option is now ignored. For more<br />
      detailed logging on credential switching, set CREDENTIAL_LOG_LEVEL<br />
      in common/compat.c to LOG_NOTICE or higher. Patch by Jacob Appelbaum<br />
      and Steven Murdoch. Bugfix on 0.0.2pre14. Fixes bug 848.</li>
<li>Do not use or believe expired v3 authority certificates. Patch<br />
      from Karsten. Bugfix in 0.2.0.x. Fixes bug 851.</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>Now NodeFamily and MyFamily config options allow spaces in<br />
      identity fingerprints, so it's easier to paste them in.<br />
      Suggested by Lucky Green.</li>
<li>Implement the 0x20 hack to better resist DNS poisoning: set the<br />
      case on outgoing DNS requests randomly, and reject responses that do<br />
      not match the case correctly. This logic can be disabled with the<br />
      ServerDNSRamdomizeCase setting, if you are using one of the 0.3%<br />
      of servers that do not reliably preserve case in replies. See<br />
      "Increased DNS Forgery Resistance through 0x20-Bit Encoding"<br />
      for more info.</li>
<li>Preserve case in replies to DNSPort requests in order to support<br />
      the 0x20 hack for resisting DNS poisoning attacks.</li>
</ul>

<p><strong>Hidden service performance improvements:</strong></p>

<ul>
<li>When the client launches an introduction circuit, retry with a<br />
      new circuit after 30 seconds rather than 60 seconds.</li>
<li>Launch a second client-side introduction circuit in parallel<br />
      after a delay of 15 seconds (based on work by Christian Wilms).</li>
<li>Hidden services start out building five intro circuits rather<br />
      than three, and when the first three finish they publish a service<br />
      descriptor using those. Now we publish our service descriptor much<br />
      faster after restart.</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>Minor fix in the warning messages when you're having problems<br />
      bootstrapping; also, be more forgiving of bootstrap problems when<br />
      we're still making incremental progress on a given bootstrap phase.</li>
<li>When we're choosing an exit node for a circuit, and we have<br />
      no pending streams, choose a good general exit rather than one that<br />
      supports "all the pending streams". Bugfix on 0.1.1.x. Fix by rovv.</li>
<li>Send a valid END cell back when a client tries to connect to a<br />
      nonexistent hidden service port. Bugfix on 0.1.2.15. Fixes bug<br />
      840. Patch from rovv.</li>
<li>If a broken client asks a non-exit router to connect somewhere,<br />
      do not even do the DNS lookup before rejecting the connection.<br />
      Fixes another case of bug 619. Patch from rovv.</li>
<li>Fix another case of assuming, when a specific exit is requested,<br />
      that we know more than the user about what hosts it allows.<br />
      Fixes another case of bug 752. Patch from rovv.</li>
<li>Check which hops rendezvous stream cells are associated with to<br />
      prevent possible guess-the-streamid injection attacks from<br />
      intermediate hops. Fixes another case of bug 446. Based on patch<br />
      from rovv.</li>
<li>Avoid using a negative right-shift when comparing 32-bit<br />
      addresses. Possible fix for bug 845 and bug 811.</li>
<li>Make the assert_circuit_ok() function work correctly on circuits that<br />
      have already been marked for close.</li>
<li>Fix read-off-the-end-of-string error in unit tests when decoding<br />
      introduction points.</li>
<li>Fix uninitialized size field for memory area allocation: may improve<br />
      memory performance during directory parsing.</li>
<li>Treat duplicate certificate fetches as failures, so that we do<br />
      not try to re-fetch an expired certificate over and over and over.</li>
<li>Do not say we're fetching a certificate when we'll in fact skip it<br />
      because of a pending download.</li>
</ul>

<p>The original announcement can be found at <a href="http://archives.seul.org/or/talk/Nov-2008/msg00229.html" rel="nofollow">http://archives.seul.org/or/talk/Nov-2008/msg00229.html</a></p>

---
_comments:

<a id="comment-384"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-384" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2008</p>
    </div>
    <a href="#comment-384">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-384" class="permalink" rel="bookmark">OT: link on download page to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OT: link on download page to stable OS X bundle is incorrect - should point to <a href="https://www.torproject.org/dist/vidalia-bundles/vidalia-bundle-0.2.0.32-0.1.10-universal.dmg" rel="nofollow">https://www.torproject.org/dist/vidalia-bundles/vidalia-bundle-0.2.0.32…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-385"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-385" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">November 21, 2008</p>
    </div>
    <a href="#comment-385">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-385" class="permalink" rel="bookmark">nice catch</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Fixed in svn.  Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-387"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-387" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 21, 2008</p>
    </div>
    <a href="#comment-387">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-387" class="permalink" rel="bookmark">upgrade to 2.1.7 on OS X</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>upgrade to 2.1.7 on OS X doesn't install.  Complains that current version (2.1.6) is newer than the version being installed</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-395"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-395" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">November 24, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-387" class="permalink" rel="bookmark">upgrade to 2.1.7 on OS X</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-395">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-395" class="permalink" rel="bookmark">which OS X?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Feel free to submit a bug at bugs.torproject.org.  I can't re-create this on any of my OS X test systems.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-390"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-390" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="https://blog.torproject.org">seef20002000 (not verified)</a> said:</p>
      <p class="date-time">November 22, 2008</p>
    </div>
    <a href="#comment-390">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-390" class="permalink" rel="bookmark">i wouldlike to one of your</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i wouldlike to one of your members</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-391"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-391" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="https://blog.torproject.org">seef20002000 (not verified)</a> said:</p>
      <p class="date-time">November 22, 2008</p>
    </div>
    <a href="#comment-391">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-391" class="permalink" rel="bookmark">thanks</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-393"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-393" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">November 24, 2008</p>
    </div>
    <a href="#comment-393">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-393" class="permalink" rel="bookmark">when will be the new tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>when will be the new tor browser bundle is coming with Tor 0.2.1.7 alpha? Please make it fast guys.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-396"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-396" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">November 24, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-393" class="permalink" rel="bookmark">when will be the new tor</a> by <span>PETER (not verified)</span></p>
    <a href="#comment-396">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-396" class="permalink" rel="bookmark">Soon</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Building the TBB is a manual process right now.  We also will update to Firefox 2.0.0.18, which needs some investigation to make sure everything works the same wa as before.  It's coming.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-394"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-394" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>krishna (not verified)</span> said:</p>
      <p class="date-time">November 24, 2008</p>
    </div>
    <a href="#comment-394">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-394" class="permalink" rel="bookmark">multiple circuits in tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello, </p>
<p>Can anyone explain why the support for multiple circuits was removed in the latest version of TOR? I see it as a way to improve speed. Thanks.</p>
<p>Krishna</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-397"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-397" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">November 24, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-394" class="permalink" rel="bookmark">multiple circuits in tor</a> by <span>krishna (not verified)</span></p>
    <a href="#comment-397">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-397" class="permalink" rel="bookmark">We didn&#039;t.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why do you think we removed this ability?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-409"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-409" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 29, 2008</p>
    </div>
    <a href="#comment-409">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-409" class="permalink" rel="bookmark">ver 0.2.0.32</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>what is difference between TOR 0.2.0.31 and 0.2.0.32<br />
0.2.0.32 on download page, but what is differecne between that version and 0.2.0.31<br />
also, which ver is better, stable or unstable, for users TOR strictly for anonymity (not testing soft)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-411"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-411" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">December 01, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-409" class="permalink" rel="bookmark">ver 0.2.0.32</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-411">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-411" class="permalink" rel="bookmark">the difference</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor 0.2.0.32 is going to be the next stable release.  We were all set to release, until we ran into <a href="https://bugs.torproject.org/flyspray/index.php?do=details&amp;id=876" rel="nofollow">one issue on OS X</a>.  The code is ready to release and be announced shortly.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-446"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-446" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Leaps (not verified)</span> said:</p>
      <p class="date-time">December 21, 2008</p>
    </div>
    <a href="#comment-446">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-446" class="permalink" rel="bookmark">Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,</p>
<p>I'm running: </p>
<p>Vidalia 0.0.16<br />
Tor 0.1.2.19<br />
Qt 4.3.2</p>
<p>It's basically just running in the background. Am I protecting myself from anything by having it running or am I not using it properly. I went to the Tor website but the content and getting started is soooo lengthy that I can't get a grasp on if I'm using it correctly.</p>
<p>Any quick reference guides or advice would be greatly appreciated.</p>
<p>L</p>
</div>
  </div>
</article>
<!-- Comment END -->
