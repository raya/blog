title: Whither website translations
---
pub_date: 2011-09-28
---
author: phobos
---
tags:

bugs
website translations
polyglot internet
translators
---
categories:

localization
releases
---
_html_body:

<p>As of <a href="https://lists.torproject.org/pipermail/tor-commits/2011-September/035605.html" rel="nofollow">svn commit 25130</a> we no longer have our web pages translated. We continue to translate tor, vidalia, aurora/firefox, our manual pages, and other documentation included with the various bundles. We're also going to create a new, simplified user manual that can be translated and included in our various packages.</p>

<p>I thank our translators, both past and current, and continue to be impressed at the community we've built up around translating all things Tor. We appreciate it, our users appreciate, and I appreciate it.</p>

<p>Why did we do this?</p>

<ul>
<li>We spend a lot of time managing the translated pages on the website.</li>
<li>The English pages are frequently updated, thereby invalidating all of the other languages.</li>
<li>The switch to <a href="https://www.transifex.net/start/" rel="nofollow">Transifex</a> hasn't made more translators appear, especially for the languages we may care about the most.</li>
<li>The translators for many languages that have a right-to-left orientation complain that the transifex interface is a nightmare to use and refuse to update.</li>
<li>Lots of users simply use google translate, or read English anyway.</li>
<li>Getting translated software into the hands of people is more important than them reading our web pages (especially if the documentation is translated and available locally)</li>
<li>Our translations for right to left languages, such as Arabic and Farsi, were growing out of date and required far more technical skill to do than more translators are capable of handling. This involved editing the raw perl templating system we used.</li>
<li>And, according to our webserver logs, our translated website traffic is less than 2% of total GET requests, nevermind page views.</li>
</ul>

<p>We may go back to the old model, which was svn or git commits directly to the repository for translations. It seemed this model attracted a few dedicated translators for some interesting languages. These languages were always kept current with the English pages. Another option is automated translation, such as something Bayesian that understands the difference between the snack cookie and a web browser cookie.</p>

<p>If you have suggestions or other ideas how to improve our translations, feel free to respond to <a href="https://trac.torproject.org/projects/tor/ticket/4082" rel="nofollow">ticket 4082</a>.</p>

<p>I still look forward to universal translators, helping me to understand the <a href="http://www.ethanzuckerman.com/blog/the-polyglot-internet/" rel="nofollow">polyglot Internet</a>. There is so much content out there that isn't in languages I understand. I hope universal translators can greatly improve in a short time.</p>

---
_comments:

<a id="comment-11918"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11918" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 28, 2011</p>
    </div>
    <a href="#comment-11918">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11918" class="permalink" rel="bookmark">hi!i thinks we need a FORUM</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi!i thinks we need a FORUM for TOR user!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-11928"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11928" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 29, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11918" class="permalink" rel="bookmark">hi!i thinks we need a FORUM</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-11928">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11928" class="permalink" rel="bookmark">hi again.really i feel a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi again.really i feel a forum is able to cover all subject and ideas.a good place to share our knowledge and expand our awarness of computer security and related subjects.do the tor project forum site is not a good idea?thanks.b</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-11947"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11947" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 30, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11928" class="permalink" rel="bookmark">hi again.really i feel a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-11947">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11947" class="permalink" rel="bookmark">Since there isnt a forum, I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Since there isnt a forum, I use Tor regularly from home but am currently staying in a college dorm where we use the residential network (resnet) and also a program called client security agent which simply makes sure you are running updated windows software and an up to date firewall. I have not used Tor here yet and am curious if anyone knows if I'll have troubles with anonymity while using it here.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-12119"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12119" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 14, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11918" class="permalink" rel="bookmark">hi!i thinks we need a FORUM</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-12119">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12119" class="permalink" rel="bookmark">yes I agree with you. we</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>yes I agree with you. we need a forum.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-11919"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11919" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 28, 2011</p>
    </div>
    <a href="#comment-11919">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11919" class="permalink" rel="bookmark">Anyone who is aware of and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Anyone who is aware of and technically competent enough to use Tor is very likely able to understand English.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11920"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11920" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 28, 2011</p>
    </div>
    <a href="#comment-11920">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11920" class="permalink" rel="bookmark">im from iran  we have error</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>im from iran  we have error with vidalia 0.2.14 in windows  </p>
<p>Sep 28 22:05:59.875 [Error] nodelist_assert_ok(): Bug: nodelist.c:397: nodelist_assert_ok: Assertion md-&gt;held_by_node == 1 failed; aborting.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-11926"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11926" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">September 28, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11920" class="permalink" rel="bookmark">im from iran  we have error</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-11926">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11926" class="permalink" rel="bookmark">This was a bug in the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This was a bug in the development (alpha) version of Tor.</p>
<p>You can either move to using the stable bundle, or you can wait for the update (coming soon) for the development version.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-11927"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11927" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 29, 2011</p>
    </div>
    <a href="#comment-11927">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11927" class="permalink" rel="bookmark">As a long time Italian</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As a long time Italian translator for the Tor project, I believe the key issue is this:</p>
<p>"The English pages are frequently updated, thereby invalidating all of the other languages."</p>
<p>The pace of website update has always been fast and the changes were trivial most of the times. I remember well how frustrating it was to keep up.</p>
<p>Moreover, in my personal case, the move from svn to a web-based interface completely broke my workflow and did not fit into my habits/schedule.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11931"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11931" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 29, 2011</p>
    </div>
    <a href="#comment-11931">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11931" class="permalink" rel="bookmark">How about instead of a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How about instead of a direct translation of every page, and every change, have more general page in each language?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11948"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11948" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 30, 2011</p>
    </div>
    <a href="#comment-11948">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11948" class="permalink" rel="bookmark">This is Wei from India</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is Wei from India ▬▬▬&gt; I want to remind why you didn't tell on blog the update of stable Vidalia bundle from</p>
<p>0.2.2.(32)-0.2.14-1  to  0.2.2.(33)-0.2.14-1</p>
<p>I didnt find any release notes or posts on tor website about this new release</p>
<p>Have doubt someone tampered it after the DigiNotar incident.</p>
<p>Please clarify our doubts....</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-12664"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-12664" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 14, 2011</p>
    </div>
    <a href="#comment-12664">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-12664" class="permalink" rel="bookmark">why can&#039;nt we Play any Video</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>why can'nt we Play any Video Through TOR?<br />
i am using it but i am not satisfied.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13067"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13067" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2011</p>
    </div>
    <a href="#comment-13067">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13067" class="permalink" rel="bookmark">Hi
I&#039;m a teacher from</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi<br />
I'm a teacher from Corsica. I installed Tor and HTTPS everywhere so as to see videos for kids on B B C and P B S websites. When I go there, latest version of flash player is required but I already have it. So what's the problem? is there a US version of flash?<br />
Thanks for helping me.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13333"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13333" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 31, 2011</p>
    </div>
    <a href="#comment-13333">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13333" class="permalink" rel="bookmark">please add a forum for user</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>please add a forum for user issues. thank you</p>
<p>and note the following results which prior to using Tor showed all ports "Stealth" and passing via shieldsup. - - - </p>
<p>GRC Port Authority Report created on UTC: 2011-12-31 at 07:42:29</p>
<p>Results from scan of ports: 0, 21-23, 25, 79, 80, 110, 113,<br />
                            119, 135, 139, 143, 389, 443, 445,<br />
                            1002, 1024-1030, 1720, 5000</p>
<p>    2 Ports Open<br />
   21 Ports Closed<br />
    3 Ports Stealth<br />
---------------------<br />
   26 Ports Tested</p>
<p>Ports found to be OPEN were: 22, 80</p>
<p>Ports found to be STEALTH were: 135, 139, 445</p>
<p>Other than what is listed above, all ports are CLOSED.</p>
<p>TruStealth: FAILED - NOT all tested ports were STEALTH,<br />
                   - NO unsolicited packets were received,<br />
                   - A PING REPLY (ICMP Echo) WAS RECEIVED.</p>
</div>
  </div>
</article>
<!-- Comment END -->
