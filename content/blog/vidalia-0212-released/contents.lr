title: Vidalia 0.2.12 is released
---
pub_date: 2011-04-13
---
author: erinn
---
tags:

vidalia
bug fixes
improvements
vidalia release
---
categories:

applications
releases
---
_html_body:

<p>The new release of Vidalia 0.2.12 is out. We'd also like to congratulate Tomás Touceda on his first release and thank him for all his work and patience in getting this out!</p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p><strong>0.2.12  10-Apr-2011</strong></p>

<ul>
<li>Vidalia's SVN repository has been migrated to Git. All branches but<br />
    master have been archived for later review, since SVN trunk had changed<br />
    significantly; they should be reviewed later to determine whether<br />
    they can and should still be merged. All \version $Id$ headers have been<br />
    removed since Git does not support $Id$.</li>
<li>As part of the move, Vidalia's Trac is now at:<br />
      <a href="https://trac.torproject.org/" rel="nofollow">https://trac.torproject.org/</a><br />
    All Trac numbers in Vidalia 0.2.12 and beyond refer to the new Trac<br />
    entries. The old Trac is archived for posterity at:<br />
      <a href="https://trac-vidalia.torproject.org/projects/vidalia" rel="nofollow">https://trac-vidalia.torproject.org/projects/vidalia</a></li>
<li>Add support for Tor's ControlSocket as an alternative to ControlPort. It<br />
    can be used for Linux maintainers to build a better default interaction<br />
    between Tor and Vidalia by just setting the right permissions and file<br />
    owner on the socket file for the connection. Using ControlSocket means<br />
    you don't need to worry about authentication methods with ControlPort.<br />
    Resolves bug 2091.</li>
<li>Add a way to edit arbitrary torrc entries while Tor is running. Now<br />
    Vidalia users have more flexibility for configuring Tor. This change<br />
    doesn't replace editing torrc directly, because on some systems<br />
    (like Debian) Tor can't write to its torrc file. Resolves bug 2083.</li>
<li>Remove Vidalia's direct dependency on OpenSSL. This dependency had<br />
    caused Vidalia to fail to run on FreeBSD (due to a bug in the FreeBSD<br />
    ports collection) and Fedora 14 (due to an incompatibility between<br />
    OpenSSL and Fedora's SELinux configuration). Resolves bug 2287 and<br />
    2611.</li>
<li>Restore compatibility with Windows 2000. An update to the MiniUPnPc<br />
    library had introduced an unnecessary dependency on a system library<br />
    not included in Windows 2000. Fixes bug 2612.</li>
<li>Fix how the advanced message log window displays message updates when<br />
    messages are coming in too quickly, for example when you're listening<br />
    to debug-level messages from Tor. Fixes bug 2093.</li>
<li>Add a what's this? link to the bridge option to explain in a more verbose<br />
    fashion what being a bridge involves. Resolves bug 1995.</li>
<li>Prompt users to restart Tor after changing the path to torrc. Fixes bug<br />
    2086.</li>
<li>Disable the directory port configuration field when configuring a<br />
    bridge. A bridge does not need to operate a separate directory port,<br />
    and operating one can make a bridge easier to detect. Fixes bug 2431.</li>
<li>When Vidalia asks Tor for a bridge's usage history before anyone has<br />
    used it, correctly report that no clients have used the bridge recently.<br />
    Previously, it would incorrectly warn that it was unable to retrieve the<br />
    bridge's usage history. Fixes bug 2186.</li>
</ul>

