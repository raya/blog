title: Tor in the Media: 2020
---
pub_date: 2021-02-08
---
author: alsmith
---
tags: media coverage
---
_html_body:

<p>This year, we’re continuing a new tradition of reviewing media and news stories that mentioned Tor and the Tor Project. Our goal is to highlight what is changing (or not) in the conversation about privacy and censorship, as well as identifying the ways the media discusses Tor in the context of these challenges.</p>
<h2>When everything changed</h2>
<p>Last year started off on a “normal” note--we were preparing to dive into our roadmap for 2020, and news outlets were publishing articles <a href="https://techround.co.uk/interviews/interview-tor-project/">explaining Tor</a>, demonstrating <a href="https://www.theverge.com/2020/2/21/21138403/tor-privacy-tools-private-network-browser-settings-security">how to use Tor Browser to protect your privacy online,</a> and highlighting how <a href="https://bitcoinmagazine.com/articles/privacy-isnt-radical-how-tor-supports-dissent-as-a-human-right">privacy is a human right Tor fights</a> to make available for everyone online. And then COVID-19 changed everything.</p>
<p>When the pandemic hit the Tor Project, we had to make some hard decisions that became headline news: <a href="https://www.zdnet.com/article/tor-project-lays-off-a-third-of-its-staff/">Tor Project lays off a third of its staff 4/18/2020, and </a><a href="https://siliconangle.com/2020/04/19/covid-19-forces-tor-project-lay-off-third-staff/">COVID-19 forces Tor Project to lay off a third of its staff 4/19/2020</a>.</p>
<h2>Use Tor, fight the surveillance pandemic</h2>
<p>The COVID-19 pandemic changed everything, and to be online became an even more essential part of our daily lives. Many people began to understand that privacy online is now more important than ever. Journalists began looking for advice to give their readers about protecting their privacy, and we became <a href="https://economictimes.indiatimes.com/magazines/panache/tape-the-webcam-enable-firewall-11-rules-to-ensure-cyber-security-when-you-work-from-home/articleshow/75005471.cms">‘rule #11’ on how to ensure cyber security while working from home</a>:</p>
<blockquote><p><em>11. Secure browsing</em></p>
<p><em>If you want an extra layer of security and privacy, it is a good idea to install the Tor browser. It comes with many security features, which makes Web-based attacks difficult to execute on your computer.</em></p>
</blockquote>
<p>Throughout the year, other outlets continued writing stories that highlight Tor as a tool to protect your privacy online. <a href="https://www.techradar.com/news/chromes-incognito-mode-wont-make-you-anonymous-but-there-are-other-options">TechRadar</a> and <a href="https://www.vice.com/en/article/y3gzgb/incognito-mode-is-actually-pretty-useless">Vice</a> both cited Tor in articles about how “incognito mode” is not enough to protect users’ privacy.</p>
<p>
The uprising for Black Lives during the summer raised the concern about state surveillance of activists, a topic discussed at our <a href="https://cointelegraph.com/news/snowden-and-human-rights-advocates-talk-internet-surveillance-in-the-era-of-blm">third PrivChat, with Snowden as the panel moderator.</a> <a href="https://www.vice.com/en/article/a37m4g/the-motherboard-guide-to-avoiding-state-surveillance-privacy-guide">Motherboard</a> wrote a great guide to avoiding state surveillance in which Tor is cited, and <a href="https://freedom.press/training/-depth-guide-choosing-web-browser/">Freedom of the Press</a> also wrote a comprehensive guide to ‘pick your browser’ that compared the privacy features in Tor Browser, Firefox, Brave, and Chrome. Tor Browser and other apps that use Tor were part of the <a href="https://mashable.com/video/david-dobrik-100k-puzzle/">Mashable</a> list of ‘apps you should have downloaded in 2020.’ <a href="https://www.expressvpn.com/blog/apps-integrate-tor-dark-web-encryption/">ExpressVPN</a> wrote an article describing the benefits of integrating Tor to apps, and recommended just building the services with onion services so Tor (and privacy) is part of an app’s design by default.</p>
<p>Last year showed us that now, more than ever, we have to keep working on Tor to make it easier to use and more accessible for more people. The results of this work made the news as well:</p>
<h2>Onion Services improvements</h2>
<p>The Tor Browser 9.5 release introduced new onion service features that improved the user experience. Changes include Onion-Location, a feature that makes it much easier for users to find and return to an .onion version of a website, a popular feature that made it easier to find secure onion services; improved onion services error messages; and the ability for administrators to password-protect .onion pages. This work was covered by <a href="https://www.pcmag.com/news/tor-browser-makes-it-easier-to-visit-mainstream-websites-onion-addresses">PCMagazine</a>, <a href="https://www.zdnet.com/article/tors-latest-release-makes-it-easier-to-find-secure-onion-services/">ZDNet</a>, <a href="https://www.ghacks.net/2020/06/05/tor-browser-9-5-is-out-with-major-usability-improvements/">ghacks</a> and <a href="https://www.bleepingcomputer.com/news/software/tor-browser-95-lets-websites-promote-their-onion-addresses/">BleepingComputer</a>.</p>
<p>This year we also announced the depreciation timeline for onion services v2. We aim to completely disable onion services v2 by October 15, 2021. Some projects have already migrated to v3, including Bitcoin Core, as was covered by <a href="https://decrypt.co/44640/bitcoins-next-upgrade-will-support-tor-v3-addresses">decrypto.co</a>.</p>
<p>And finally, we rolled out a prototype for human readable names for onion services in partnership with the <a href="https://securedrop.org/news/introducing-onion-names-securedrop/">SecureDrop</a> team, who wrote about this process on their blog.</p>
<h2>Tor Browser releases</h2>
<p>The Tor Browser 10.0 release is the first stable release of the 10.0 series based on Firefox 78esr--it included important security updates from Firefox and was covered by <a href="https://www.bleepingcomputer.com/news/software/tor-browser-10-released-to-sync-with-latest-firefox-esr-version/">BleepingComputer</a> and <a href="https://www.techradar.com/news/tor-browser-100-released-to-offer-a-more-private-web-experience">TechRadar</a>. Following this release, we reached an important milestone, which was to bring Tor Browser for Android to the new Android Firefox release Fenix--an effort that involved many months of design and development. Many news outlets covered our work, such as <a href="https://www.ghacks.net/2020/10/10/first-tor-browser-alpha-for-android-based-on-new-firefox-is-now-available/">ghacks</a>, <a href="https://timesofindia.indiatimes.com/gadgets-news/browsing-internet-safely-on-android-phones-becomes-easier-with-this-new-app/articleshow/79013318.cms">Times of India</a>, <a href="https://www.androidpolice.com/2020/11/02/tor-browser-for-android-now-based-on-updated-firefox-app/">Android Police</a>, and <a href="https://news.softpedia.com/news/tor-browser-for-android-receives-major-update-now-based-on-firefox-531455.shtml">Softpedia News</a>.</p>
<h2>Combating DDoS on onion services</h2>
<p>Our proposals (that are in the works, not yet shipped) on how to solve DDoS attacks against onion services received attention from several outlets, including <a href="https://www.bleepingcomputer.com/news/security/tor-project-shares-proposals-to-limit-ddos-impact-on-onion-sites/">BleepingComputer,</a> <a href="https://www.techradar.com/news/tor-will-finally-fix-a-bug-that-allowed-for-ddos-attacks-against-dark-web-sites">TechRadar</a> and <a href="https://techleash.com/tor-going-to-fix-bugs-for-ddos-against-dark-web-sites/">Tech Leash</a>, wrote about these proposals. <a href="https://www.coindesk.com/tor-anonymous-tokens-stop-hacks-dos-attacks">CoinDesk</a> published an article focused on our tokens proposal that we presented at our State of the Onion in November last year.</p>
<h2>Network Performance</h2>
<p><a href="https://portswigger.net/daily-swig/tor-project-rolls-out-program-to-turbo-charge-network-throughput">The Daily Swig</a> wrote about our very important work to improve the Tor network’s performance, making it faster for users, that we started in 2020.. Our goal is to improve one of the number one usability issues with Tor: that it’s too slow. Keep following the blog for more news on these improvements.</p>
<h2>Other wins</h2>
<p>Even though 2020 started with so many uncertainties and unknowns, we also had some important successes, and we are happy to share a few stories that make us proud.</p>
<h3>Belarus censorship circumvention</h3>
<p>This year, internet users in Belarus faced censorship, and Tor helped to provide them with circumvention. <a href="https://decrypt.co/38443/tor-and-psiphon-activity-surges-in-protest-stricken-belarus">Decrypt</a> wrote how Tor saw a surge in use during the protests in Belarus, and <a href="https://www.benzinga.com/general/20/11/18293601/how-tor-combats-internet-censorship">Benzinga</a> did a great in-depth article about how Tor combats internet censorship.</p>
<h3>Mexico unblocks Tor</h3>
<p>After many months of persistent work, volunteers and researches from the Tor community in Mexico not only managed to discover how the largest telecommunications operator in Mexico was blocking Tor, but also managed to contact them, get them to change their policies and stop blocking Tor, and convince them to run Tor relays and contribute to the network. This is a huge win for internet privacy and anti-censorship advocacy. GlobalVoices published an article in <a href="https://globalvoices.org/2020/09/08/we-made-the-largest-mexican-telecommunications-operator-stop-blocking-secure-internet/">English</a> and <a href="https://es.globalvoices.org/2020/08/18/asi-logramos-que-el-mas-grande-operador-de-telecomunicaciones-mexicano-dejara-de-bloquear-la-internet-segura/">Spanish</a> detailing the whole story.</p>
<h3>Launch of Tor’s Membership Program</h3>
<p><a href="https://www.coindesk.com/tor-onion-membership">CoinDesk</a> wrote about the Tor Project’s new Membership Program, a program we launched in 2020 and of which we are very proud. The Membership Program is designed to build a supportive relationship between our nonprofit and private sector organizations that use our technology or want to support our mission.</p>

---
_comments:

<a id="comment-291098"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291098" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>kanan (not verified)</span> said:</p>
      <p class="date-time">February 08, 2021</p>
    </div>
    <a href="#comment-291098">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291098" class="permalink" rel="bookmark">Congratulations! We need Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Congratulations! We need Tor now more than ever. Thanks to everyone involved with the project.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291100"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291100" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 08, 2021</p>
    </div>
    <a href="#comment-291100">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291100" class="permalink" rel="bookmark">&gt; at our third third PrivChat</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; at our third third PrivChat</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291108"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291108" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291100" class="permalink" rel="bookmark">&gt; at our third third PrivChat</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291108">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291108" class="permalink" rel="bookmark">Thanks. Fixed!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks. Fixed!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291104"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291104" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
    <a href="#comment-291104">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291104" class="permalink" rel="bookmark">Ironically, &quot;indiatimes.com&quot;…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ironically, "indiatimes.com" blocks tor</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291111"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291111" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291104" class="permalink" rel="bookmark">Ironically, &quot;indiatimes.com&quot;…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291111">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291111" class="permalink" rel="bookmark">Confirmed.
In the past I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Confirmed.</p>
<p>In the past I was able to visit that site.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291105"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291105" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Thomas (not verified)</span> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
    <a href="#comment-291105">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291105" class="permalink" rel="bookmark">The Android app, Conion,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The Android app, Conion, claims that it is based on Tor, and can encrypt messages end-to-end.<br />
Is it really reliable?</p>
<p>(<a href="https://play.google.com/store/apps/details?id=com.secapp.tor.conion&amp;hl=en_US&amp;gl=US" rel="nofollow">https://play.google.com/store/apps/details?id=com.secapp.tor.conion&amp;hl=…</a>)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291113"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291113" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291105" class="permalink" rel="bookmark">The Android app, Conion,…</a> by <span>Thomas (not verified)</span></p>
    <a href="#comment-291113">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291113" class="permalink" rel="bookmark">We can&#039;t vet every app that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We can't vet every app that claims to use Tor, much less determine if it is reliable or not. I will say that I have never heard of this app. Some questions to ask: are they open source? Can you look at the work they are doing and verify they are doing what they claim?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291106"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291106" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
    <a href="#comment-291106">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291106" class="permalink" rel="bookmark">Terima kasih Torproject.org</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Terima kasih Torproject.org</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291112"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291112" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
    <a href="#comment-291112">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291112" class="permalink" rel="bookmark">Great stories, and nice to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great stories, and nice to see Tor getting some good press.</p>
<p>Has TP considered responding via "email to the editor" to the more common misleading stories which spread FBI inspired FUD without mentioning the many benefits for ordinary people of using Tor Browser?  What about a boilerplate "if you would like to get a comment from Tor Project on your story, please email us at".  This way you would not waste time on "news" sources which have no interest in providing context for their readers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291114"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291114" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291112" class="permalink" rel="bookmark">Great stories, and nice to…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291114">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291114" class="permalink" rel="bookmark">We&#039;re working on some…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We're working on resources and strategies to better address this. Keep spreading the word!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291129"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291129" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 11, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291112" class="permalink" rel="bookmark">Great stories, and nice to…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291129">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291129" class="permalink" rel="bookmark">&gt; the many benefits for…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; the many benefits for ordinary people</p>
<p>A quick visit to the <a href="https://community.torproject.org/" rel="nofollow">community</a> page and <a href="https://blog.torproject.org/search/node?keys=tor+stories" rel="nofollow">Tor Stories</a> should cover a lot on that front. The old website also has a <a href="https://2019.www.torproject.org/about/torusers.html" rel="nofollow">page about Tor users</a>. For particular questions, the <a href="https://2019.www.torproject.org/docs/documentation.html.en" rel="nofollow">documentation</a> is linked at the top of torproject.org.</p>
<p>Some relevant blog tags: <a href="https://blog.torproject.org/category/tags/human-rights" rel="nofollow">human rights</a>, <a href="https://blog.torproject.org/search/node?keys=rights" rel="nofollow">the word "rights"</a>, <a href="https://blog.torproject.org/category/tags/censorship" rel="nofollow">censorship</a>, <a href="https://blog.torproject.org/category/tags/training" rel="nofollow">training</a>, <a href="https://blog.torproject.org/category/tags/eff" rel="nofollow">EFF</a>, <a href="https://blog.torproject.org/category/tags/media-coverage" rel="nofollow">media coverage</a>, <a href="https://blog.torproject.org/category/tags/free-speech" rel="nofollow">free speech</a>.</p>
<p>But all of this should be made more visible to the public. Tor users can reply with this info to articles, but one comment here or there is likely to be buried in the pile.</p>
<p>&gt; FBI inspired FUD</p>
<p>I suspect VPN and DDoS-mitigation company inspired also. They pay for tons of ads on TV in the U.S., and practically every Youtuber has shilled for one. Tor is an open, participatory, no-login, donation-funded competitor to centralized one-hop VPN, and Tor's trustless design to resist surveillance by global adversaries and its operators that so far necessarily tolerates DDoS is stigmatized which props up DDoS mitigation business models. Who knows if they aren't causing trouble in their favor through Tor themselves.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291136"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291136" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 11, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291129" class="permalink" rel="bookmark">&gt; the many benefits for…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291136">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291136" class="permalink" rel="bookmark">&gt; A quick visit to the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; A quick visit to the community page and Tor Stories should cover a lot on that front. The old website also has a page about Tor users. For particular questions, the documentation is linked at the top of torproject.org.</p>
<p>True, but let me try to restate the point I was trying to make.</p>
<p>Many well known national and local newspapers print stories several times a year which quote extensively from "information" (generally slanted and sometimes simply false) provided to them by anonymous FBI officials about some hair raising case involving horrifying crimes such as child sexual abuse or human trafficking, which often mention that the alleged malefactors "disguised their activities using Tor".  These stories almost never give a link to torproject.org, much less mention that human rights researchers and political dissidents also use Tor.  (Stories at Wired and ZDNet sometimes do, but these are exceptional.)  How many readers of such anti-privacy FUD will even guess that they might be able to find another view at torproject.org, much less bother to search for the link on their own initiative?</p>
<p>What I am suggesting is that Tor Project should be more assiduous in sending a polite boiler plate "email to the editor" saying that TP is aware that "a story at your outlet recently mentioned Tor", adding "if you would like a response from Tor Project, please email us at  or take a look at these urls from our website: ..."  Responsible news outlets will, one hopes, suddenly remember that they have a responsibility to provide context for disparaging information fed to them from a source with an obvious bias (FBI in this case).</p>
<p>&gt; I suspect VPN and DDoS-mitigation company inspired also.</p>
<p>Good point.  You may well be right about that.  And of course FBI loves people who think (incorrectly) that using a VPN is "safer"[sic] than using Tor Browser.  And I agree that a big part of the problem is that the virus scanning industry has a vested interest in ensuring that society does not make the kind of genuine cybersecurity improvements which would render their business less relevant.</p>
<p>Case in point: EFF issued a wonderful tool, Yaya, built upon the open source malware scanner Yara.  Alas, Yara is useless without the signatures which is what Yaya conveniently provides.  But it seems that the antivirus industry is preventing them from turning this into a Debian package which any ordinary person could easily download and use to obtain the latest signatures in order to scan their own devices.   Because the commercial virus scanning company customer base would be eroded by the EFF tool, if that ever became something which could be used by ordinary citizens to protect themselves, their families, their friends, their customers, their coworkers, and their community.  I need more people concerned about cybersecurity to tell EFF that Yara could be even bigger than LetsEncrypt if they pursue it regardless of pushback from business interests.</p>
<p>As you probably know, Congress is suddenly making a lot of noise about the apalling state of US cybersecurity (federal, state, local government agencies, large and small companies, SOHO routers, medical devices and other IoT devices, Zoom calls, etc, etc).  We have seen such uproars before, mainly followed by carelessly written bills which would not only do nothing to help cybersecurity but which would actually make the problems worse.  </p>
<p>Highly sensitive information about citizens is still required to be shared among public and private entities, and is commonly shared by unencrypted fax transmissions.  If Congress were really serious about protecting ordinary people, outlawing that practice would be a good place to start.  USG blessing plus an NSF grant to help EFF turn YaYa into a Debian package would be another good starting point.  Mandating USG backdoors in all encrypted communications would on the other hand be possibly the single worst thing they could possibly do.</p>
<p>Well meaning people sometimes suggest that I should express my views to politicians.  Unfortunately what I hear repeatedly from saddened staffers is "one voice is never heard; you need to come back as part of a group".  The more honest ones take me aside and whisper that the price of admission to the inner office is far lower than one might guess: a thousand dollar contribution to the campaign chest will get you 15 minutes.  (Amazingly, that is all that horrid companies like Comcast seen to need to pour poison onto a particular Congressional ear.)  But you have to come as a group.  Now, Tor Project is a group.  My voice alone merely echoes endlessly unheard in a desolate mountain landscape.  Hint, hint.</p>
<p>&gt;  Who knows if they aren't causing trouble in their favor through Tor themselves.</p>
<p>You might well be right about that too.   What a world...</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-291116"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291116" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 10, 2021</p>
    </div>
    <a href="#comment-291116">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291116" class="permalink" rel="bookmark">Keeping my fingers crossed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Keeping my fingers crossed for further development!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291117"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291117" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 10, 2021</p>
    </div>
    <a href="#comment-291117">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291117" class="permalink" rel="bookmark">Thank you for your hard work…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for your hard work and your contributions! We need the Tor network!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291137"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291137" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 11, 2021</p>
    </div>
    <a href="#comment-291137">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291137" class="permalink" rel="bookmark">Why are no comments allowed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why are no comments allowed at </p>
<p><a href="https://blog.torproject.org/anonymous-gitlab" rel="nofollow">https://blog.torproject.org/anonymous-gitlab</a></p>
<p>?</p>
<p>As a user who has long called for something like this, I'd love to have the chance to try a test portal before your announce.  Jes' sayin'...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291141"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291141" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  alsmith
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Al Smith</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/194">Al Smith</a> said:</p>
      <p class="date-time">February 12, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291137" class="permalink" rel="bookmark">Why are no comments allowed…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291141">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291141" class="permalink" rel="bookmark">The blog post outlines how…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The blog post outlines how you can test out this feature and give feedback. Feel free to join us on IRC if you have further comments.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
