title: Digital Privacy Expo (Toronto Library)
---
author: steph
---
start_date: 2018-09-22
---
body:


Public libraries have long been defenders of intellectual freedom - the right to pursue different ideas or research, without fear of judgment. The increase of mass surveillance technology online today makes guarding intellectual freedom trickier than ever before.

The library aims to empower Torontonians with the knowledge and confidence they need to navigate digital spaces without compromising their privacy and security by presenting a series on branch [programs](https://www.torontopubliclibrary.ca/search.jsp?Ntt=Digital+Privacy&N=37867&view=grid&Erp=25), an all-day [Digital Privacy Expo](https://www.torontopubliclibrary.ca/programs-and-classes/featured/digital-privacy.jsp#expo) and a [Tor Browser Pilot Project](https://www.torontopubliclibrary.ca/using-the-library/computer-services/tor-browser-pilot/):

Digital Privacy Expo
---------------------

* Date: Saturday, September 22
* Time: 10 am - 4 pm
* Location: [North York Central Library](https://www.torontopubliclibrary.ca/northyorkcentral/)

Event Schedule
---------------

10 - 11:15 am  

[Privacy and Security on the 21st Century Internet: A Big Picture View](https://www.eventbrite.com/e/privacy-and-security-on-the-21st-century-internet-a-big-picture-view-tickets-48736961564)  

with [Nasma Ahmed](https://www.torontopubliclibrary.ca/programs-and-classes/featured/digital-privacy.jsp#nasma) (Digital Justice Lab) & [Ksenia Ermoshina](https://www.torontopubliclibrary.ca/programs-and-classes/featured/digital-privacy.jsp#ksenia) (Citizen Lab)

11:30 am - 12:45 pm  

[Your Privacy and the Law in Canada](https://www.eventbrite.com/e/your-privacy-and-the-law-in-canada-tickets-48740383800)  

with [Brent Arnold](https://www.torontopubliclibrary.ca/programs-and-classes/featured/digital-privacy.jsp#brent) (Ontario Bar Association)

1 - 2:15 pm  

[Talking Privacy Tech: A Round-Table with Leading Technologists](https://www.eventbrite.com/e/talking-privacy-tech-a-round-table-with-leading-technologists-tickets-48741023714)  

with [Roger Dingledine](https://www.torontopubliclibrary.ca/programs-and-classes/featured/digital-privacy.jsp#roger) (Tor Project), [Ksenia Ermoshina](https://www.torontopubliclibrary.ca/programs-and-classes/featured/digital-privacy.jsp#ksenia) (Citizen Lab) and [James Donaldson](https://www.torontopubliclibrary.ca/programs-and-classes/featured/digital-privacy.jsp#james) (Copperhead).

2:30 - 3:45 pm  

[Spyware and Malware: An Insider's Perspective](https://www.eventbrite.com/e/spyware-and-malware-an-insiders-perspective-tickets-48741700739)  

with [Garry Pejski](https://www.torontopubliclibrary.ca/programs-and-classes/featured/digital-privacy.jsp#garry) (former malware developer, currently at Rogers).

Speaker Bios
-------------

**Nasma Ahmed** works at the intersections of social justice, technology and policy. She is a technologist and capacity builder based in Toronto and Executive Director of the Digital Justice Lab, a national organization focusing on building a more just and equitable digital future in Canada.

**Brent Arnold** is a partner practising in Gowling WLG's advocacy department, specializing in commercial litigation and arbitration. Brent’s experience includes cyber risk, consumer, implementation and other disputes for ecommerce vendors and software developers.

**Roger Dingledine** is president and co-founder of the Tor Project, a non-profit that develops free and open-source software to protect people from tracking, censorship and surveillance online. Among his achievements, he has been recognized by Foreign Policy magazine as one of its top 100 global thinkers.

**James Donaldson** is a hacker with specializations in operational security, systems engineering, community outreach and personal data privacy. He is CEO of mobile security experts Copperhead, and has been featured in publications such as The Wall Street Journal, The Globe and Mail and VICE’s Cyberwar.

**Ksenia Ermoshina** is a postdoctoral research fellow and Open Tech Fund fellow at the Citizen Lab at U of T. Her current work focusses on information control in Crimea. Her PhD focusses on civic apps and the civic hacking movement.

**Garry Pejski** has been making software in a variety of roles and industries for 20 years. He has created dating websites, Internet casinos, nuclear plant software and malware. He has spoken on the topic of security at the DEF CON hacking conference and in Wired magazine.

Website:
[Event Website ](https://tpl.ca/digitalprivacy)
